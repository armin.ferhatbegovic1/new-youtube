import RootComponentStyles from '../Styles/RootComponentStyles'

const styles ={
  ...RootComponentStyles,
  toolbarRoot: {
    paddingRight: 24,
    backgroundColor:"#2db1ff"
  },
  menuButton: {
    marginLeft: 12,
    marginRight: 36
  },
  title: {
    flexGrow: 1
  }


}

export default styles
