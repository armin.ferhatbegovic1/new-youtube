import React from 'react'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import IconButton from '@material-ui/core/IconButton'
import Typography from '@material-ui/core/Typography'
import Badge from '@material-ui/core/Badge'
import MenuItem from '@material-ui/core/MenuItem'
import Menu from '@material-ui/core/Menu'
import { fade } from '@material-ui/core/styles/colorManipulator'
import { withStyles } from '@material-ui/core/styles'
import AccountCircle from '@material-ui/icons/AccountCircle'
import MailIcon from '@material-ui/icons/Mail'
import NotificationsIcon from '@material-ui/icons/Notifications'
import MoreIcon from '@material-ui/icons/MoreVert'
import { connect } from 'react-redux'
import { logoutUser } from '../../store/actions/authentication'
import { withRouter } from 'react-router-dom'
import { isEmpty, localize } from '../../services'
import Logo from '../UI/Logo/Logo'
import Dimensions from '../../utils/Dimensions'
import Button from '@material-ui/core/Button'
import MenuIcon from '@material-ui/icons/Menu'
import { deviceType } from '../../config/Constants'
import Colors from '../../utils/Colors'
import UserImage from '../UserImage/UserImage'

const styles = theme => ({
  appBar: {
    boxShadow: 'unset'
  },
  root: {},
  grow: {
    flexGrow: 1
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20
  },
  title: {
    marginLeft: 15,
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block'
    }
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25)
    },
    marginRight: theme.spacing.unit * 2,
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing.unit * 3,
      width: 'auto'
    }
  },
  searchIcon: {
    width: theme.spacing.unit * 9,
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  inputRoot: {
    color: 'inherit',
    width: '100%'
  },
  inputInput: {
    paddingTop: theme.spacing.unit,
    paddingRight: theme.spacing.unit,
    paddingBottom: theme.spacing.unit,
    paddingLeft: theme.spacing.unit * 10,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: 200
    }
  },
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex'
    }
  },
  sectionMobile: {
    display: 'flex',
    [theme.breakpoints.up('md')]: {
      display: 'none'
    }
  },
  toolbarRoot: {
    backgroundColor: '#2db1ff',
    paddingTop: Dimensions.baseMargin,
    paddingBottom: Dimensions.baseMargin
  },
  homePageToolbarRoot: {
    justifyContent: 'flex-end',
    backgroundColor: '#2db1ff',
    paddingTop: Dimensions.baseMargin,
    paddingBottom: Dimensions.baseMargin
  }
})

class Header extends React.Component {
  state = {
    anchorEl: null,
    mobileMoreAnchorEl: null
  }

  handleProfileMenuOpen = event => {
    this.setState({anchorEl: event.currentTarget})
  }

  handleMenuClose = () => {
    this.setState({anchorEl: null})
    this.handleMobileMenuClose()
  }

  handleMobileMenuOpen = event => {
    this.setState({mobileMoreAnchorEl: event.currentTarget})
  }

  handleMobileMenuClose = () => {
    this.setState({mobileMoreAnchorEl: null})
  }

  onLogout (e) {
    e.preventDefault()
    this.props.logoutUser(this.props.history)
  }

  goToMyProfile = () => {
    this.props.history.push('/profile')
  }

  renderHomePageToolbar () {
    const {classes} = this.props
    return <Toolbar className={classes.homePageToolbarRoot}>
      <Button color='inherit'>{localize('howItWorks')}</Button></Toolbar>
  }

  componentDidMount () {
    window.addEventListener('resize', this.resize)
  }

  componentWillUnmount () {
    window.removeEventListener('resize', this.resize)
  }

  resize = () => {
    const {open, handleToggleDrawer} = this.props
    if (Dimensions.getDevice() === deviceType.PHONE && open) {
      handleToggleDrawer()
      this.handleMenuClose()
      this.forceUpdate()
    }
    if (Dimensions.getDevice() !== deviceType.PHONE && !open) {
      handleToggleDrawer()
      this.handleMenuClose()
      this.forceUpdate()
    }
  }

  renderBrandToolbar () {
    const {anchorEl} = this.state
    const {classes, handleToggleDrawer, auth} = this.props
    const isMenuOpen = Boolean(anchorEl)
    const userImage = !isEmpty(auth.user['profileImage']) ? auth.user['profileImage'] : null
    return <Toolbar classes={{root: classes.toolbarRoot}} style={{color: Colors.white}}>
      {Dimensions.getDevice() !== deviceType.PHONE ?
        <Logo mini={true} header={true} />
        :
        <IconButton className={classes.menuButton} color='inherit' aria-label='Menu'
          onClick={() => {handleToggleDrawer()}}>
          <MenuIcon />
        </IconButton>
      }
      <Typography className={classes.title} variant='h6' color='inherit' noWrap style={{color: Colors.white}}>
        VoiceOver
      </Typography>

      <div className={classes.grow} />
      <div className={classes.sectionDesktop}>

        <IconButton
          aria-owns={isMenuOpen ? 'material-appbar' : undefined}
          aria-haspopup='true'
          onClick={this.handleProfileMenuOpen}
          color='inherit'
        >
          <UserImage src={userImage} mini={true} header={true} />
        </IconButton>

        <Button color='inherit' onClick={this.onLogout.bind(this)}>{localize('logOut')}</Button>
      </div>
      <div className={classes.sectionMobile}>
        <IconButton aria-haspopup='true' onClick={this.handleMobileMenuOpen} color='inherit'>
          <MoreIcon />
        </IconButton>
      </div>
    </Toolbar>

  }

  render () {
    const {anchorEl, mobileMoreAnchorEl} = this.state
    const {classes, auth} = this.props
    const isMenuOpen = Boolean(anchorEl)
    const isMobileMenuOpen = Boolean(mobileMoreAnchorEl)

    const renderMenu = (
      <Menu
        anchorEl={anchorEl}
        anchorOrigin={{vertical: 'top', horizontal: 'right'}}
        transformOrigin={{vertical: 'top', horizontal: 'right'}}
        open={isMenuOpen}
        onClose={this.handleMenuClose}
      >
        <MenuItem onClick={this.goToMyProfile}>{localize('myAccount')}</MenuItem>
      </Menu>
    )

    const renderMobileMenu = (
      <Menu
        anchorEl={mobileMoreAnchorEl}
        anchorOrigin={{vertical: 'top', horizontal: 'right'}}
        transformOrigin={{vertical: 'top', horizontal: 'right'}}
        open={isMobileMenuOpen}
        onClose={this.handleMenuClose}
      >
        <MenuItem onClick={this.handleMobileMenuClose}>
          <IconButton color='inherit'>
            <Badge badgeContent={4} color='secondary'>
              <MailIcon />
            </Badge>
          </IconButton>
          <p>Messages</p>
        </MenuItem>
        <MenuItem onClick={this.handleMobileMenuClose}>
          <IconButton color='inherit'>
            <Badge badgeContent={11} color='secondary'>
              <NotificationsIcon />
            </Badge>
          </IconButton>
          <p>Notifications</p>
        </MenuItem>
        <MenuItem onClick={this.handleProfileMenuOpen}>
          <IconButton color='inherit'>
            <AccountCircle />
          </IconButton>
          <p>Profile</p>
        </MenuItem>
      </Menu>
    )

    return (
      <div className={classes.root}>
        <AppBar position='fixed' className={classes.appBar}>
          {auth.isAuthenticated ? this.renderBrandToolbar() :
            this.renderHomePageToolbar()}
        </AppBar>
        {renderMenu}
        {renderMobileMenu}
      </div>
    )
  }
}

const mapStateToProps = state => ({
  auth: state.auth
})

const mapStateToDispatch = dispatch => ({
  logoutUser: (history) => dispatch(logoutUser(history))
})

export default connect(mapStateToProps, mapStateToDispatch)(withRouter(withStyles(styles)(Header)))
