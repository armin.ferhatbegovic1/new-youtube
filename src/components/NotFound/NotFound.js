import React, { Component } from 'react';
import styles from '../Styles/RootComponentStyles'
import { localize } from '../../services'
class NotFound extends Component {
    render() {
        return (
            <div className="container-full align-center-full">
                <div style={styles.messageContainer} className="align-center-full flex-wrap">
                    <p className="full-width text-center" style={styles.message404}>
                      {localize('404code')}
                    </p>
                    <p className="full-width text-center" style={styles.messageNotFound}>
                      {localize('pageNotFound')}
                    </p>
                </div>
            </div>
        );
    }
}

export default NotFound;
