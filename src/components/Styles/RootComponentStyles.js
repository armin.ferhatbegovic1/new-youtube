import Dimensions from '../../utils/Dimensions'
import Colors from '../../utils/Colors'
import { deviceType } from '../../config/Constants'

const RootComponentStyles = {
  containerFull: {
    width: '100% !important',
    height: '100% !important',
    overflowY: 'auto !important',
    overflowX: 'hidden !important'
  },
  paddingFullStandard: {
    padding: '16px'
  },
  errorMessage: {

    color: 'red'
  },
  formControl: {
    width: Dimensions.inputFields.input3XLarge,
    maxWidth: Dimensions.inputFields.input3XLarge,
    marginTop: '16px'
  },
  chips: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  primaryButton: {
    color: Colors.white,
    backgroundColor: Colors.secondary
  },
  secondaryButton: {
    color: Colors.secondary,
    backgroundColor: Colors.color4

  },
  card: {
    maxWidth: 400
  },
  media: {
    height: 0,
    paddingTop: '56.25%' // 16:9
  },
  placeholderMedia: {
    height: 0,
    paddingTop: '56.25%', // 16:9
    opacity: '0.4'
  },
  actions: {
    display: 'flex'
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto'
    /*  transition: theme.transitions.create('transform', {
        duration: theme.transitions.duration.shortest
      })*/
  },
  expandOpen: {
    transform: 'rotate(180deg)'
  },
  avatar: {
    backgroundColor: 'red' //red[500]
  },
  dealActive: {
    backgroundColor: Colors.active
  },
  dealDraft: {
    backgroundColor: Colors.draft
  },
  requirementLabel: {
    fontWeight: 'bold'
  },
  cardButton: {
    display: 'block',
    textAlign: 'initial'
  },
  previewDealLink: {
    textDecoration: 'none',
    color: Colors.black,
  },
  dealCard: {zIndex: 0, textAlign: 'unset'},
  readMore: {
    color: Colors.secondary,
    fontSize: Dimensions.textSmall - 3
  },
  placeholder: {
    opacity: '0.1'
  },
  cardTitleLargeDesktop: {
    fontSize: Dimensions.textMedium
  },
  cardTitleDesktop: {
    fontSize: Dimensions.textMedium
  },
  cardTitleTablet: {
    fontSize: Dimensions.textSmall
  },
  cardTitlePhone: {
    fontSize: Dimensions.textXSmall
  },
  categoryChip: {
    marginTop: Dimensions.baseMargin,
    marginLeft: Dimensions.baseMargin / 2,
    marginRight: Dimensions.baseMargin / 2,
   /* color:Colors.blackLight,
    backgroundColor:Colors.gray,*/
    color:Colors.white,
    backgroundColor:Colors.secondary,
    boxShadow: '0px 2px 4px -1px rgba(0,0,0,0.2), 0px 4px 5px 0px rgba(0,0,0,0.14), 0px 1px 10px 0px rgba(0,0,0,0.12)',

  },
  cardDealActions: {
    display: 'flex',
    justifyContent: 'space-between',
    marginTop: Dimensions.baseMargin
  },

  dealDescription: {
    fontStyle: 'italic',
    color: Colors.blackLight
  },
  bidCardTitle: {
    fontWeight: 'bold',
    lineHeight: 2,
    color: Colors.blackLight,
    fontSize: Dimensions.getDevice() === deviceType.LARGE_DESKTOP ? Dimensions.textSmall : Dimensions.textMedium
  },
  propertyValueField: {
    color: Colors.secondary,
    lineHeight: 2,
    fontStyle: 'italic',
  },
  propertyValueFieldLargeDesktop: {
    fontSize: Dimensions.textSmall
  },

  propertyValueFieldDesktop: {
    fontSize: Dimensions.textXSmall
  },

  propertyValueFieldTablet: {
    fontSize: Dimensions.textXXSmall
  },
  propertyValueFieldPhone: {
    fontSize: Dimensions.textXXSmall
  },

  propertyField: {
    fontWeight: 'bold',
    color: Colors.blackLight,
    display: 'inline-flex',
      fontSize:20,
      marginRight:10
  },

  propertyFieldLargeDesktop: {
    fontSize: Dimensions.textXSmall
  },

  propertyFieldDesktop: {
    fontSize: Dimensions.textXSmall
  },

  propertyFieldTablet: {
    fontSize: Dimensions.textXXSmall
  },
  propertyFieldPhone: {
    fontSize: Dimensions.textXXSmall
  },
  noResultsContainer: {
    color: Colors.secondary,
    fontSize: Dimensions.textX4Large,
    marginLeft: Dimensions.doubleBaseMargin
  },
  messageContainer: {
    height: 250
  },
  message404: {
    fontSize: 150,
    color: Colors.secondary,
    margin: 0
  },
  messageNotFound: {
    fontSize: 48,
    color: Colors.secondary,
    margin: 0
  },
  cardGlobalTitle:{
    textDecoration:'unset',
    color:Colors.secondary
  },
  primaryShadow:{
    boxShadow: '0px 2px 4px -1px rgba(0,0,0,0.2), 0px 4px 5px 0px rgba(0,0,0,0.14), 0px 1px 10px 0px rgba(0,0,0,0.12)',
  }
}

export default RootComponentStyles