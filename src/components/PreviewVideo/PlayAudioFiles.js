import React, { Component } from 'react'
import { videos } from '../../config/Endpoints'
import { localize, prepareDataFormForSubmitAction } from '../../services'
import { audioFileFormPreview } from '../../forms/uploadVideo'
import InputForm from '../UI/InputForm/InputForm'
import { alertDialogType, formMode, layoutType, toastType } from '../../config/Constants'
import AxiosInstance from '../../services/Axios'
import connect from 'react-redux/es/connect/connect'
import { setToastNotification } from '../../store/actions/toastAction'
import { setAlertDialog } from '../../store/actions/alertDialogAction'

const config = {
  headers: {
    'X-Auth-Token': localStorage.getItem('jwtToken')
  }
}

class UploadAudioFiles extends Component {
  constructor (props) {
    super(props)
    this.state = {
      audioFileForm: JSON.parse(JSON.stringify(audioFileFormPreview)),
      index: props.index,
      addAudioData: props.addAudioData,
      play: false
    }
  }

  componentWillMount () {
    this.loadDataIntoForm()
  }
  loadDataIntoForm(){

    /*    audioFileForm['text'].value=data.text
        audioFileForm['end'].value=data.end
        audioFileForm['start'].value=data.start*/
  }

  uploadAudioFile = async (file) => {
    const formData = new FormData()
    const {audioFileForm, addAudioData, index} = this.state
    const {setAlertDialog, setToastNotification} = this.props
    const audioFileUploaded = prepareDataFormForSubmitAction(audioFileForm)
    formData.append('belongs_to', 'sentence.audio')
    formData.append('public', true)
    if (audioFileUploaded['videoCover'] === null) {
      setAlertDialog(true, 'Audio File Is not uploaded', localize('videoUploadWarning'), alertDialogType.WARNING)
    } else {
      formData.append('file', audioFileUploaded['videoCover'].value)

      await AxiosInstance.post(videos.FILE_UPLOAD, formData, config).then(reponse => {
        let reponseMessage = ''
        let type = toastType.SUCCESS
        if (reponse.data.success === true) {
          reponseMessage = 'Audio File is successful uploaded'

          const data = {
            ...audioFileUploaded,
            audio_file_uuid: reponse.data['object_uuid']
          }

          delete data.videoCover
          addAudioData(data, index)

        }
        if (reponse.data.success !== true || reponse.status !== 200) {
          reponseMessage = 'Audio File uploaded is failed' + reponse.data
          type = toastType.FAILURE
        }

        setToastNotification(true, reponseMessage, type)
      })

    }
  }

  render () {
    const {audioFileForm} = this.state
    const {data} = this.props
    return <div style={{width: 500}}>
      <InputForm formElements={audioFileForm} prepareDataAndSubmit={this.uploadAudioFile} data={data}
                 mode={formMode.VIEW} layout={layoutType.COLUMNS_2} lastElementInLayout={layoutType.COLUMNS_1}
                 buttonLabel='Upload Audio' />
    </div>
  }
}

export default connect(null, {setToastNotification, setAlertDialog})((UploadAudioFiles))
