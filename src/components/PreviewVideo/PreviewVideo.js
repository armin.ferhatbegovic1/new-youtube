/** @flow */
import React, { Component, Fragment } from 'react'
import { getRequest, isEmpty } from '../../services'
import styles from './styles'
import Grid from '@material-ui/core/Grid/Grid'
import { setToastNotification } from '../../store/actions/toastAction'
import connect from 'react-redux/es/connect/connect'
import Dimensions from '../../utils/Dimensions'
import { videos } from '../../config/Endpoints'
import config from '../../config/config'
import PlayAudioFiles from './PlayAudioFiles'

class PreviewVideo extends Component {

  constructor (props) {
    super(props)
    this.state = {
      videoID: new URL(window.location.href).searchParams.get('id'),
      video: null,
      audioSetienceNumber: 0,
      audioSetienceData: [],
    }
  }

  async componentDidMount () {
    const data = await this.getVideo()
    if (!isEmpty(data)) {
      this.setState({video: data['payload'], audioFiles: data['payload'].sentences,classificationType:data['payload'].classification_type})
    } else {
      this.setState({noResultsMessageVisible: true})
    }
  }

  async getVideo () {
    return await getRequest(videos.VIDEO_BY_ID + '/' + this.state.videoID)
  }

  createUrlForReadingVideo (video, index) {
    const videoFileUuid = video.video_file_uuid
    return config.apiEndpoint + '/file/' + videoFileUuid

  }

  renderAudioFields = () => {
    const {audioSetienceNumber, audioFiles} = this.state

    if (!isEmpty(audioFiles) && audioFiles.length !== 0) {
      return Array.apply(0, Array(audioFiles.length)).map((x, i) => {
        return <PlayAudioFiles key={i} index={i} addAudioData={this.addAudioDataInArray} data={audioFiles[i]} />
      })
    }
  }

  renderVideoUploader () {
    const {video} = this.state
    return !isEmpty(video) ?
      <Fragment>
        <Grid container spacing={24}>
          <Grid item xs={12}>
            <div style={{display: 'flex',    justifyContent: 'center'}}>
              <div style={styles.videoContainer} id='video-container'>
                <video id='videoPlayer' controls style={{
                  maxWidth: Dimensions.videoWidth,
                  maxHeight: Dimensions.videoHeight
                }}>
                  <source src={this.createUrlForReadingVideo(video, 0)} type='video/mp4' />
                  <source src={this.createUrlForReadingVideo(video, 1)} type='video/ogg' />
                  <source src={this.createUrlForReadingVideo(video, 2)} type='video/webm' />
                </video>
              </div>
            </div>

            <div style={{display: 'flex', flexDirection: 'column',alignContent:'center'}}>
              <label style={styles.description}> {!isEmpty(video['title']) ? video['title'] : video['originalName']}</label>
              <label style={styles.description}> <label style={styles.propertyField}> Description: </label>  {!isEmpty(video['description']) ? video['description'] : ''}</label>
              <label style={styles.description}> <label style={styles.propertyField}>  Duration: </label>  {!isEmpty(video['video_duration']) ? video['video_duration'] : ''}</label>
              <label style={styles.description}> <label style={styles.propertyField}>  Difficulty: </label>  {!isEmpty(video['difficulty']) ? video['difficulty'] : ''}</label>
              <label style={styles.description}> <label style={styles.propertyField}>  Classification Type: </label>  {!isEmpty(video['classification_type']) ? video['classification_type'] : ''}</label>
            </div>
          </Grid>
          <Grid item xs={12}>
            <div style={{display: 'flex', flexWrap: 'wrap'}}> {this.renderAudioFields()}</div>
          </Grid>
        </Grid>
      </Fragment> : <label>Loading</label>

  }

  render () {
    return (
      this.renderVideoUploader()
    )
  }
}

export default connect(null, {setToastNotification})((PreviewVideo))
