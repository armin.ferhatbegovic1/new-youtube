import RootComponentStyles from '../Styles/RootComponentStyles'
import Colors from '../../utils/Colors'
import Dimensions from '../../utils/Dimensions'

const styles = {
  ...RootComponentStyles,
  videoContainer: {
    border: '4px solid ' + Colors.yellow,
    boxShadow: '5px 5px 5px 5px' + Colors.color5,
    maxWidth: Dimensions.videoWidth+20,
    maxHeight: Dimensions.videoHeight+5,
    marginTop: Dimensions.doubleBaseMargin * 2,
    marginBottom: Dimensions.doubleBaseMargin * 2,
    marginLeft: 66,
    backgroundColor: '#DDDDDD',
  },
  videoPlaceholder: {
    maxWidth: Dimensions.videoWidth - 10,
    maxHeight: Dimensions.videoHeight - 10,
    backgroundColor: '#DDDDDD',
    width: '100%',
    height: 'auto'
  },
  removeVideo: {
    alignSelf: 'flex-end',
    marginLeft: '16'
  },
  playerControls: {
    display: 'flex',
    justifyContent: 'flex-end'
  },
  btnContainer: {
    margin: 10,
    color: 'white'
  },
  description:{
    alignSelf: 'center',
    display: 'flex',
    color: Colors.secondary,
    fontSize:22
  },
  playBox: {
    width: 55,
    height: 35,
    borderRadius:15
  }

}

export default styles
