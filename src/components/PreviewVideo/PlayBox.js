import React, { Component } from 'react'
import styles from './styles'
import Images from '../../utils/Images'
import { videos } from '../../config/Endpoints'
import apiEndpoint from '../../config/config'

export default class PlayBox extends Component {
  constructor (props) {
    super(props)
    this.state = {
      blobObject: null,
      isPaused: false,
      isPlaying: false
    }
  }

  startStopPlaying = () => {
    const {isPlaying} = this.state
    if (isPlaying) {
      this.setState({isPlaying: false})
      this.audioRef.pause()
    } else {
      this.setState({isPlaying: true})
      this.audioRef.play()
    }
  }

  componentDidMount () {
    this.audioRef.addEventListener('ended', this.startStopPlaying)
  }

  componentWillUnmount () {
    this.audioRef.removeEventListener('ended', this.startStopPlaying)
  }

  render () {
    const {audio, style} = this.props
    let audioFile = null
    if (audio.hasOwnProperty('valueImg')) {
      audioFile = audio.valueImg
    } else {
      audioFile =apiEndpoint.apiEndpoint+videos.FILE+'/'+ audio
    }

    //,backgroundColor:properties.color
    return <div onClick={this.startStopPlaying} style={Object.assign({}, styles.playBox)} className='playBox'>
      <img className='musicClass' style={{...style}} src={this.state.isPlaying ? Images.stopIcon : Images.play}
        alt='music' />
      <audio ref={(input) => {
        this.audioRef = input
      }} controls='controls' src={audioFile} style={{display: 'none'}} />
    </div>
  }
}
