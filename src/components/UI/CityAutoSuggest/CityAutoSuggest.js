import React from 'react'
import AutoSuggest from '../AutoSuggest/AutoSuggest'

const cityAutoSuggest = (props) => {
  const {label, placeholder, countryCode, onChange, isValid, value, disabled, style} = props
  const requestLink = '/cities?country=' + countryCode + '&q='
  return (<AutoSuggest label={label} placeholder={placeholder} requestLink={requestLink} fieldForShow='cityName'
    valueHandler={onChange} isValid={isValid} value={value} disabled={disabled} style={style} />)
}

export default cityAutoSuggest