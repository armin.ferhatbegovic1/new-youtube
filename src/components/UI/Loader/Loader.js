import React from 'react'
import { connect } from 'react-redux'
import Dimensions from '../../../utils/Dimensions'
import CircularProgress from '@material-ui/core/CircularProgress/CircularProgress'

const styles = {
  circularProgress: {
    overflow: 'hidden',
    position: 'absolute',
    width: Dimensions.fullWidth,
    height: Dimensions.fullWidth,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 999
  }
}

const Loader = (props) => {
  return (
    props.visible ? <div style={styles.circularProgress}><CircularProgress /></div> : null
  )
}

function mapStateToProps (store) {
  return {
    visible: store.loader.visible
  }
}

export default connect(mapStateToProps)(Loader)
