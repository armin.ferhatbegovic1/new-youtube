import React, { Component } from 'react'
import { table } from '../../../config/Constants'
import {
  Card,
  Grid,
  Table,
  TableBody,
  TableCell,
  TableFooter,
  TableHead,
  TablePagination,
  TableRow
} from '@material-ui/core'

import TableSimpleCell from './TableSimpleCell'
import styles from './styles'
import Dimensions from '../../../utils/Dimensions'
import TableMenu from '../TableMenu/TableMenu'

export class TableSimple extends Component {
  constructor (props) {
    super(props)
    this.state = {
      dialogCellOpen: false,
      dialogCellHeader: [],
      dialogCellData: {},
      actions: props.actions,
      actionsProperty: props.actionsProperty
    }
  }

  handleChangePage = (event, page) => {
    this.props.setTablePage(page, false)
  }

  renderTitle () {
    const {title} = this.props
    if (title) {
      return (
        <div className='padding-vertical-half' style={styles.tableTitle}>
          <span>{title}</span>
        </div>
      )
    }
  }

  renderTableHeader () {
    const tableHeaderData =
      this.props.manager && !this.props.permaData
        ? this.props.header.concat({name: ' ', center: true})
        : this.props.header
    if (this.props.data && this.props.data.length !== 0) {
      return tableHeaderData.map((header, i) => (
        <TableCell
          key={header.label || header}
          style={Object.assign(
            {},
            styles.tableCell,
            styles.headerCell,
            i === 0
              ? styles.tableCellFirst
              : i === tableHeaderData.length - 1 ? styles.tableCellLast : ''
          )}
          className={header.center ? 'text-center' : ''}
          numeric={header.numeric}
        >
          {header.label || header}
        </TableCell>
      ))
    } else {
      return (
        <TableCell
          style={Object.assign({}, styles.headerCell)}
          className='text-center'
        >
          Empty
        </TableCell>
      )
    }
  }

  renderTableBody () {
    const {data} = this.props
    if (data) {
      return (
        <TableBody>
          {data
          .map((item, i) => {
            return (
              <TableRow key={i}>
                {this.renderData(item, i)}
              </TableRow>
            )
          })}
        </TableBody>
      )
    }
  }

  renderTableFooter () {
    const {totalCount, currentPage} = this.props
    if (this.props.data && totalCount > table.TABLE_ROWS_PER_PAGE) {
      return (
        <TableFooter>
          <TableRow>
            <TablePagination
              colSpan={12}
              count={totalCount}
              rowsPerPage={table.TABLE_ROWS_PER_PAGE}
              page={currentPage}
              onChangePage={this.handleChangePage}
              rowsPerPageOptions={[table.TABLE_ROWS_PER_PAGE]}

            />
          </TableRow>
        </TableFooter>
      )
    }
  }

  renderData (item, i) {
    const {fields} = this.props

    if (fields) {
      return Object.keys(fields).map((ix, i) => {
        if (typeof fields[ix] === 'object' && fields[ix].name !== 'actions') {
          return this.renderTableCell(
            i,
            item[fields[ix].name],
            fields[ix],
            Object.keys(fields).length
          )
        } else {
          return this.renderDataTableActions(item)
        }
      })
    }
    return Object.keys(item).map((ix, i) => {
      return this.renderTableCell(i, item[ix], null, Object.keys(item).length)
    })
  }

  renderDataTableActions (item) {

    const {actions, actionsProperty} = this.state
    return (
      <TableCell key={'actionCell'} style={styles.actionCell}>
        <TableMenu actions={actions} argumentForAction={item[actionsProperty]} />
      </TableCell>
    )

  }

  renderTableCell (ix, keyCell, field, rowLength) {
    // Inputs: Position of the cell, data object key
    // Action: Renders individual table cells
    const {manager, permaData} = this.props

    return (
      <TableSimpleCell
        key={ix}
        index={ix}
        keyCell={keyCell}
        format={field ? field.format : null}
        numeric={field ? field.numeric : null}
        rowLength={rowLength}
        actionsExist={manager && !permaData}
        advanceProperty={field.property ? field.property : null}
      />
    )
  }

  render () {
    const {title, width, flexWidth, size} = this.props

    return (
      <Grid
        item
        xs={size[0]}
        sm={size[1]}
        md={size[2]}
        lg={size[3] || size[2]}
        style={styles.paddingFullStandard}>
        <Card style={Object.assign({}, width < Dimensions.phone && !flexWidth ? styles.tableWrapper : '')}>
          {this.renderTitle()}
          <Table data-id={'table-simple/' + title}>
            <TableHead>
              <TableRow style={styles.tableHeaderRow}>
                {this.renderTableHeader()}
              </TableRow>
            </TableHead>
            {this.renderTableBody()}
            {this.renderTableFooter()}
          </Table>
        </Card>
      </Grid>
    )
  }
}

export default TableSimple
