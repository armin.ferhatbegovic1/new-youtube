import React, { Component } from 'react'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import { DIALOG_MESSAGE, DIALOG_OPEN, DIALOG_ACTION } from '../../../store/actions/types'
import { alertDialogType } from '../../../config/Constants'
import { localize, isEmpty } from '../../../services'
import { connect } from 'react-redux'

function mapStateToProps (store) {
  return {
    open: store.dialog.open,
    message: store.dialog.message,
    type: store.dialog.type,
    title: store.dialog.title,
    action: store.dialog.action
  }
}

class AlertDialog extends Component {

  handleClose = () => {
    this.closeDialog()
  }

  closeDialog = () => {
    const {dispatch} = this.props
    dispatch({type: DIALOG_OPEN, payload: false})
    dispatch({type: DIALOG_ACTION, payload: null})
    setTimeout(
      function () {
        dispatch({type: DIALOG_MESSAGE, payload: ''})
      },
      500
    )
  }

  getTitle (type) {
    switch (type) {
      case alertDialogType.WARNING:
        return localize('warningMessage')
      case alertDialogType.ERROR:
        return localize('errorMessage')
      case alertDialogType.INFO:
        return localize('infoMessage')
      case alertDialogType.CONFIRM:
        return localize('confirmMessage')
      default:
        return null
    }
  }

  handleAction = () => {
    const {action} = this.props
    action()
    this.handleClose()
  }

  getButtons (type) {
    switch (type) {
      case alertDialogType.WARNING:
        return <Button onClick={this.handleClose} color='primary'>{localize('ok')}</Button>
      case alertDialogType.ERROR:
        return localize('errorMessage')
      case alertDialogType.INFO:
        return localize('infoMessage')
      case alertDialogType.CONFIRM:
        return <div><Button onClick={this.handleAction} color='primary'>{localize('yes')}</Button><Button
          onClick={this.handleClose} autoFocus color='primary'>{localize('cancel')}</Button></div>
      default:
        return null
    }
  }

  render () {
    const {message, open, type, title} = this.props
    let dialogTitle = !isEmpty(title) ? title : this.getTitle(type)

    return (
      <div>
        <Dialog
          open={open}
          onClose={this.handleClose}
          aria-labelledby='alert-dialog-title'
          aria-describedby='alert-dialog-description'
        >
          {dialogTitle ? <DialogTitle id='alert-dialog-title'>{dialogTitle}</DialogTitle> : null}
          <DialogContent>
            <DialogContentText id='alert-dialog-description'>
              {message}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            {this.getButtons(type)}
          </DialogActions>
        </Dialog>
      </div>
    )
  }
}

export default connect(mapStateToProps)(AlertDialog)