import RootComponentStyles from '../../Styles/RootComponentStyles'
import Dimensions from '../../../utils/Dimensions'

const styles = {
  ...RootComponentStyles,
  mainButton: {
    textTransform:'unset',
    width: Dimensions.tripeBaseMargin * 4,
    borderRadius: Dimensions.doubleBaseMargin,
    padding: Dimensions.baseMargin,
    fontWeight: 'bold',
    margin:Dimensions.baseMargin
  }
}

export default styles