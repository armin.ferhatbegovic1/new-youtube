import React from 'react'
import styles from './styles'
import Button from '@material-ui/core/Button'

const mainButton = (props) => {
  const {label, disabled, buttonType, type} = props
  return <Button variant='contained' style={Object.assign({}, buttonType === 'primary' ? styles.primaryButton : styles.secondaryButton, styles.mainButton)}
    disabled={disabled} type={type ? type : ''}>
    {label}
  </Button>
}

export default mainButton