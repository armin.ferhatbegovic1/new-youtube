import RootComponentStyles from '../../Styles/RootComponentStyles'
import Colors from '../../../utils/Colors'
import Dimensions from '../../../utils/Dimensions'

const styles = {
  ...RootComponentStyles,
  neutralButton: {
    boxShadow: 'unset',
    color: Colors.white
  },
  miniButton: {
    marginTop: Dimensions.baseMargin,
    height: Dimensions.miniButtonIcon,
    width: Dimensions.miniButtonIcon
  }
}
export default styles










