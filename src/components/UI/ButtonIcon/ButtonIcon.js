import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import Fab from '@material-ui/core/Fab'
import styles from './styles'

function ButtonIcon (props) {
  const {handleAction, children, buttonType, mini} = props
  return (
    <Fab color='primary' style={Object.assign({},styles[buttonType+'Button'], mini? styles.miniButton: null)} aria-label='Add' onClick={() => {handleAction()}}>
      {children}
    </Fab>
  )
}

export default withStyles(styles)(ButtonIcon)