import RootComponentStyles from '../../Styles/RootComponentStyles'
import Dimensions from '../../../utils/Dimensions'

const styles = {
  ...RootComponentStyles,
  logo:{
    marginTop: Dimensions.baseMargin,
    width: 'auto',
    height: Dimensions.logoHeight
  },
  miniLogo: {
    width: 'auto',
    height: Dimensions.logoMiniHeight
  },
  logoContainer: {
    width: Dimensions.fullWidth,
    display: 'flex',
    justifyContent: 'center'
  },
  logoHeaderContainer:{
    marginTop:0,
    display: 'flex',
    justifyContent:'start',
    marginLeft:Dimensions.doubleBaseMargin
  }
}

export default styles