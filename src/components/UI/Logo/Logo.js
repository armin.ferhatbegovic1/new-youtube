import React from 'react'
import styles from './styles'
import Images from '../../../utils/Images'
import { localize } from '../../../services'




const Logo = ({ mini, isTablet,header }) => (
  <div
    style={header?styles.logoHeaderContainer:styles.logoContainer}>
    <img
        src={Images.logo}
        style={mini?styles.miniLogo:styles.logo}
        alt={localize('logo')}
    />
  </div>
)

export default Logo
