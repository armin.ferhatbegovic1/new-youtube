import RootContainerStyles from '../../Styles/RootComponentStyles'
import Colors from '../../../utils/Colors'
import Dimensions from '../../../utils/Dimensions'
const styles = {
  ...RootContainerStyles,
  title: {
    minHeight: 32,
    color: Colors.white,
    fontWeight: '400',
    fontSize: Dimensions.textMedium,
    textAlign: 'center',
    width: '100%',
    background: Colors.secondary,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'left',
    paddingLeft: '50px'
  },
  selectListContainer: {
    maxWidth: Dimensions.fullWidth,
    width: Dimensions.fullWidth,
    marginTop: 0
  },
  categoryChip: {
    marginTop: Dimensions.baseMargin,
    marginLeft: Dimensions.baseMargin / 2,
    marginRight: Dimensions.baseMargin / 2,
    /* color:Colors.blackLight,
     backgroundColor:Colors.gray,*/
    color:Colors.white,
    backgroundColor:Colors.secondary,
    boxShadow: '0px 2px 4px -1px rgba(0,0,0,0.2), 0px 4px 5px 0px rgba(0,0,0,0.14), 0px 1px 10px 0px rgba(0,0,0,0.12)',

  }
}

export default styles