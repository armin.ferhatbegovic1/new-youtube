import RootComponentStyles from '../../Styles/RootComponentStyles'
import Colors from '../../../utils/Colors'
import Dimensions from '../../../utils/Dimensions'

const styles = {
  ...RootComponentStyles,
  appBarContainer: {
    color: Colors.secondary,
    backgroundColor: Colors.yellow,
    //  marginTop:'-10px',
    boxShadow: '0px 2px 4px -1px rgba(0,0,0,0.2), 0px 4px 5px 0px rgba(0,0,0,0.14), 0px 1px 10px 0px rgba(0,0,0,0.12)',
    height:Dimensions.baseMargin*8,
    justifyContent: 'center'
  },
  toolbarContainer:{
    display: 'flex',
    justifyContent: 'flex-start'
  }

}
export default styles










