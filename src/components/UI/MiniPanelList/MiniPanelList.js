import React, { Component } from 'react'
import { withStyles } from '@material-ui/core/styles'
import MuiExpansionPanel from '@material-ui/core/ExpansionPanel'
import MuiExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary'
import MuiExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails'
import Typography from '@material-ui/core/Typography'
import Divider from '@material-ui/core/Divider/Divider'
import Colors from '../../../utils/Colors'
import Dimensions from '../../../utils/Dimensions'
import { table } from '../../../config/Constants'
import TablePagination from '@material-ui/core/TablePagination/TablePagination'
import TableRow from '@material-ui/core/TableRow/TableRow'
import TableFooter from '@material-ui/core/TableFooter/TableFooter'
import Table from '@material-ui/core/Table/Table'
import RootComponentStyles from '../../Styles/RootComponentStyles'

const styles = {
  ...RootComponentStyles,
  panelActionsSection: {
    justifyContent: 'flex-end',
    display: 'flex',
    flex: 1,
    alignItems: 'flex-end'
  }
}
const ExpansionPanel = withStyles({
  root: {
    border: '1px solid rgba(0,0,0,.125)',
    boxShadow: 'none',
    '&:not(:last-child)': {
      borderBottom: 0
    },
    '&:before': {
      display: 'none'
    }
  },
  expanded: {
    margin: 'auto'
  }
})(MuiExpansionPanel)

const ExpansionPanelSummary = withStyles({
  root: {
    backgroundColor: 'rgba(0,0,0,.03)',
    borderBottom: '1px solid rgba(0,0,0,.125)',
    marginBottom: -1,
    minHeight: 56,
    '&$expanded': {
      minHeight: 56
    }
  },
  content: {
    '&$expanded': {
      margin: '12px 0'
    }
  },
  expanded: {}
})(props => <MuiExpansionPanelSummary {...props} />)

ExpansionPanelSummary.muiName = 'ExpansionPanelSummary'

const ExpansionPanelDetails = withStyles(theme => ({
  root: {
    padding: theme.spacing.unit * 2
  }
}))(MuiExpansionPanelDetails)

class MiniPanelList extends Component {
  state = {
    expanded: 'panel0'
  }

  handleChange = panel => (event, expanded) => {
    this.setState({
      expanded: expanded ? panel : false
    })
  }

  renderLabelValueItem (label, value, valueSufix = '') {
    return <div>
      <div
        style={Object.assign({}, styles.propertyField, styles['propertyField' + Dimensions.getDevice()])}> {label}</div>
      <label
        style={Object.assign({}, styles.propertyValueField, styles['propertyValueField' + Dimensions.getDevice()])}> {value} {valueSufix}</label>
    </div>
  }

  renderItemContent (item, index) {
    const {fields} = this.props
    return <div key={index}>{fields.slice(1).map((field, i) => {
      return <div key={i + index}>
        {this.renderLabelValueItem(field.label, item[field.name])}
        <Divider />
      </div>
    })}</div>
  }

  renderItem (item, index) {
    const {fields, actions} = this.props
    const {expanded} = this.state
    return <ExpansionPanel
      key={index}
      square
      expanded={expanded === 'panel' + index.toString()}
      style={{borderBottom:'1px solid rgba(0,0,0,.125)'}}
      onChange={this.handleChange('panel' + index.toString())}
    >
      <ExpansionPanelSummary style={{backgroundColor: Colors.secondaryLight3}}>
        <Typography style={{color: Colors.secondary}}>{fields[0].label} {item[fields[0].name]}</Typography>
      </ExpansionPanelSummary>
      <ExpansionPanelDetails>
        {this.renderItemContent(item, index)}
        <div style={styles.panelActionsSection}>
          {actions(item)}
        </div>
      </ExpansionPanelDetails>
    </ExpansionPanel>
  }

  renderItemList () {
    const {data} = this.props
    return data.map((item, i) => {
      return this.renderItem(item, i)
    })
  }

  handleChangePage = (event, page) => {
    this.props.setTablePage(page, false)
  }

  renderTableFooter () {
    const {totalCount, currentPage} = this.props
    if (this.props.data && totalCount > table.TABLE_ROWS_PER_PAGE) {
      return (
        <TableFooter>
          <TableRow>
            <TablePagination
              colSpan={12}
              count={totalCount}
              rowsPerPage={table.TABLE_ROWS_PER_PAGE}
              page={currentPage}
              onChangePage={this.handleChangePage}
              rowsPerPageOptions={[table.TABLE_ROWS_PER_PAGE]}

            />
          </TableRow>
        </TableFooter>
      )
    }
  }

  render () {
    return (
      <div style={{padding: 24}}>
        {this.renderItemList()}
        <Table data-id={'table-mobile'}>
          {this.renderTableFooter()}
        </Table>
      </div>
    )
  }
}

export default MiniPanelList