import React from 'react'
import AutoSuggest from '../AutoSuggest/AutoSuggest'

const countryAutoSuggest = (props) => {
  const {label, placeholder, onChange, errorMessage, error, value, disabled, style} = props
  return (<AutoSuggest label={label} placeholder={placeholder} requestLink='/country?q=' fieldForShow='name'
    valueHandler={onChange} errorMessage={errorMessage} isValid={error} value={value} disabled={disabled}
    style={style} />)
}

export default countryAutoSuggest