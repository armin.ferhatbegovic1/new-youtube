import React, { Component } from 'react'
import Slider from '@material-ui/lab/Slider'
import AvatarEditor from 'react-avatar-editor'
import withStyles from '@material-ui/core/styles/withStyles'
import Button from '@material-ui/core/Button'
import Dimensions from '../../../utils/Dimensions'
import { deviceType } from '../../../config/Constants'
import { awaitFunction, isEmpty, localize } from '../../../services'
import EditIcon from '@material-ui/icons/AddAPhoto'
import ButtonIcon from '../ButtonIcon/ButtonIcon'
import styles from './styles'
import Images from '../../../utils/Images'

const IMAGE_WIDTH = 250
const AD = 8

function calculateSizeOfImage () {
  if (Dimensions.getDevice() === deviceType.LARGE_DESKTOP)
    return IMAGE_WIDTH

  if (Dimensions.getDevice() === deviceType.DESKTOP)
    return IMAGE_WIDTH / 2

  if (Dimensions.getDevice() === deviceType.TABLET || Dimensions.getDevice() === deviceType.PHONE)
    return IMAGE_WIDTH / 2
}

class AvatarEditorImage extends Component {
  constructor (props) {
    super(props)
    this.state = {
      scale: 1,
      radius: 0,
      rotate: 0,
      hovered: false,
      saveButtonDisabled: true
    }
  }

  handleChange = (event, value, name) => {
    this.setState({[name]: value})
  }

  handleRotateLeft = () => {
    const {rotate} = this.state
    this.setState({rotate: rotate - 90, saveButtonDisabled: false})
  }
  handleRotateRight = () => {
    const {rotate} = this.state
    this.setState({rotate: rotate + 90, saveButtonDisabled: false})
  }

  onClickSave = () => {
    if (this.editor) {

      const img = this.editor.getImageScaledToCanvas().toDataURL()
      this.props.handleSaveImage(img)
      this.setState({scale: 1, saveButtonDisabled: false, rotate: 0})
    }

  }

  setEditorRef = (editor) => this.editor = editor

  setImageHovered = async (state) => {
    await awaitFunction(this.setState({hovered: state}))
  }

  renderOptionForUploadImage () {
    const {editImage} = this.props
    if (editImage) {
      return <div style={Object.assign({}, {
        top: -1 * (calculateSizeOfImage()) / 2,
        height: calculateSizeOfImage() / 2,
        width: calculateSizeOfImage()
      }, styles.userImageAddButtonContainer)}>

        <div style={Object.assign({}, {marginLeft: (calculateSizeOfImage() - 55) / 2}, styles.imageUploadButton)}>
          <ButtonIcon label={localize('submit')} buttonType='neutral' type='submit' handleAction={this.uploadImage}>
            <EditIcon />
          </ButtonIcon>
        </div>
      </div>
    }
  }

  uploadImage = () => {
    this.upload.click()
  }

  resize = () => this.forceUpdate()

  componentDidMount () {
    window.addEventListener('resize', this.resize)
  }

  componentWillUnmount () {
    window.removeEventListener('resize', this.resize)
  }

  render () {
    const {classes, src, mini, imageUploadCallBack, editImage} = this.props
    const {scale, radius, rotate, hovered, saveButtonDisabled} = this.state
    return (
      <div style={styles.dealImageContainer}>
        <div
          style={mini ? styles.miniUserImage : Object.assign({}, {
            width: (calculateSizeOfImage() + AD),
            height: (calculateSizeOfImage() + AD)
          }, styles.userImage)}
        >
          <div
            onMouseEnter={() => this.setImageHovered(true)}
            onMouseLeave={() => this.setImageHovered(false)}>

            <AvatarEditor
              ref={this.setEditorRef}
              image={!isEmpty(src) ? src : Images.logo}
              width={calculateSizeOfImage()}
              height={calculateSizeOfImage()}
              border={0}
              scale={scale}
              borderRadius={radius}
              rotate={rotate}
              // style={{borderRadius: '50%'}}
            />

            {hovered === true ?
              this.renderOptionForUploadImage()
              : null}
            <input
              ref={(ref) => this.upload = ref}
              accept='image/*'
              style={{display: 'none'}}
              id='contained-button-file-2'
              type='file'
              onClick={(event)=> {
                event.target.value=null
              }}
              onChange={(event) =>{
                imageUploadCallBack(event.target.files)
                event.target.value=null
              }}
            />
          </div>
        </div>
        <Slider
          disabled={!editImage}
          classes={{container: classes.slider}}
          value={scale}
          aria-labelledby='label'
          min={0}
          max={10}
          step={0.1}
          onChange={(event, value) => {this.handleChange(event, value, 'scale')}}
        />
        <Button variant='contained' size='small' color='primary' style={styles.btnContainer} disabled={!editImage}
          onClick={this.handleRotateLeft}>
          {localize('newDealScreen.left')}
        </Button>
        <Button variant='contained' size='small' color='primary' style={styles.btnContainer} disabled={!editImage}
          onClick={this.handleRotateRight}>
          {localize('newDealScreen.right')}
        </Button>
        <Button variant='contained' size='small' color='primary' style={styles.btnContainer}
          disabled={!editImage || saveButtonDisabled}
          onClick={this.onClickSave}>
          {localize('newDealScreen.saveImage')}
        </Button>
      </div>

    )
  }
}

export default withStyles(styles)(AvatarEditorImage)


