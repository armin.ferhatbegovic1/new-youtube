import React from 'react'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import SelectList from '../SelectList/SelectList'
import { getRequest, localize } from '../../../services/index'
import FormLabel from '@material-ui/core/FormLabel'
import { dealsEndpoints } from '../../../config/Endpoints'

const styles = ({
  root: {
    display: 'flex'
  },
  dealDurationSelectList: {
    width: '300px',
    margin: '15px auto'
  },
  dealPrice: {
    width: 150,
    margin: '0 auto',
    fontWeight: 'bold'
  }
})

export default class RenewDealDialog extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      dealFee: null,
      dealFeeSelected: '',
      dealFeeObject: null
    }
    this._isMounted = false
  }

  componentWillUnmount () {
    this._isMounted = false
  }

  async getDealFee () {
    if (this._isMounted) {
      return await getRequest(dealsEndpoints.DEAL_FEE)
    }
  }

  async componentDidMount () {
    this._isMounted = true
    const dealFee = await this.getDealFee()
    this.setState({dealFee})
  }

  handleClose = () => {
    const {close} = this.props
    this.setState({open: false})
    close()
  }

  handleSelectDuration = (event) => {
    const {dealFee} = this.state
    const dealFeeSelected = event.target.value
    this.setState({dealFeeSelected})
    const indexOfChoosedItem = dealFee.findIndex((item) => item.duration === dealFeeSelected)
    this.setState({dealFeeObject: dealFee[indexOfChoosedItem]})
  }

  submitAction () {
    const {submitAction} = this.props
    const {dealFeeObject} = this.state
    submitAction(dealFeeObject)
  }

  checkIsDurationSelected () {
    const {dealFeeObject} = this.state
    return dealFeeObject !== null ? true : false
  }

  render () {
    const {dealFee, dealFeeSelected, dealFeeObject} = this.state
    const {open} = this.props

    return (
      <div>
        <Dialog
          open={open}
          onClose={this.handleClose}
          aria-labelledby='form-dialog-title'
        >
          <DialogTitle id='form-dialog-title'>   {localize('newDealScreen.dealDuration')}</DialogTitle>
          <DialogContent>
            <DialogContentText>
              {localize('newDealScreen.howMuchDaysWillDealBeVisible')}
            </DialogContentText>
            <div style={styles.dealDurationSelectList}>
              {dealFee !== null ? <SelectList valueProperty={dealFeeSelected} fieldForShow='duration'
                onChange={(event) => this.handleSelectDuration(event)}
                label={localize('newDealScreen.selectDealDuration')}
                itemsList={dealFee} errorMessage='errorMessages'
                error={true} /> : null}  </div>
            <div style={styles.dealPrice}>
              {dealFeeObject !== null ? <FormLabel
                component='legend'> {localize('newDealScreen.price')} {dealFeeObject['price']} {dealFeeObject['currency']}</FormLabel> : ''}
            </div>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color='primary'>
              {localize('cancel')}
            </Button>
            <Button onClick={() => {this.submitAction()}} color='primary' disabled={!this.checkIsDurationSelected()}>
              {localize('newDealScreen.pay')}
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    )
  }
}
