import React, { Component } from 'react'
import SelectListMultiple from '../SelectListMultiple/SelecListMultiple'
import { getRequest, isEmpty } from '../../../services/index'
import { dealsEndpoints } from '../../../config/Endpoints'

class SelectListCategories extends Component {
  constructor (props) {
    super(props)
    this.state = {
      categories: [],
      placeholder: this.props.placeholder,
      errorMessage: this.props.errorMessage
    }
  }

  async getCategoriesList () {
    return await getRequest(dealsEndpoints.CATEGORIES)
  }

  async componentDidMount () {
    const categories = await this.getCategoriesList()
    this.setState({categories})
  }

  render () {

    const {categories, errorMessage, required, label, placeholder} = this.state
    const {onChange, handleDelete, error, valueProperty, disabled} = this.props
    return (
      !isEmpty(categories) ? (
        <SelectListMultiple valueProperty={valueProperty} fieldName={'dealCategory'} fieldForShow='name'
          handleChange={onChange} placeholder={placeholder} label={label}
          itemsList={categories} required={required} errorMessage={errorMessage} error={error} disabled={disabled}
          handleDelete={handleDelete} />
      ) : null)
  }
}

export default SelectListCategories