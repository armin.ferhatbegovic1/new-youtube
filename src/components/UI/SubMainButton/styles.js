import RootComponentStyles from '../../Styles/RootComponentStyles'
import Colors from '../../../utils/Colors'
import Dimensions from "../../../utils/Dimensions";

const styles = {
  ...RootComponentStyles,
  button:{
    borderRadius: 10,
    width:Dimensions.doubleBaseMargin*10,
    padding:Dimensions.baseMargin,
    fontWeight: 'bold',
    textTransform:'unset',
    margin:Dimensions.baseMargin
  },
  buttonMini:{
    borderRadius: 20,
    width:Dimensions.doubleBaseMargin*6,
    paddingTop:Dimensions.baseMargin,
    paddingBottom:Dimensions.baseMargin,
    paddingLeft:5,
    paddingRight:5,
    fontWeight: 'bold',
    textTransform:'unset',
    display:'flex',
    margin:Dimensions.baseMargin
  },
  primary: {
    color: Colors.secondary,
    backgroundColor: Colors.gray
  },
  secondary: {
    color: Colors.secondary,
    backgroundColor: Colors.white,
    border: '1px solid #2db1ff',
  },
  tertiary: {
    color: Colors.secondary,
    backgroundColor: Colors.yellow
  }
}

export default styles