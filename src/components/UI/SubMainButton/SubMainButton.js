import React from 'react'
import styles from './styles'
import Button from '@material-ui/core/Button'

const subMainButton = (props) => {
  const {label, disabled, buttonType, type,onClick,mini} = props
  return <Button variant='contained' style={Object.assign({}, mini?styles.buttonMini:styles.button, styles[buttonType])}
    disabled={disabled} type={type ? type : ''} onClick={()=>{onClick()}}>
    {label}
  </Button>
}

export default subMainButton

