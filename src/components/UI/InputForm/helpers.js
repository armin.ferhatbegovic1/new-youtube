import { checkIsBoolean, isEmpty, localize } from '../../../services'
import { alertDialogType, uploadDealImage } from '../../../config/Constants'

export function checkValidity (value, rules, dependenceValue) {
  let isValid = true
  if (!rules) {
    return true
  }
  if (!Array.isArray(value)) {
    if (rules.required !== 'dependence' && rules.required) {
      isValid = value !== null && (checkIsBoolean(value) ? value !== false : value.trim() !== '') && isValid
    }
    if (rules.required === 'dependence') {
      if (rules.hasOwnProperty('value') && rules['dependenceField'].value == dependenceValue) {
        isValid = value !== null && (checkIsBoolean(value) ? value !== false : value.trim() !== '') && isValid
      }
      if (rules['dependenceField'].hasOwnProperty('operation')) {
        if (rules['dependenceField'].operation === 'equal')
          isValid = value === dependenceValue
      }
    }
    if (rules.isEmail) {
      const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/
      isValid = pattern.test(value) && isValid
    }

    if (rules.isNumeric) {
      const pattern = /^\d+$/
      isValid = pattern.test(value) && isValid
    }

    if (typeof value === 'string') {
      if (rules.minLength) {

        isValid = value.length >= rules.minLength && isValid
      }

      if (rules.maxLength) {
        isValid = value.length <= rules.maxLength && isValid
      }
    }

  } else {
    if (rules.required) {
      isValid = value.length !== 0 && isValid
    }
    if (rules.minLength) {
      isValid = value.length >= rules.minLength && isValid
    }

    if (rules.maxLength) {
      isValid = value.length <= rules.maxLength && isValid
    }

  }

  return isValid
}

export function loadDataIntoForm (newForm, newData) {
  for (let formElement in newForm) {

    if (!isEmpty(newData[formElement])) {

      if (formElement === 'image') {
        newForm[formElement].value = newData[formElement].img
      }
      else if (formElement === 'country' || formElement === 'countryOfInterest') {
        newForm[formElement].value = newData[formElement].name
        newForm[formElement].id = newData[formElement]._id
      }
      else if (formElement === 'city' || formElement === 'cityOfInterest') {
        newForm[formElement].value = newData[formElement].cityName
        newForm[formElement].id = newData[formElement]._id
      }
      else if (formElement === 'category' || formElement === 'categories' || formElement === 'categoriesOfInterest') {
        newForm[formElement].value = newData[formElement]
      }
      else if (formElement === 'worldwide') {
        newForm[formElement].value = newData[formElement]
      }else if (formElement === 'audio_url') {
        console.log(" newData[formElement].audio_url ",newData[formElement].audio_url)
        newForm[formElement].value = newData[formElement].audio_url
      }else if (formElement === 'text') {
        newForm[formElement].value = newData[formElement].toString().trim()
      }
      else {
        newForm[formElement].value = newData[formElement].toString()
      }
    }
  }
  return newForm
}

export function checkSizeOfUploadedImage (image) {
  const {width, height} = image
  if (uploadDealImage.MIN_UPLOAD_DEAL_IMAGE_WIDTH < width && uploadDealImage.MIN_UPLOAD_DEAL_IMAGE_HEIGHT < height)
    return true
  return false
}

export function imageUpload (acceptedFiles, that, formElement) {
  const {setAlertDialog} = that.props
  if (acceptedFiles.length !== 0) {
    const file = acceptedFiles[0]
    const img = new Image()
    let reader = new FileReader()
    reader.readAsDataURL(file)
    reader.onload = () => {
      img.src = reader.result
    }

    reader.onloadend = () => {

        that.inputChangedHandler({value:file,valueImg:reader.result}, formElement)

    }
  }
}
