import RootComponentStyles from '../../Styles/RootComponentStyles'
import Dimensions from '../../../utils/Dimensions'
import Colors from '../../../utils/Colors'
import { deviceType } from '../../../config/Constants'

const styles = {
  ...RootComponentStyles,
  formContainer: {
    flexDirection: 'column',
    paddingLeft: Dimensions.tripeBaseMargin + 20,
    paddingRight: Dimensions.tripeBaseMargin + 20,
    paddingTop: Dimensions.doubleBaseMargin * 2,
    paddingBottom: Dimensions.doubleBaseMargin * 2,
    minWidth:290
  },
  fieldContainer: {
    display: 'flex'
    //   marginBottom: 5
  },
  submitButton: {
    display: 'flex',
    justifyContent: 'center',

    //  marginBottom: Dimensions.doubleBaseMargin
  },
  editButton: {
    display: 'flex',
    justifyContent: 'flex-end',

    // marginBottom: Dimensions.doubleBaseMargin
  },
  formElementFieldCenter: {
    /* alignItems: 'center',*/
    flexDirection: 'column',
    display: 'flex',
    justifyContent: 'space-around'
  },
  countryCityContainer: {
    display: 'flex',
    color: Colors.secondary,
    fontWeight: 'bold',
    fontStyle: 'italic'
  },
  countryCityField: {
    maxWidth: Dimensions.inputFields.inputXXLarge * 2
  },
  uploadImageContainer: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  alignSelfCenter: {
    alignSelf: 'center'
  },
  uploadImageButton: {
    marginTop: 20
  },
  propertyValueField: {
    color: Colors.secondary,
    fontWeight:'bold',
    fontStyle: 'italic',
    fontSize: Dimensions.getDevice() === deviceType.LARGE_DESKTOP ? Dimensions.textSmall : Dimensions.textXXXSmall
  },
  formItem:{
    marginTop:Dimensions.baseMargin,
    marginBottom:Dimensions.baseMargin
  }
}

export default styles
