import React, { Component } from 'react'
import { checkValidity, imageUpload, loadDataIntoForm } from './helpers'
import CountryAutoSuggest from '../CountryAutoSuggest/CountryAutoSuggest'
import SelectListCategories from '../SelectListCategories/SelectListCategories'
import styles from './styles'
import FormHelperText from '@material-ui/core/FormHelperText/FormHelperText'
import {
  deviceType,
  formMode as formType,
  formMode,
  inputTypeFormElement,
  layoutType,
  positionButton
} from '../../../config/Constants'
import InputLabel from '@material-ui/core/InputLabel/InputLabel'
import Input from '@material-ui/core/Input/Input'
import FormControl from '@material-ui/core/FormControl/FormControl'
import MainButton from '../MainButton/MainButton'
import { awaitFunction, isEmpty, localize } from '../../../services'
import Checkbox from '@material-ui/core/Checkbox/Checkbox'
import FormControlLabel from '@material-ui/core/FormControlLabel/FormControlLabel'
import Switch from '@material-ui/core/Switch/Switch'
import CityAutoSuggest from '../CityAutoSuggest/CityAutoSuggest'
import Grid from '@material-ui/core/Grid/Grid'
import UserImage from '../../UserImage/UserImage'
import MusicImage from '../../MusicImage/MusicImage'
import ButtonIcon from '../ButtonIcon/ButtonIcon'
import EditIcon from '@material-ui/icons/Edit'
import OutlinedInput from '@material-ui/core/OutlinedInput/OutlinedInput'
import * as ReactDOM from 'react-dom'
import SelectList from '../SelectList/SelectList'
import AvatarEditor from '../AvatarEditorImage/AvatarEditorImage'
import Button from '@material-ui/core/Button/Button'
import { setToastNotification } from '../../../store/actions/toastAction'
import { setAlertDialog } from '../../../store/actions/alertDialogAction'
import { setFormDialog } from '../../../store/actions/formDialogAction'
import connect from 'react-redux/es/connect/connect'
import SubMainButton from '../SubMainButton/SubMainButton'
import Dimensions from '../../../utils/Dimensions'

class InputForm extends Component {
  constructor (props) {
    super(props)
    this.state = {
      // form: {...props.formElements},
      layout: !isEmpty(props.layout) ? props.layout : layoutType.COLUMNS_2,
      lastElementInLayout: !isEmpty(props.lastElementInLayout) ? props.lastElementInLayout : layoutType.COLUMNS_1,
      saveButtonPosition: !isEmpty(props.saveButtonPosition) ? props.saveButtonPosition : positionButton.CENTER
    }
  }

  componentWillMount () {

    const {mode, data, formElements} = this.props
    if (mode !== formMode.NEW && !isEmpty(data)) {
      const form = loadDataIntoForm(formElements, data)
      this.setState({form})
    }
  }

  componentWillUnmount () {
    window.removeEventListener('resize', this.resize)
  }

  componentDidMount () {
    const {data} = this.props
    if (!isEmpty(data)) {
      const countryCode = this.getCountryCode()
      this.setCountryCode(countryCode)
    }
    window.addEventListener('resize', this.resize)
  }

  getCountryCode () {
    const {data} = this.props
    if (!isEmpty(data['countryOfInterest']) || !isEmpty(data['country'])) {
      let countryCode = !isEmpty(data['countryOfInterest']) ? data['countryOfInterest'] : data['country']
      return countryCode['countryIso']
    }
  }

  handleDeleteCategories (data1, data2) {
    const newValue = []
    const a = data2.value
    for (let item in data2.value) {
      if (a[item].name !== data1) {
        newValue.push(a[item])
      }
    }
    this.inputChangedHandler(newValue, data2)
  }

  inputChangedHandler = (value, inputIdentifier, id = null) => {
    const newForm = {
      ...this.props.formElements
    }
    const updatedFormElement = inputIdentifier
    updatedFormElement.value = value

    if (id !== null) {
      updatedFormElement.id = id
    }

    let dependenceField
    if (!isEmpty(updatedFormElement.validation['dependenceField'])) {

      const fieldName = updatedFormElement.validation['dependenceField'].fieldName
      dependenceField = newForm[fieldName]
      const dependenceValue = dependenceField['value']

      updatedFormElement.valid = checkValidity(updatedFormElement.value, updatedFormElement.validation, dependenceValue)
    } else {
      updatedFormElement.valid = checkValidity(updatedFormElement.value, updatedFormElement.validation)
    }

    //  newLoginForm[inputIdentifier] = updatedFormElement
    let formIsValid = true
    for (let inputIdentifier in newForm) {
      formIsValid = newForm[inputIdentifier].valid && formIsValid
    }
    newForm['formIsValid'] = formIsValid
    this.setState({newForm})
  }

  handleCountry = async (dealCountry, formField) => {
    await this.resetCityField()
    this.inputChangedHandler(dealCountry.name, formField, dealCountry._id)
    this.setCountryCode(dealCountry.countryIso)
  }

  resetCityField = async () => {
    const form = this.props.formElements
    let city = !isEmpty(form['city']) ? form['city'] : form['cityOfInterest']
    this.inputChangedHandler('', city, city['_id'])
  }

  handleCheckBox = (field) => {
    this.inputChangedHandler(!field['value'], field)
  }

  setCountryCode = async (countryIso) => {
    await awaitFunction(this.setState({countryCode: countryIso}))
  }

  getUploadImageButton (formField) {
    return <div style={styles.uploadImageButton}>
      <label htmlFor='contained-button-file'>
        <Button variant='contained' component='span'
        >
          {formField['label']}
        </Button>
        <FormControl error={!formField['valid']}>
          {!formField['valid'] ? <FormHelperText
            id='component-error-text'>{formField['errorMessages']}</FormHelperText> : null}
        </FormControl>
      </label></div>
  }

  checkImgExist(formElement){
   return  !isEmpty(formElement)&&(formElement.hasOwnProperty('valueImg'))?formElement.valueImg:formElement
  }

  validateAllFields () {
    const newForm = {
      ...this.props.formElements
    }
    let formIsValid = true
    for (let formElement in newForm) {
      let dependenceField
      if (!isEmpty(newForm[formElement].validation['dependenceField'])) {
        dependenceField = newForm[formElement].validation['dependenceField']
        const dependenceValue = newForm[dependenceField['fieldName']]
        newForm[formElement].valid = checkValidity(newForm[formElement].value, newForm[formElement].validation, dependenceValue['value'])

      } else {
        newForm[formElement].valid = checkValidity(newForm[formElement].value, newForm[formElement].validation)
      }
      formIsValid = newForm[formElement].valid & formIsValid
    }
    newForm['formIsValid'] = formIsValid
    this.setState({newForm})
    return formIsValid
  }

  getElementVisibility (dependecies) {
    if (dependecies === false) return false
    if (!isEmpty(dependecies)) {
      const dependenceField = dependecies['dependenceField']
      const form = this.props.formElements
      const realField = form[dependenceField.fieldName]
      return realField.value.toString() !== dependenceField.value.toString()
    } else
      return true
  }

  resize = () => this.forceUpdate()

  visibilityElementRender (element) {
    if (Dimensions.getDevice() === deviceType.PHONE) {
      return {display: this.getElementVisibility(element.visibility) ? 'block' : 'none'}
    } else {
      return {visibility: this.getElementVisibility(element.visibility) ? 'visible' : 'hidden'}
    }
  }

  renderInputElementRow () {
    const {layout, lastElementInLayout} = this.state
    const form = this.props.formElements
    const {rowCount} = this.props

    const addMarginToElement = Dimensions.getDevice() === deviceType.TABLET || Dimensions.getDevice() === deviceType.PHONE
    const numberOfFormElements = Object.keys(form).length
    let layoutForm = Object.entries(form).map(([ix, i], index) => {
      // if (tmpIndex === numberOfFormElements && (layout===layoutType.COLUMNS_2_1? tmpIndex % 2 !== 0:true)) {
      return (index + 1) === numberOfFormElements ?
        <Grid item xs={12} sm={lastElementInLayout} key={ix}
          style={{
            alignSelf: 'center',
            visibility: this.getElementVisibility(form[ix].visibility) ? 'visible' : 'hidden'
          }}>
          <div style={styles.formElementFieldCenter}>{this.renderInputElement(form[ix], ix)}</div>
        </Grid> : <Grid item xs={12} sm={layout} key={ix}
          style={Object.assign({}, addMarginToElement ? styles.formItem : null, this.visibilityElementRender(form[ix]), !isEmpty(rowCount) ? null : styles.alignSelfCenter)}>
          {this.renderInputElement(form[ix], ix)}</Grid>
    })
    if (!isEmpty(rowCount)) {
      let tmpArray = []
      for (let i = 0; i < layoutForm.length; i = i + rowCount) {
        tmpArray.push(<Grid item xs={12} sm={12} md={4} key={i}
          style={styles.formElementFieldCenter}>{layoutForm.slice(i, i + rowCount)}</Grid>)
      }
      layoutForm = tmpArray
    }

    return <div style={styles.renderInputRowElements}>
      <Grid container spacing={24}>
        {layoutForm}
      </Grid></div>
  }

  renderInputElement (formField, ix) {
    const {mode} = this.props
    const disabledField = !isEmpty(formField['disabled']) ? formField['disabled'] : false

    const isFieldDisabled = mode === formType.VIEW ? true : disabledField

    if (formField.inputType === inputTypeFormElement.COUNTRY_AUTO_SUGGEST) {
      return <div style={styles.countryCityField}>
        <CountryAutoSuggest placeholder={formField['placeholder']} value={formField['value']}
          label={formField['label']} onChange={(value) => {this.handleCountry(value, formField)}}
          style={mode === formType.VIEW ? styles.propertyValueField : null}
          errorMessage={formField['errorMessages']} disabled={isFieldDisabled}
          error={formField['valid']} key={ix} />
      </div>
    }
    if (formField.inputType === inputTypeFormElement.SELECT_LIST_CATEGORIES) {
      return <SelectListCategories valueProperty={formField['value']}
        onChange={(event) => this.inputChangedHandler(event.target.value, formField)}
        placeholder={formField['placeholder']}
        label={formField['label']}
        errorMessage={formField['errorMessages']}
        disabled={isFieldDisabled}
        error={formField['valid']} handleDelete={(value1) => () => {this.handleDeleteCategories(value1, formField)}}
        key={ix} />
    }
    if (formField.inputType === inputTypeFormElement.CHECKBOX) {
      return <FormControl error={!formField['valid']}
        style={Object.assign({}, {alignItems: 'center'}, styles.fieldContainer)} key={ix}>
        <FormControlLabel
          style={styles.checkBoxContainer}
          control={
            <Checkbox
              checked={formField['value']}
              onChange={() => {this.handleCheckBox(formField)}}
              value={ix}
              style={styles.checkBoxSecondary}
            />
          }
          label={formField['label']}
        />
        {!formField['valid'] ? <FormHelperText
          id='component-error-text'>{formField['errorMessages']}</FormHelperText> : null}
      </FormControl>
    }
    if (formField.inputType === inputTypeFormElement.SWITCH) {
      let switchValue = formField['value']
      if (typeof formField['value'] === 'string' || formField['value'] instanceof String) {
        switchValue = (formField['value'].toString() === 'true')
      }

      return <FormControl error={!formField['valid']} style={Object.assign({}, styles.fieldContainer)}
        key={ix}>
        <FormControlLabel
          style={styles.checkBoxContainer}
          control={
            <Switch
              disabled={isFieldDisabled}
              checked={switchValue}
              onChange={() => {this.handleCheckBox(formField)}}
              value={ix}
              color='primary'
            />
          }
          label={formField['label']}
        />
        {!formField['valid'] ? <FormHelperText
          id='component-error-text'>{formField['errorMessages']}</FormHelperText> : null}
      </FormControl>
    }
    if (formField.inputType === inputTypeFormElement.TEXT_AREA) {
      return <FormControl error={!formField['valid']} style={styles.fieldContainer} key={ix} variant='outlined'>
        <InputLabel ref={ref => {
          this.labelRef = ReactDOM.findDOMNode(ref)
        }}
          htmlFor='component-outlined'>{formField['label']}</InputLabel>
        <OutlinedInput labelWidth={this.labelRef ? this.labelRef.offsetWidth : 0}
          htmlFor='component-outlined' id={'component-' + ix} autoComplete={ix}
          value={formField['value']}
          multiline={true}
          type={formField['type'] === 'password' ? 'password' : ''}
          disabled={isFieldDisabled}
          style={mode === formType.VIEW ? styles.propertyValueField : null}
          onChange={(event) => this.inputChangedHandler(event.target.value, formField)} />
        {!formField['valid'] ? <FormHelperText
          id='component-error-text'>{formField['errorMessages']}</FormHelperText> : null}
      </FormControl>
    }
    if (formField.inputType === inputTypeFormElement.CITY_AUTO_SUGGEST) {
      return <div style={styles.countryCityField}><CityAutoSuggest countryCode={this.state.countryCode}
        placeholder={formField['placeholder']} label={formField['label']}
        onChange={(suggestion) => this.inputChangedHandler(suggestion['cityName'], formField, suggestion['_id'])}
        isValid={true} style={mode === formType.VIEW ? styles.propertyValueField : null}
        disabled={isFieldDisabled}
        errorMessage={formField['errorMessages']}
        error={formField['valid']} value={formField['value']} /></div>
    }
    if (formField.inputType === inputTypeFormElement.SELECT_LIST) {
      return <SelectList valueProperty={formField['value']}
        onChange={(event) => this.inputChangedHandler(event.target.value, formField)}
        label={formField['label']}
        style={mode === formType.VIEW ? styles.propertyValueField : null}
        disabled={isFieldDisabled}
        itemsList={formField['itemsList']} errorMessage={formField['errorMessages']}
        error={formField['valid']} />
    }
    if (formField.inputType === inputTypeFormElement.IMAGE) {
      return <UserImage src={this.checkImgExist(formField['value'])} editImage={!(isFieldDisabled)}
        imageUploadCallBack={(file) => {imageUpload(file, this, formField)}} />
    }
    if (formField.inputType === inputTypeFormElement.MUSIC) {
      return <MusicImage src={this.checkImgExist(formField['value'])} editImage={!(isFieldDisabled)}
        imageUploadCallBack={(file) => {imageUpload(file, this, formField)}}  value={formField['value']}/>
    }
    if (formField.inputType === inputTypeFormElement.MAIN_BUTTON) {
      return <SubMainButton label={formField['label']} buttonType='secondary'
        type='button' onClick={() => {formField.actionCallBack()}} />
    }

    if (formField.inputType === inputTypeFormElement.UPLOAD_EDIT_IMAGE) {
      return <FormControl error={!formField['valid']} style={styles.fieldContainer} key={ix}>
        <div style={styles.uploadImageContainer}>
          {<AvatarEditor src={formField['value']} editImage={!(isFieldDisabled)}
            imageUploadCallBack={(file) => {imageUpload(file, this, formField)}}
            handleSaveImage={(img) => { this.inputChangedHandler(img, formField)}} />}
          {!formField['valid'] ? <FormHelperText
            id='component-error-text'>{formField['errorMessages']}</FormHelperText> : null}
        </div>
      </FormControl>

    } else
      return <FormControl error={!formField['valid']} style={styles.fieldContainer} key={ix}>
        <InputLabel htmlFor='component-simple'>{formField['label']}</InputLabel>
        <Input id={'component-' + ix} autoComplete={ix} value={formField['value']} multiline={formField['multiline']}
          type={formField['type'] === 'password' ? 'password' : ''}
          disabled={isFieldDisabled}
          style={Object.assign({}, mode === formType.VIEW ? styles.propertyValueField : null, formField['multiline'] ? {marginBottom: Dimensions.doubleBaseMargin} : null)}
          onChange={(event) => this.inputChangedHandler(event.target.value, formField)} />
        {!formField['valid'] ? <FormHelperText
          id='component-error-text'>{formField['errorMessages']}</FormHelperText> : null}
      </FormControl>

  }

  formHandler = (event) => {
    const {prepareDataAndSubmit} = this.props
    event.preventDefault()
    if (this.validateAllFields()) {
      prepareDataAndSubmit()
    }
  }

  formHandlerAdvanced = () => {
    const {prepareDataAndSubmitAdvanced} = this.props
    if (this.validateAllFields()) {
      prepareDataAndSubmitAdvanced()
    }
  }

  renderAdvancedButton () {
    const {advanceButtonLabel, prepareDataAndSubmitAdvanced} = this.props
    if (!isEmpty(advanceButtonLabel) && !isEmpty(prepareDataAndSubmitAdvanced))
      return <SubMainButton label={advanceButtonLabel} buttonType='secondary' editable={true}
        onClick={() => this.formHandlerAdvanced()} />
  }

  render () {
    const {mode, editable, editFormButton, buttonLabel, formContainer, children} = this.props
    const {saveButtonPosition, layout} = this.state
    return (
      <form style={isEmpty(formContainer) ? styles.formContainer : {...formContainer}} onSubmit={this.formHandler}>
        {
          this.renderInputElementRow()
        }
        <div
          style={Object.assign({}, styles.submitButton, layout == layoutType.COLUMNS_1 ? {marginTop: Dimensions.doubleBaseMargin * 1.5} : {marginTop:20}, {justifyContent: saveButtonPosition})}>
          {children}
          {mode !== formMode.VIEW ?
            <MainButton label={!isEmpty(buttonLabel) ? buttonLabel : localize('save')} buttonType='primary'
              type='submit' /> : null}
          {mode !== formMode.VIEW ?
            <div>{this.renderAdvancedButton()}</div>
            : null}
        </div>

        {editable && mode !== formMode.EDIT && mode !== formMode.NEW ? <div style={styles.editButton}>
          <ButtonIcon label={localize('submit')} buttonType='primary' type='submit'
            handleAction={() => {editFormButton()}}>
            <EditIcon />
          </ButtonIcon>

        </div> : null}

      </form>)
  }

}

export default connect(null, {setToastNotification, setAlertDialog, setFormDialog})((InputForm))

