import React from 'react'
import FormControl from '@material-ui/core/FormControl'
import MenuItem from '@material-ui/core/MenuItem'
import Select from '@material-ui/core/Select/Select'
import InputLabel from '@material-ui/core/InputLabel'
import FormHelperText from '@material-ui/core/FormHelperText'
import styles from './styles'

const selectList = (props) => {
  const {label, valueProperty, itemsList, onChange, error, errorMessage, fieldForShow,disabled,style} = props
  return (
    <FormControl error={!error} style={styles.selectList}>
      <InputLabel htmlFor='age-simple'>{label}</InputLabel>
      <Select
        value={valueProperty}
        onChange={onChange}
        disabled={disabled}
        style={style}
      >
        {itemsList.map((name, value) => {
          const fieldValue = fieldForShow ? name[fieldForShow] : name
          return <MenuItem key={value} value={fieldValue}>
            {fieldValue}
          </MenuItem>
        })}
      </Select>
      {!error ? <FormHelperText id='component-error-text'>{errorMessage}</FormHelperText> : ''}
    </FormControl>
  )
}

export default selectList