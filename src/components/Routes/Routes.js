import SettingScreen from '../../containers/SettingScreen/SettingScreen'
import NotFound from '../NotFound/NotFound'
import LoginScreen from '../../containers/LoginScreen/LoginScreen'
import {LayoutType, role} from '../../config/Constants'
import {BrowserRouter as Router, Switch} from 'react-router-dom'
import React from 'react'
import Dashboard from '../Dashboard/Dashboard'
import Home from '../Home/Home'
import EmptyRoute from '../EmptyRoute/EmptyRoute'
import {isEmpty} from '../../services'
import AddNewVideoScreen from '../../containers/AddNewVideoScreen/AddNewVideoScreen'
import VideoListScreen from '../../containers/VideoListScreen/VideoListScreen'
import RedirectionScreen from '../../containers/Redirection/Redirection'
import PreviewVideoScreen from '../../containers/PreviewVideoScreen/PreviewVideoScreen'
import EditUploadVideo from "../EditVideo/EditUploadVideo";
import EditVideoScreen from "../../containers/EditVideoScreen/EditVideoScreen";

const routesList = [
    {
        path: '/',
        exact: true,
        component: LoginScreen,
        layoutType: LayoutType.empty,
        authenticate: false
    },
    {
        path: '/',
        exact: true,
        component: VideoListScreen,
        layoutType: LayoutType.dashboard,
        authenticate: true,
        role: [role.ADMIN]
    },
    {
        path: '/login',
        exact: false,
        component: LoginScreen,
        layoutType: LayoutType.empty,
        authenticate: false
    },
    {
        path: '/setting',
        exact: false,
        component: SettingScreen,
        layoutType: LayoutType.dashboard,
        authenticate: true,
        role: [role.ADMIN]
    },
    {
        path: '/new-video',
        component: AddNewVideoScreen,
        layoutType: LayoutType.dashboard,
        authenticate: true,
        role: [role.ADMIN]
    },
    {
        path: '/edit-video',
        component: EditVideoScreen,
        layoutType: LayoutType.dashboard,
        authenticate: true,
        role: [role.ADMIN]
    },
    {
        path: '/video-list',
        component: VideoListScreen,
        layoutType: LayoutType.dashboard,
        authenticate: true,
        role: [role.ADMIN]
    },
    {
        path: '/preview-video',
        component: PreviewVideoScreen,
        layoutType: LayoutType.dashboard,
        authenticate: true,
        role: [role.ADMIN]
    },
    {
        path: '/redirect_app',
        component: RedirectionScreen,
        layoutType: LayoutType.empty,
        authenticate: true,
        role: [role.ADMIN]
    },
    {
        path: '/redirect_app',
        component: RedirectionScreen,
        layoutType: LayoutType.empty,
        authenticate: false,
        role: []
    },
    {
        component: NotFound,
        layoutType: LayoutType.empty,
        authenticate: false,
        role: [role.ADMIN]
    },
    {
        component: NotFound,
        layoutType: LayoutType.empty,
        authenticate: true,
        role: [role.ADMIN]
    }
]

function getLayoutByType(route, index, authenticate) {
    switch (route.layoutType) {
        case LayoutType.empty:
            return <EmptyRoute key={index} path={route.path} exact={route.exact} component={route.component}/>
        case LayoutType.dashboard:
            return <Dashboard key={index} path={route.path} exact={route.exact} component={route.component}/>
        case LayoutType.home:
            return <Home key={index} path={route.path} exact={route.exact} component={route.component}/>
        default:
            return null
    }
}

function getPage(route, index, authenticate) {
    const user = authenticate['user']
    if (!isEmpty(route.role) && !isEmpty(user) && route.role.indexOf(user.role) !== -1) {
        return getLayoutByType(route, index, authenticate)
    } else if (isEmpty(route.role) && isEmpty(user) && route.authenticate === false) {
        return getLayoutByType(route, index, authenticate)
    } else return null
}

const RoutesList = (props) => {
    const {authenticate} = props
    return (
        <Router>
            <Switch>
                {routesList.map((route, index) => (
                        getPage(route, index, authenticate)
                    )
                )}
            </Switch>
        </Router>
    )
}

export default RoutesList
