import RootComponentStyles from '../Styles/RootComponentStyles'
import Colors from '../../utils/Colors'
import Dimensions from '../../utils/Dimensions'

const styles = {
    ...RootComponentStyles,
    videoContainer: {
        border: '4px solid ' + Colors.yellow,
        boxShadow: '5px 5px 5px 5px' + Colors.color5,
        maxWidth: Dimensions.videoWidth,
        maxHeight: Dimensions.videoHeight,
        marginTop: Dimensions.doubleBaseMargin * 2,
        marginBottom: Dimensions.doubleBaseMargin * 2,
        backgroundColor: '#DDDDDD',
    },
  videoContainer2: {
    border: '4px solid ' + Colors.yellow,
    boxShadow: '5px 5px 5px 5px' + Colors.color5,
    maxWidth: Dimensions.videoWidth,
    maxHeight: Dimensions.videoHeight,
    marginTop: Dimensions.doubleBaseMargin * 2,
    backgroundColor: '#DDDDDD',
  },
    videoPlaceholder: {
        maxWidth: Dimensions.videoWidth - 10,
        maxHeight: Dimensions.videoHeight - 10,
        backgroundColor: '#DDDDDD',
        width: '100%',
        height: 'auto'
    },
    removeVideo: {
        alignSelf: 'flex-end',
        marginLeft: '16'
    },
    playerControls: {
        display: 'flex',
        justifyContent: 'flex-end',
        height: 50
    },
    btnContainer: {
        margin: 10,
        color: 'white'
    },
    videoContainerUpload: {
        display: 'flex',
        //justifyContent: 'center'
    }
    ,
    saveVideoButtonContainer: {
        display: 'flex',
      flexDirection: 'row',
      justifyContent: 'center'}
}

export default styles
