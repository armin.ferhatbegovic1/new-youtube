import React, { Component } from 'react'
import { videos } from '../../config/Endpoints'
import SubMainButton from '../UI/SubMainButton/SubMainButton'
import { localize } from '../../services'

export default class UploadImage extends Component {

  state = {
    loading: false,
    uploading: false,
    images: []
  }

  componentDidMount() {
  }


  onChange = e => {
    const errs = []
    const files = Array.from(e.target.files)

    const formData = new FormData()
    const types = ['image/png', 'image/jpeg', 'image/gif']

    files.forEach((file, i) => {

      if (types.every(type => file.type !== type)) {
        errs.push(`'${file.type}' is not a supported format`)
      }
      if (file.size > 150000) {
        errs.push(`'${file.name}' is too large, please pick a smaller file`)
      }
      formData.append('file', file)
    })

    if (errs.length) {
     console.log(errs)
    }

    this.setState({ uploading: true })
    formData.append('belongs_to', 'video.cover')
    formData.append('public', true)
    fetch(videos.FILE_UPLOAD, {
      method: 'POST',
      body: formData,
      headers: {
        'X-Auth-Token': localStorage.getItem('jwtToken')
      }
    })
    .then(res => {
      if (!res.ok) {
        throw res
      }
      return res.json()
    })
    .then(images => {
      this.setState({
        uploading: false,
        images
      })
    })
    .catch(err => {
      err.json().then(e => {
      console.log("e",e)
        this.setState({ uploading: false })
      })
    })
  }

  filter = id => {
    return this.state.images.filter(image => image.public_id !== id)
  }

  removeImage = id => {
    this.setState({ images: this.filter(id) })
  }

  onError = id => {
    console.log('Oops, something went wrong')
    this.setState({ images: this.filter(id) })
  }

  render() {
    const { loading, uploading, images } = this.state

    const content = () => {
      switch(true) {
        case loading:
          return <div>loading</div>
        case uploading:
          return <div>uploading</div>
        case images.length > 0:
          return <img src={images[0]} alt='upload cover'/>
        default:
          return   <SubMainButton label='Upload Image' buttonType='tertiary'
            type='button' onClick={this.onChange} />
      }
    }

    return (
      <div className='container'>
          {content()}
        <input type='file' id='multi' onChange={this.onChange} multiple />
      </div>
    )
  }
}
