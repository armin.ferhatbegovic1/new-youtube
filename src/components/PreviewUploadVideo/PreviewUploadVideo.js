/** @flow */
import React, {Component, Fragment} from 'react'
import {awaitFunction, getRequest, isEmpty, localize, postRequest, prepareDataFormForSubmitAction} from '../../services'
import styles from './styles'
import MainButton from '../../components/UI/MainButton/MainButton'
import ReactPlayer from 'react-player'
import Images from '../../utils/Images'
import {uploadVideoForm, videoCoverForm} from '../../forms/uploadVideo'
import {
    alertDialogType,
    formMode,
    layoutType,
    toastNotification,
    toastType,
    requestHeader
} from '../../config/Constants'
import InputForm from '../UI/InputForm/InputForm'
import Grid from '@material-ui/core/Grid/Grid'
import ButtonWithIcon from '../UI/ButtonIcon/ButtonIcon'
import DeleteIcon from '@material-ui/icons/Delete'
import AxiosInstance from '../../services/Axios'
import Button from '@material-ui/core/Button/Button'
import {setToastNotification} from '../../store/actions/toastAction'
import connect from 'react-redux/es/connect/connect'
import Dimensions from '../../utils/Dimensions'
import UploadAudioFiles from './UploadAudioFiles'
import SubMainButton from '../UI/SubMainButton/SubMainButton'
import {setAlertDialog} from '../../store/actions/alertDialogAction'
import {videos} from '../../config/Endpoints'
import {patchRequest} from "../../services/RequestManager/RequestManager";
import config from "../../config/config";


class PreviewUploadVideo extends Component {
    constructor(props) {
        super(props)
        this.state = {
            title: '',
            file: null,
            video: null,
            video2: null,
            url: null,
            pip: false,
            playing: false,
            playing2: false,
            controls: false,
            light: false,
            volume: 0.8,
            muted: false,
            played: 0,
            loaded: 0,
            duration: 0,
            playbackRate: 1.0,
            loop: false,
            message: null,
            uploadVideoForm: JSON.parse(JSON.stringify(uploadVideoForm)),
            videoCoverForm: JSON.parse(JSON.stringify(videoCoverForm)),
            imageUploadResponse: null,
            videoUploadResponse: null,
            audioSetienceNumber: 0,
            audioSetienceData: [],
            savedVideo: false,
            newVideoUuid: null
        }
    }

    uploadVideoFileAndData(type) {
        const {file, imageUploadResponse} = this.state
        const {setAlertDialog} = this.props

        if (isEmpty(imageUploadResponse)) {
            setAlertDialog(true, localize('videoImageCoverIsNotUploaded'), localize('videoUploadWarning'), alertDialogType.WARNING)

        } else if (file === null) {
            setAlertDialog(true, localize('videoIsNotUploaded'), localize('videoUploadWarning'), alertDialogType.WARNING)
        } else {
            this.fileUpload(file).then(async (response) => {
                await this.videoRecordUpload(response, type)
            })
        }
    }

    onFormSubmit = (e) => {
        e.preventDefault() // Stop form submit
        this.uploadVideoFileAndData('post')
    }

    saveOriginalVideo() {
        this.uploadVideoFileAndData('patch')
    }


    addAudioSetienceNumber = () => {
        const {audioSetienceNumber} = this.state
        let newValue = audioSetienceNumber + 1
        this.setState({audioSetienceNumber: newValue})
    }
    onChange = (e) => {
        awaitFunction(this.setState({file: e.target.files[0], video: URL.createObjectURL(e.target.files[0])}))
    }

    fileUpload = async (file) => {
        const formData = new FormData()
        formData.append('file', file)
        formData.append('belongs_to', 'video.video')
        formData.append('public', true)
        return await AxiosInstance.post(videos.FILE_UPLOAD, formData, {
            headers: requestHeader()
        })
    }

    imageUpload = async (file) => {
        const formData = new FormData()
        const {videoCoverForm} = this.state
        const {setToastNotification} = this.props
        const uploadVideo = prepareDataFormForSubmitAction(videoCoverForm)
        if (isEmpty(uploadVideo['videoCover'])) {
            setToastNotification(true, 'You didnt add any cover image')
            return
        } else {

            formData.append('belongs_to', 'video.cover')
            formData.append('public', true)
            formData.append('file', uploadVideo['videoCover'].value)
            await AxiosInstance.post(videos.FILE_UPLOAD, formData, {headers: requestHeader()}).then(reponse => {
                let reponseMessage = ''
                let type = toastType.SUCCESS
                if (reponse.data.success === true && reponse.status === 200) {
                    reponseMessage = 'Cover Image is successful uploaded'
                    this.setState({imageUploadResponse: reponse})
                }
                if (reponse.data.success !== true || reponse.status !== 200) {
                    reponseMessage = 'Cover Image uploaded is failed' + reponse.data
                    type = toastType.FAILURE
                }

                setToastNotification(true, reponseMessage, type)
            })
        }
    }

    videoRecordUpload = async (response, type) => {
        const {uploadVideoForm, imageUploadResponse, audioSetienceData} = this.state
        const uploadVideo = prepareDataFormForSubmitAction(uploadVideoForm)

        let dataVideoRecord = {
            ...uploadVideo,
            'cover_file_uuid': imageUploadResponse['data'].object_uuid,
            'video_file_uuid': response.data['object_uuid']
        }
        if (audioSetienceData.length !== 0) {
            dataVideoRecord['sentences'] = audioSetienceData
        }
        const videoDataResponse = type === 'post' ? await postRequest('/video', dataVideoRecord, toastNotification.SHOW) :
            await patchRequest('/video/' + this.state.newVideoUuid, dataVideoRecord, toastNotification.SHOW)
        await awaitFunction(this.setState({
            savedVideo: true,
            newVideoFileUuid: dataVideoRecord['video_file_uuid'],
            newVideoUuid: videoDataResponse['object_uuid']
        }))
    }

    submitUploadVideoForm = () => {
        this.clickUploadBtn()
    }

    deleteVideo = () => {
        this.setState({video: null, file: null})
    }

    clickUploadBtn() {
        document.querySelector('#uploadBtn button').click()
    }

    playPause = () => {
        this.setState({playing: !this.state.playing})
    }
    stop = () => {
        this.setState({url: null, playing: false})
    }
    stop2 = () => {
        this.setState({playing2: false})
    }
    playPause2 = () => {
        this.setState({playing2: !this.state.playing2})
    }
    optimization = async () => {
        const {savedVideo, newVideoUuid, newVideoFileUuid} = this.state
        if (savedVideo && !isEmpty(newVideoUuid)) {
            const data = await patchRequest('/video/' + newVideoUuid + '/optimize', {}, toastNotification.SHOW)
            if (data['success']) {
                const data = await getRequest(videos.VIDEO_BY_ID + '/' + newVideoUuid)

                this.setState({video2: config.apiEndpoint + '/file/' + newVideoFileUuid})
            }
        } else {
            this.props.setAlertDialog(true, localize('youHaveToSaveVideoFirst'), localize('videoUploadWarning'), alertDialogType.WARNING)
        }
    }
    addAudioDataInArray = async (data, index) => {
        await awaitFunction(this.setState({
            audioSetienceData: this.state.audioSetienceData.concat(data)
        }))
    }

    renderAudioFields = () => {
        const {audioSetienceNumber} = this.state
        if (audioSetienceNumber !== 0) {
            return Array.apply(0, Array(audioSetienceNumber)).map((x, i) => {
                return <UploadAudioFiles key={i} index={i} addAudioData={this.addAudioDataInArray}/>
            })
        }
    }

    renderVideoControls() {
        const {video, playing} = this.state
        return video !== null ?
            <div style={styles.playerControls}>
                <Button variant='contained' size='small' color='primary' style={styles.btnContainer}
                        disabled={!video}
                        onClick={this.optimization}>
                    {localize('optimization')}
                </Button>
                <Button variant='contained' size='small' color='primary' style={styles.btnContainer}
                        disabled={!video}
                        onClick={this.stop}>
                    {localize('stop')}
                </Button>
                <Button variant='contained' size='small' color='primary' style={styles.btnContainer}
                        disabled={!video}
                        onClick={this.playPause}>
                    {playing ? 'Pause' : 'Play'}
                </Button>
                <ButtonWithIcon handleAction={this.deleteVideo} buttonType='primary'>
                    <DeleteIcon/>
                </ButtonWithIcon>
            </div> : null
    }

    renderVideoControls2() {
        const {video2, playing2} = this.state
        return video2 !== null ?
            <div style={styles.playerControls}>
                <Button variant='contained' size='small' color='primary' style={styles.btnContainer}
                        disabled={!video2}
                        onClick={this.stop2}>
                    {localize('stop')}
                </Button>
                <Button variant='contained' size='small' color='primary' style={styles.btnContainer}
                        disabled={!video2}
                        onClick={this.playPause2}>
                    {playing2 ? 'Pause' : 'Play'}
                </Button>
            </div> : null
    }

    renderVideoUploadContainer() {
        const {video, playing} = this.state
        return <div style={styles.videoContainerUpload}>
            <div style={{flexDirection: 'column', display: 'flex', marginLeft: 40}}>
                <div style={styles.videoContainer} id='video-container'> {video !== null ?
                    <div><ReactPlayer url={video}
                                      width={'100%'} height={'auto'}
                                      playing={playing}/>
                    </div>
                    : <img
                        src={Images.videoPlaceholder}
                        alt='placeholder'
                        onClick={() => {
                            this.upload.click()
                        }}
                        style={styles.videoPlaceholder}
                    />}</div>
                {this.renderVideoControls()}
                {this.renderVideoUploadContainerOptimize()}
            </div>

            <div style={{flexDirection: 'column', display: 'flex', justifyContent: 'center'}}>
                {this.renderImageCoverUpload()}
                {this.renderVideoDetails()}

            </div>
        </div>
    }

    renderVideoUploadContainerOptimize() {
        const {video2, playing2} = this.state
        return !isEmpty(video2) ? <div style={styles.videoContainerUpload}>
            <div style={{flexDirection: 'column', display: 'flex', marginLeft: 40}}>
                <div style={styles.videoContainer} id='video-container'> {video2 !== null ?
                    <div><ReactPlayer url={video2}
                                      width={'100%'} height={'auto'}
                                      playing={playing2}/>
                    </div>
                    : <img
                        src={Images.videoPlaceholder}
                        alt='placeholder'
                        onClick={() => {
                            this.upload.click()
                        }}
                        style={styles.videoPlaceholder}
                    />}</div>
                {this.renderVideoControls2()}
            </div>
        </div> : null
    }

    renderVideoDetails() {
        const {uploadVideoForm, message, savedVideo} = this.state
        return <div>
            <form onSubmit={this.onFormSubmit}>
                <input type='file' onChange={this.onChange}
                       style={{display: 'none'}}
                       ref={(ref) => this.upload = ref}
                       id='video-file-2'/>
                <div id='uploadBtn' style={{display: 'none'}}>
                    <MainButton label={localize('uploadVideo')} buttonType='primary'
                                type='submit'/>
                </div>
            </form>
            {savedVideo ? <div style={styles.saveVideoButtonContainer}>
                <SubMainButton label='Save Original Video' buttonType='secondary'
                               type='button' onClick={() => {
                    this.saveOriginalVideo()
                }}/>
                <SubMainButton label='Save Optimized Video' buttonType='secondary'
                               type='button' onClick={() => {
                    this.saveOptimizationVideo()
                }}/>
            </div> : <InputForm formElements={uploadVideoForm} prepareDataAndSubmit={this.submitUploadVideoForm}
                                mode={formMode.NEW} layout={layoutType.COLUMNS_3} buttonLabel='Save Video'>
                {!isEmpty(message) ? <label
                    style={{fontSize: Dimensions.textMedium, alignSelf: 'center'}}>{message}</label> : null}
            </InputForm>}

        </div>
    }

    saveOptimizationVideo = () => {
        const {video2} = this.state
        const {setToastNotification} = this.props
        if (isEmpty(video2)) {
            setToastNotification(true, 'You didnt click button optimization',toastType.FAILURE)
        } else {
            setToastNotification(true, 'You save optimized video successfully',toastType.SUCCESS)
        }
    }

    renderImageCoverUpload() {
        const {videoCoverForm, savedVideo} = this.state
        return <div style={{justifyContent: 'flex-start', display: 'flex'}}>

            <InputForm formElements={videoCoverForm}
                       prepareDataAndSubmit={this.imageUpload}
                       mode={savedVideo ? formMode.VIEW : formMode.NEW}
                       layout={layoutType.COLUMNS_1}
                       lastElementInLayout={layoutType.COLUMNS_2}
                       buttonLabel='Upload Cover'/>
            <div style={{height: 50, alignSelf: 'center'}}>
                {!savedVideo ? <SubMainButton label='Add New Audio' buttonType='primary'
                                              type='button' onClick={() => this.addAudioSetienceNumber()}/> : null}
            </div>
        </div>
    }

    renderVideoUploader() {
        return (
            <Fragment>
                <Grid container spacing={24}>
                    <Grid item xs={12}>
                        {this.renderVideoUploadContainer()}

                    </Grid>
                    <Grid item xs={12}>
                        <div style={{display: 'flex', flexWrap: 'wrap'}}>
                            {this.renderAudioFields()}</div>
                    </Grid>
                </Grid>
            </Fragment>
        )
    }

    render() {
        return (
            this.renderVideoUploader()
        )
    }
}

export default connect(null, {setToastNotification, setAlertDialog})((PreviewUploadVideo))
