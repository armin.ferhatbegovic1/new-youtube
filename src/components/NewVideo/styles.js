import RootComponentStyles from '../Styles/RootComponentStyles'
import Colors from '../../utils/Colors'
import Dimensions from '../../utils/Dimensions'

const styles = {
  ...RootComponentStyles,
  videoContainer: {
    border: '4px solid ' + Colors.yellow,
    boxShadow: '5px 5px 5px 5px' + Colors.color5,
    width: Dimensions.videoWidth,
    height: 370,
    marginTop: Dimensions.doubleBaseMargin * 2,
    marginBottom: Dimensions.doubleBaseMargin * 2,
    backgroundColor: '#DDDDDD'
  },
  videoPlaceholder: {
    width: 641,
    height: 362,
    backgroundColor: '#DDDDDD'
  },
  removeVideo: {
    alignSelf: 'flex-end',
    marginLeft: '16'
  },
  playerControls: {
    display: 'flex',
    justifyContent: 'flex-end',
    height: 50,
    marginTop: 20
  },
  btnContainer: {
    margin: 10,
    color: 'white'
  },
  videoContainerUpload: {
    display: 'flex',
    justifyContent: 'center'
  },
  videoContainer2: {
    border: '4px solid ' + Colors.yellow,
    boxShadow: '5px 5px 5px 5px' + Colors.color5,
    maxWidth: Dimensions.videoWidth,
    maxHeight: Dimensions.videoHeight,
    marginTop: Dimensions.doubleBaseMargin * 2,
    backgroundColor: '#DDDDDD'
  },
  renderVideoUploadContainer:
    {flexDirection: 'row', display: 'flex', margin: 40, flexWrap: 'wrap', width: '100%'},
  videoDetailsContainer:
    {flexDirection: 'column', display: 'flex', flexWrap: 'wrap'},
  videoFormDetails: {maxWidth: 600, width: '100%'},
  saveVideoButtonContainer: {
    // marginTop:30,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center'
  }
}

export default styles
