import React, { Component } from 'react'
import { videos } from '../../config/Endpoints'
import { awaitFunction, isEmpty, prepareDataFormForSubmitAction } from '../../services'
import { audioFileForm } from '../../forms/uploadVideo'
import InputForm from '../UI/InputForm/InputForm'
import { formMode, layoutType, toastNotification, toastType } from '../../config/Constants'
import connect from 'react-redux/es/connect/connect'
import { setToastNotification } from '../../store/actions/toastAction'
import { setAlertDialog } from '../../store/actions/alertDialogAction'
import { patchRequest } from '../../services/RequestManager/RequestManager'
import { uploadFileAction } from '../../containers/EditVideoScreen/helpers'

class UploadAudioFiles extends Component {
  constructor (props) {
    super(props)
    this.state = {
      audioFileForm: JSON.parse(JSON.stringify(audioFileForm)),
      index: props.index,
      addAudioData: props.addAudioData,
      edit: !isEmpty(props.data),
      mode: formMode.VIEW
    }
  }

  async componentWillMount () {
    await this.loadDataIntoForm()
  }

  async loadDataIntoForm () {
    const audioFileForm = {...this.state.audioFileForm}
    audioFileForm['videoCover'].value = this.props.data.audio_file_uuid
    await awaitFunction(this.setState({audioFileForm}))
  }

  checkIsAudioUploaded () {
    const {audioFileForm} = this.state
    let audio = audioFileForm['videoCover'].value
    return !isEmpty(audioFileForm['videoCover'].value['valueImg']) && audioFileForm['videoCover'].value['valueImg'].includes('data:audio')
  }

  uploadAudioFile = async () => {
    const {audioFileForm} = this.state
    const audioFileUploaded = prepareDataFormForSubmitAction(audioFileForm)
    if (this.checkIsAudioUploaded()) {
      const uploadVideoResponse = await uploadFileAction(audioFileUploaded['videoCover'].value, 'sentence.audio')
      this.responseAfterCoverUpload(uploadVideoResponse)
    }else{
      this.updateAudio(audioFileUploaded)
    }

  }

  responseAfterCoverUpload (dataResponse) {
    const {setToastNotification} = this.props
    const {audioFileUploaded} = this.state
    let reponseMessage = ''
    let type = toastType.SUCCESS
    if (dataResponse.data.success === true && dataResponse.status === 200) {
      reponseMessage = 'Audio is successful uploaded'
      let objectUUID = dataResponse['data'].object_uuid
      const updatedData = {
        ...audioFileUploaded,
        audio_file_uuid: objectUUID
      }
      this.updateAudio(updatedData)

      if (dataResponse.data.success !== true || dataResponse.status !== 200) {
        reponseMessage = 'Audio uploaded is failed' + dataResponse.data
        type = toastType.FAILURE
      }
      setToastNotification(true, reponseMessage, type)
    }
  }

  changeFormMode = () => {
    this.setState({mode: formMode.EDIT})
  }
  updateAudio = async (updatedData) => {
    const {data} = this.props
    await patchRequest(videos.SENTENCE + '/' + data['sentence_uuid'], updatedData, toastNotification.SHOW)
    this.setState({mode: formMode.VIEW})
  }

  render () {
    const {audioFileForm, mode} = this.state
    const {data} = this.props
    return <div style={{width: 500}}>
      <InputForm formElements={audioFileForm} prepareDataAndSubmit={this.uploadAudioFile} data={data}
        mode={mode} layout={layoutType.COLUMNS_2} lastElementInLayout={layoutType.COLUMNS_2}
        buttonLabel='Modify Audio' editFormButton={this.changeFormMode} editable={true} />
    </div>
  }
}

export default connect(null, {setToastNotification, setAlertDialog})((UploadAudioFiles))
