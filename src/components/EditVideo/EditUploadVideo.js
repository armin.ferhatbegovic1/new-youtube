/** @flow */
import React, {Component, Fragment} from 'react'
import {awaitFunction, isEmpty, localize, prepareDataFormForSubmitAction} from '../../services'
import styles from './styles'
import ReactPlayer from 'react-player'
import Images from '../../utils/Images'
import {uploadVideoForm, videoCoverForm} from '../../forms/uploadVideo'
import {alertDialogType, formMode, layoutType, toastType} from '../../config/Constants'
import InputForm from '../UI/InputForm/InputForm'
import ButtonWithIcon from '../UI/ButtonIcon/ButtonIcon'
import DeleteIcon from '@material-ui/icons/Delete'
import Button from '@material-ui/core/Button/Button'
import {setToastNotification} from '../../store/actions/toastAction'
import Dimensions from '../../utils/Dimensions'
import UploadAudioFiles from './UploadAudioFiles'
import SubMainButton from '../UI/SubMainButton/SubMainButton'
import {videos} from '../../config/Endpoints'
import apiEndpoint from '../../config/config'
import UploadAudioFiles2 from '../PreviewUploadVideo/UploadAudioFiles'
import {
    getVideoBinary,
    getVideoFormData,
    modifyVideoRecordAction,
    optimizationAction,
    uploadFileAction
} from '../../containers/EditVideoScreen/helpers'
import MainButton from '../UI/MainButton/MainButton'
import Grid from '@material-ui/core/Grid/Grid'
import connect from 'react-redux/es/connect/connect'
import {setAlertDialog} from '../../store/actions/alertDialogAction'

class EditUploadVideo extends Component {
    constructor(props) {
        super(props)
        this.state = {
            file: null,
            video: null,
            video2: null,
            url: null,
            pip: false,
            playing: false,
            playing2: false,
            controls: false,
            light: false,
            volume: 0.8,
            muted: false,
            played: 0,
            loaded: 0,
            duration: 0,
            playbackRate: 1.0,
            loop: false,
            message: null,
            uploadVideoForm: JSON.parse(JSON.stringify(uploadVideoForm)),
            videoCoverForm: JSON.parse(JSON.stringify(videoCoverForm)),
            imageUploadResponse: null,
            videoUploadResponse: null,
            audioSetienceNumber: 0,
            audioSetienceData: [],
            videoID: new URL(window.location.href).searchParams.get('id'),
            coverData: null,
            video_file_uuid: null,
            savedVideo: true,
            newVideoUuid: null,
            videoFileURL: null,
            newVideoFileBlobURL: null,
            newCoverFileBlobURL: null,
            videoFileUUID: null,
            videoFileURLCompressed: null,
            videoFileUUIDCompressed: null,
            newVideoFileBlobFile: null
        }
    }

    async componentDidMount() {
        const {videoID} = this.state
        const videoData = await getVideoFormData(videoID)
        if (!isEmpty(videoData)) {
            await awaitFunction(this.setState({
                videoFileURL: apiEndpoint.apiEndpoint + videos.FILE_VIEW + '/' + videoData['payload'].video_file_uuid,
                videoFileUUID: videoData['payload'].video_file_uuid,
                coverFileURL: apiEndpoint.apiEndpoint + videos.FILE_VIEW + '/' + videoData['payload']['cover'].uuid,
                coverFileUUID: videoData['payload']['cover'].uuid,
                sentencesAudio: videoData['payload'].sentences,
                videoDataPayload: videoData['payload']
            }))
        } else {
            this.setState({noResultsMessageVisible: true})
        }
    }

    videoFormSubmitAction = async (e) => {
        const {videoID} = this.state
        e.preventDefault()
        if (this.checkIsValidForm()) {
            const videoData = this.prepareVideoDataForUpload()
            await modifyVideoRecordAction(videoID, videoData)
        }
    }

    checkIsImageValidForm(videoData) {
        const {setToastNotification} = this.props
        let isValid = true
        if (isEmpty(videoData['videoCover'])) {
            setToastNotification(true, localize('youtDidntAddCover'))
            isValid = false
        }
        return isValid
    }

    imageFormSubmitAction = async () => {
        const {videoCoverForm} = this.state
        const {setToastNotification} = this.props
        const uploadImageData = prepareDataFormForSubmitAction(videoCoverForm)
        if (this.checkIsImageValidForm(uploadImageData) && !isEmpty(uploadImageData['videoCover'].valueImg)) {
            let image = uploadImageData['videoCover'].value
            const uploadVideoResponse = await uploadFileAction(image, 'video.cover')
            this.responseAfterCoverUpload(uploadVideoResponse)
        } else {
            setToastNotification(true, localize('youtDidntAddNewCover'))
        }
    }


    responseAfterCoverUpload(dataResponse) {
        const {setToastNotification} = this.props
        const {videoCoverForm} = this.state
        let reponseMessage = ''
        let type = toastType.SUCCESS
        if (dataResponse.data.success === true && dataResponse.status === 200) {
            reponseMessage = 'Cover Image is successful uploaded'
            let objectUUID = dataResponse['data'].object_uuid
            this.setState({
                coverFileURL: videoCoverForm['videoCover'].value,
                coverFileUUID: objectUUID
            })
        }
        if (dataResponse.data.success !== true || dataResponse.status !== 200) {
            reponseMessage = 'Cover Image uploaded is failed' + dataResponse.data
            type = toastType.FAILURE
        }
        setToastNotification(true, reponseMessage, type)
    }

    checkIsValidForm() {
        const {videoFileUUID, coverFileUUID} = this.state
        const {setAlertDialog} = this.props
        let isValid = true
        if (isEmpty(coverFileUUID)) {
            isValid = false
            setAlertDialog(true, localize('videoImageCoverIsNotUploaded'), localize('videoUploadWarning'), alertDialogType.WARNING)
        }
        if (isEmpty(videoFileUUID)) {
            isValid = false
            setAlertDialog(true, localize('videoIsNotUploaded'), localize('videoUploadWarning'), alertDialogType.WARNING)
        }
        return isValid
    }

    prepareVideoDataForUpload = (audioUpdate = false) => {
        const {uploadVideoForm, coverFileUUID, videoFileUUID, imageUploadResponse, audioSetienceData, videoID, newFileUuid, sentencesAudio} = this.state
        const uploadVideoFormData = prepareDataFormForSubmitAction(uploadVideoForm)

        let dataVideoRecord = {
            ...uploadVideoFormData
        }
        if (!audioUpdate) {
            dataVideoRecord['cover_file_uuid'] = coverFileUUID
            dataVideoRecord['video_file_uuid'] = videoFileUUID
        }

        if (audioSetienceData.length !== 0) {
            dataVideoRecord['sentences'] = audioSetienceData.concat(sentencesAudio)
        }
        return dataVideoRecord
    }


    addAudioSetienceNumber = () => {
        const {audioSetienceNumber} = this.state
        let newValue = audioSetienceNumber + 1
        this.setState({audioSetienceNumber: newValue})
    }

    uploadVideoFile = (e) => {
        this.setState({
            newVideoFileBlobURL: URL.createObjectURL(e.target.files[0]),
            newVideoFileBlobFile: e.target.files[0]
        })
        e.target.value = null
    }

    async saveOriginalVideo() {
        const {setToastNotification} = this.props
        setToastNotification(true, 'You save original video successfully', toastType.SUCCESS)
    }

    submitUploadVideoForm = () => {
        this.clickUploadBtn()
    }

    deleteVideo = () => {
        this.setState({videoFileUUID: null, videoFileURL: null, newVideoFileBlobURL: null, newVideoFileBlobFile: null})
    }

    clickUploadBtn() {
        document.querySelector('#uploadBtn button').click()
    }

    playPause = () => {
        this.setState({playing: !this.state.playing})
    }
    stop = () => {
        this.setState({url: null, playing: false})
    }
    stop2 = () => {
        this.setState({playing2: false})
    }
    playPause2 = () => {
        this.setState({playing2: !this.state.playing2})
    }

    optimization = async () => {
        const {videoID, videoFileUUID} = this.state
        if (!isEmpty(videoFileUUID)) {
            const data = await optimizationAction(videoID)
            if (data['success']) {
                let newFileUuid = data['payload'].video_file_uuid
                this.setState({video2: apiEndpoint.apiEndpoint + '/file/' + newFileUuid, newFileUuid})
            }
        } else {
            this.props.setAlertDialog(true, localize('youHaveToSaveVideoFirst'), localize('videoUploadWarning'), alertDialogType.WARNING)
        }
    }
    addAudioDataInArray = async (data, index) => {
        await awaitFunction(this.setState({
            audioSetienceData: this.state.audioSetienceData.concat(data)
        }))
        this.updateAudioVideo(data)
    }

    updateAudioVideo = async (data) => {
        const {videoID} = this.state
        if (this.checkIsValidForm()) {
            const videoData = this.prepareVideoDataForUpload(true)
            await modifyVideoRecordAction(videoID, videoData)
        }
    }

    renderAudioFields = () => {
        const {sentencesAudio, videoID} = this.state
        if (!isEmpty(sentencesAudio) && sentencesAudio.length !== 0) {
            return Array.apply(0, Array(sentencesAudio.length)).map((x, i) => {
                return <UploadAudioFiles videoID={videoID} key={i} index={i} addAudioData={this.addAudioDataInArray}
                                         data={sentencesAudio[i]}/>
            })
        }
    }
    renderAudioFieldsNEw = () => {
        const {audioSetienceNumber} = this.state
        if (audioSetienceNumber !== 0) {
            return Array.apply(0, Array(audioSetienceNumber)).map((x, i) => {
                return <UploadAudioFiles2 key={i} index={i} addAudioData={this.addAudioDataInArray}/>
            })
        }
    }

    saveVideoOnServer = async () => {
        const {newVideoFileBlobFile, newVideoFileBlobURL} = this.state
        const {setToastNotification} = this.props
        const uploadVideoResponse = await uploadFileAction(newVideoFileBlobFile)
        if (!isEmpty(uploadVideoResponse) && !isEmpty(uploadVideoResponse['data'])) {
            if (uploadVideoResponse['data'].success) {
                this.setState({
                    videoFileUUID: uploadVideoResponse['data'].object_uuid,
                    videoFileURL: newVideoFileBlobURL,
                    newVideoFileBlobURL: null,
                    newVideoFileBlobFile: null
                })
                setToastNotification(true, 'You save video successfully', toastType.SUCCESS)
            }
        }
    }

    renderVideoControls() {
        const {videoFileURL, newVideoFileBlobURL, playing} = this.state
        return <div style={styles.playerControls}>
            <Button variant='contained' size='small' color='primary' style={styles.btnContainer}
                    disabled={!newVideoFileBlobURL}
                    onClick={this.saveVideoOnServer}>
                {localize('uploadVideo')}
            </Button>
            <Button variant='contained' size='small' color='primary' style={styles.btnContainer}
                    disabled={!videoFileURL}
                    onClick={this.optimization}>
                {localize('optimization')}
            </Button>
            <Button variant='contained' size='small' color='primary' style={styles.btnContainer}
                    disabled={!videoFileURL}
                    onClick={this.stop}>
                {localize('stop')}
            </Button>
            <Button variant='contained' size='small' color='primary' style={styles.btnContainer}
                    disabled={!videoFileURL}
                    onClick={this.playPause}>
                {playing ? 'Pause' : 'Play'}
            </Button>
            <ButtonWithIcon handleAction={this.deleteVideo} buttonType='primary'>
                <DeleteIcon/>
            </ButtonWithIcon>
        </div>
    }

    renderVideoControls2() {
        const {video2, playing2} = this.state
        return video2 !== null ?
            <div style={styles.playerControls}>
                <Button variant='contained' size='small' color='primary' style={styles.btnContainer}
                        disabled={!video2}
                        onClick={this.stop2}>
                    {localize('stop')}
                </Button>
                <Button variant='contained' size='small' color='primary' style={styles.btnContainer}
                        disabled={!video2}
                        onClick={this.playPause2}>
                    {playing2 ? 'Pause' : 'Play'}
                </Button>
            </div> : null
    }

    renderOriginalVideoContainer() {
        const {videoFileURL, playing, newVideoFileBlobURL} = this.state
        const fileURL = isEmpty(videoFileURL) ? newVideoFileBlobURL : videoFileURL
        return fileURL !== null ?
            <ReactPlayer url={fileURL} width={'100%'}
                         playing={playing}/> : <img src={Images.videoPlaceholder} alt='placeholder'
                                                    onClick={() => {
                                                        this.upload.click()
                                                    }}
                                                    style={styles.videoPlaceholder}/>
    }

    renderVideoUploadContainer() {
        return <div style={styles.videoContainerUpload}>
            <div style={styles.renderVideoUploadContainer}>

                <div style={styles.videoDetailsContainer}>
                    <div style={styles.videoContainer} id='video-container'>
                        {this.renderOriginalVideoContainer()}
                        {this.renderVideoControls()}
                    </div>
                    {this.renderVideoUploadContainerOptimize()}

                </div>
                <div style={styles.videoDetailsContainer}>
                    {this.renderImageCoverUpload()}
                    {this.renderVideoDetails()}
                </div>

            </div>

        </div>

    }

    renderVideoUploadContainerOptimize() {
        const {video2, playing2} = this.state
        return !isEmpty(video2) ? <div style={styles.videoContainerUpload}>
            <div style={{flexDirection: 'column', display: 'flex', marginTop: 40}}>
                <div style={styles.videoContainer2} id='video-container'> {video2 !== null ?
                    <div><ReactPlayer url={video2}
                                      width={'100%'} height={'auto'}
                                      playing={playing2}/>
                    </div>
                    : <img
                        src={Images.videoPlaceholder}
                        alt='placeholder'
                        onClick={() => {
                            this.upload.click()
                        }}
                        style={styles.videoPlaceholder}
                    />}</div>
                {this.renderVideoControls2()}
            </div>
        </div> : null
    }

    renderVideoDetails() {
        const {uploadVideoForm, message, videoDataPayload} = this.state
        return <div style={styles.videoFormDetails}>
            <form onSubmit={this.videoFormSubmitAction}>
                <input type='file' onChange={this.uploadVideoFile}
                       style={{display: 'none'}}
                       ref={(ref) => this.upload = ref}
                       id='video-file-2'/>
                <div id='uploadBtn' style={{display: 'none'}}>
                    <MainButton label={localize('uploadVideo')} buttonType='primary'
                                type='submit'/>
                </div>
            </form>

            {!isEmpty(videoDataPayload) ?
                <Fragment>
                    <InputForm data={videoDataPayload} formElements={uploadVideoForm}
                               prepareDataAndSubmit={this.submitUploadVideoForm}
                               mode={formMode.EDIT} layout={layoutType.COLUMNS_3} buttonLabel='Modify Video'>
                        {!isEmpty(message) ? <label
                            style={{fontSize: Dimensions.textMedium, alignSelf: 'center'}}>{message}</label> : null}
                    </InputForm></Fragment> : null}
            {this.renderSavingButtons()}
        </div>

    }

    saveOptimizationVideo = () => {
        const {video2} = this.state
        const {setToastNotification} = this.props
        if (isEmpty(video2)) {
            setToastNotification(true, 'You didnt click button optimization')
        } else {
            setToastNotification(true, 'You save optimized video successfully', toastType.SUCCESS)
        }
    }

    loadCoverDataInForm() {
        const {videoCoverForm, coverFileURL} = this.state
        videoCoverForm['videoCover'].value = coverFileURL
        setTimeout(() => this.setState({coverFileURL: 1}), 1000)
    }

    renderImageCoverUpload() {
        const {videoCoverForm, coverFileURL} = this.state
        if (!isEmpty(coverFileURL) && coverFileURL !== 1) {
            this.loadCoverDataInForm()
        }

        return !isEmpty(coverFileURL) ? <div style={{justifyContent: 'flex-start', display: 'flex'}}>
            <InputForm formElements={videoCoverForm}
                       prepareDataAndSubmit={this.imageFormSubmitAction} data={coverFileURL}
                       mode={formMode.EDIT} layout={layoutType.COLUMNS_1} lastElementInLayout={layoutType.COLUMNS_2}
                       buttonLabel='Modify Cover'/>
            <div style={{height: 50, alignSelf: 'center'}}>
                <SubMainButton label='Add New Audio' buttonType='primary'
                               type='button' onClick={() => this.addAudioSetienceNumber()}/>
            </div>
        </div> : null
    }

    renderSavingButtons = () => {
        const {video2} = this.state
        return !isEmpty(video2) ? <div style={styles.saveVideoButtonContainer}>
            <SubMainButton label='Save Original Video' buttonType='secondary'
                           type='button' onClick={() => {
                this.saveOriginalVideo()
            }}/>
            <SubMainButton label='Save Optimized Video' buttonType='secondary'
                           type='button' onClick={() => {
                this.saveOptimizationVideo()
            }}/>
        </div> : null
    }

    renderVideoUploader() {
        return (
            <Fragment>
                <Grid container spacing={24}>
                    <Grid item xs={12}>
                        {this.renderVideoUploadContainer()}
                    </Grid>
                    <Grid item xs={12}>
                        <div style={{display: 'flex', flexWrap: 'wrap'}}> {this.renderAudioFields()}
                            {this.renderAudioFieldsNEw()}</div>
                    </Grid>
                </Grid>
            </Fragment>
        )
    }

    render() {
        return (
            this.renderVideoUploader()
        )
    }
}

export default connect(null, {setToastNotification, setAlertDialog})((EditUploadVideo))
