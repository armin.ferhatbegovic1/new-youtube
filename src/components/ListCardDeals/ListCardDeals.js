import React, { Component, Fragment } from 'react'
import { awaitFunction, calculateCountOfItemsInRow, getRequest, isEmpty, localize } from '../../services'
import { changeDateFormat, fetchingDealStatus, placeholderDeal } from './helpers'
import Grid from '@material-ui/core/Grid/Grid'
import CardDeal from '../UI/CardDeal/CardDeal'
import {
  BATCH_SIZE,
  cardType,
  dateFormat, deviceType,
  loaderVisbility,
  request,
  requestHeader,
  role,
  THRESHOLD_COUNT
} from '../../config/Constants'
import apiEndpoint from '../../config/config'
import styles from './styles'
import { AutoSizer, InfiniteLoader, List } from 'react-virtualized'
import { connect } from 'react-redux'
import RenewDealDialog from '../UI/RenewDealDialog/RenewDealDialog'
import {
  calculateGrid,
  createLinkForFetchingListData,
  getHeaderHeight
} from '../../services/GlobalServices'
import { setFormDialog } from '../../store/actions/formDialogAction'
import { prepareDealDataAndRedirectToPaymentScreen } from '../../services/DealServices'
import { setLoaderVisibility } from '../../store/actions/loaderAction'
import Dimensions from '../../utils/Dimensions'

const ROW_HEIGHT = Dimensions.getDevice() === deviceType.PHONE ? 580 : 630 //deal card
const ROW_HEIGHT_WITH_ACTIONS = Dimensions.getDevice() === deviceType.PHONE ? 630 : 700//deal card with submit actions
const VIEW_HEIGHT = Dimensions.getDevice() === deviceType.PHONE ? 450 : 847

class ListCardDeals extends Component {
  constructor (props) {
    super(props)
    this.state = {
      loadedRowCount: 0,
      loadedRowsMap: {},
      listOfPreparedItems: [],
      totalCount: 0,
      deals: [],
      filter: props.filter,
      noResultsMessageVisible: false,
      rowCount: 0,
      viewHeight: VIEW_HEIGHT,
      rowHeight: this.props.auth['user'].role === role.INFLUENCER ? ROW_HEIGHT_WITH_ACTIONS : ROW_HEIGHT,
      selectDurationDialogOpen: false,
      numberOfDealsInRow: calculateCountOfItemsInRow(),
      resizing: false
    }
    this._isMounted = false
    this.InfiniteLoader = React.createRef()
  }

  componentDidUpdate (prevProps, nextProps) {
    const viewHeight = window.innerHeight - getHeaderHeight()
    // const rowHeight=this.getRowHeight()
    if (this.state.viewHeight !== viewHeight && this._isMounted) {
      this.setState({viewHeight})
    }
  }

  componentWillUnmount () {
    this._isMounted = false
    window.removeEventListener('resize', this.resize)
  }

  getRowHeight () {
    const firstRow = document.getElementById('row-0-0')
    if (!isEmpty(firstRow)) {
      return firstRow.clientHeight + 20
    } else {
      return ROW_HEIGHT
    }
  }

  async componentDidMount () {
    this._isMounted = true
    const data = await this.getDeals()
    if (!isEmpty(data.totalDocs)) {
      await awaitFunction(this.calculateNumberOfRows(data.totalDocs))
      this.props.setLoaderVisibility(loaderVisbility.HIDE)
    } else {
      this.setState({noResultsMessageVisible: true})
    }
    this.recalculatePageHeight()
    window.addEventListener('resize', this.resize)
  }

  async getDeals () {
    const {endpoint, setLoaderVisibility} = this.props
    const {filter} = this.state
    setLoaderVisibility(loaderVisbility.SHOW)

    if (this._isMounted)
      return await getRequest(createLinkForFetchingListData(endpoint, 1, 1, 'name', filter))
  }

  async componentWillReceiveProps (nextProps) {
    if (nextProps.filter !== this.props.filter) {
      await awaitFunction(this.setState({filter: nextProps.filter}))
      const data = await this.getDeals()
      if (!isEmpty(data.totalDocs)) {
        this.calculateNumberOfRows(data.totalDocs)
        this.rerenderInfiniteLoader()
      } else {
        this.setState({noResultsMessageVisible: true})
      }
      this.recalculatePageHeight()
    }
  }

  recalculatePageHeight () {

    const {rowCount} = this.state
    const existStyle = document.getElementsByClassName('ReactVirtualized__Grid__innerScrollContainer')[0]
    if (!isEmpty(existStyle) && rowCount > 1) {
      console.log('=== ', rowCount, ' ==== ', (rowCount - 1) * this.state.rowHeight + 'px')
      document.getElementsByClassName('ReactVirtualized__Grid__innerScrollContainer')[0].style.maxHeight = (rowCount - 1) * this.state.rowHeight + 'px'
    }
  }

  resize = async () => {
    if (this.state.resizing === false) {
      await awaitFunction(this.setState({resizing: true}))
      this.calculateNumberOfRows(this.state.totalCount)
      this.rerenderInfiniteLoader()
      this.forceUpdate()
      this.setState({resizing: false})
    }
  }

  renderDealCardRow (index) {
    return <Grid container spacing={24}>
      {this.renderDealCard(index)}
    </Grid>
  }

  renderDealCard (index) {
    const {deals} = this.state
    const {history, listMenuDealActions} = this.props
    const gridCAL = calculateGrid()

    return deals[index].map((deal, i) => {
      return (
        <Grid key={i} item xs sm={gridCAL} md={gridCAL} lg={gridCAL} xl={3}>
          <CardDeal deal={deal} key={deal._id} history={history}
            openModalToRenewDeal={this.openModalToRenewDeal} listMenuDealActions={listMenuDealActions}
            type={cardType.PLACEHOLDER_IMAGE} rerenderInfiniteLoader={this.rerenderInfiniteLoader} index={index} />
        </Grid>
      )
    })
  }

  rerenderInfiniteLoader = () => {
    const {totalCount} = this.state

    if (totalCount !== 0) {
      this.InfiniteLoader.current.resetLoadMoreRowsCache()
      this._loadMoreRows({
        startIndex: 0,
        stopIndex: 11
      })
    } else {
      this.props.setLoaderVisibility(loaderVisbility.HIDE)
    }
  }

  renderDealCardRowPlaceholder (index) {
    return <Grid container spacing={24}>
      {this.renderPlaceholderDealCard(index)}
    </Grid>

  }

  renderPlaceholderDealCard (index) {
    const {history, listMenuDealActions} = this.props
    const tmpArray = [1, 2, 3, 4]
    return tmpArray.map((i) => {
      return <Grid key={i} item md={3} sm={4} xs={12}>
        <CardDeal deal={placeholderDeal} key={index} history={history}
          openModalToConfirmDelete={this.openModalToConfirmDelete}
          openModalToRenewDeal={this.openModalToRenewDeal}
          listMenuDealActions={listMenuDealActions}
          type={cardType.PLACEHOLDER} />
      </Grid>
    })
  }

  openModalToRenewDeal = (dealID) => {
    this.setState({renewDealID: dealID})
    this.setState({selectDurationDialogOpen: true})
  }

  selectDurationDialogClose = async () => {
    await awaitFunction(this.setState({selectDurationDialogOpen: false, renewDealID: null}))
  }

  renderRenewDealDialog () {
    const {selectDurationDialogOpen, renewDealID} = this.state
    const {auth, history} = this.props
    return auth['user'].role === role.BRAND ? <RenewDealDialog open={selectDurationDialogOpen}
      close={this.selectDurationDialogClose}
      submitAction={(paymentInfo) => {prepareDealDataAndRedirectToPaymentScreen(paymentInfo, renewDealID, history)}} /> : null
  }

  createListOfDeals (startIndex, stopIndex, data) {
    const {loadedRowsMap, deals, numberOfDealsInRow} = this.state
    changeDateFormat(data['docs'], 'startDate', dateFormat.MMMMDDYYYY)
    changeDateFormat(data['docs'], 'endDate', dateFormat.MMMMDDYYYY)

    let c = 0
    const t = stopIndex !== startIndex ? stopIndex : stopIndex + 1
    for (let i = startIndex; i < t; i++) {
      loadedRowsMap[i] = fetchingDealStatus.STATUS_LOADED
      deals[i] = data.docs.slice(numberOfDealsInRow * c, numberOfDealsInRow * c + numberOfDealsInRow)
      c++
    }
    if (this._isMounted)
      this.setState({deals})
  }

  _isRowLoaded = ({index}) => {
    const {loadedRowsMap} = this.state
    return !!loadedRowsMap[index]
  }

  fetchDealsList (startIndex, stopIndex, link, resolve) {
    fetch(link, {
      method: request.GET,
      headers: requestHeader()
    })
    .then(function (response) {
      return response.json()
    })
    .then((data) => {
      this.createListOfDeals(startIndex, stopIndex, data)
      this.forceUpdate()
      this.calculateNumberOfRows(data.totalDocs)
      this.props.setLoaderVisibility(loaderVisbility.HIDE)

      resolve()
    })
  }

  _loadMoreRows = ({startIndex, stopIndex}) => {
    const {loadedRowsMap, totalCount, numberOfDealsInRow, deals, rowCount} = this.state
    const {filter, endpoint} = this.props
    //if (rowCount !== 1 && rowCount - 1 === deals.length) return
    if (totalCount !== 0) {

      for (let i = startIndex; i < stopIndex; i++) {
        loadedRowsMap[i] = fetchingDealStatus.STATUS_LOADING
      }

      return new Promise((resolve) => {
        let size = (stopIndex === startIndex) ? numberOfDealsInRow : (stopIndex - startIndex) * numberOfDealsInRow
        let skip = 0

        if (startIndex !== 0) {
          skip = (startIndex) * numberOfDealsInRow
        }
        const link = createLinkForFetchingListData(apiEndpoint.apiEndpoint + endpoint, skip, size, 'name', filter)
        this._isMounted && this.fetchDealsList(startIndex, stopIndex, link, resolve)
      })
    }
  }

  recalculateRowHeight (index, style) {
    const newStyle = {...style}
    if (index !== 0) {
      newStyle['top'] = newStyle['top'] - 80 * (index - 1)
    }

    return newStyle
  }

  renderFirstRow (index) {
    const {children} = this.props
    return index === 0 ? children : null
  }

  renderFirstRows (index, key) {
    const {loadedRowsMap} = this.state
    return <div key={key} style={styles.row} id={'row-' + key}>
      {loadedRowsMap[index] === fetchingDealStatus.STATUS_LOADED ? this.renderDealCardRow(index) : this.renderDealCardRowPlaceholder(index)}
    </div>
  }

  _rowRenderer = ({index, key, style}) => {
    return <div key={key} style={this.recalculateRowHeight(index, style)}>
      {this.renderFirstRow(index)}
      {this.renderFirstRows(index, key)}
    </div>
  }

  renderDealsList () {

    const {viewHeight, deals, rowHeight, rowCount} = this.state
    return <InfiniteLoader
      isRowLoaded={this._isRowLoaded}
      loadMoreRows={this._loadMoreRows}
      rowCount={rowCount}
      ref={this.InfiniteLoader}
      minimumBatchSize={BATCH_SIZE}
      threshold={THRESHOLD_COUNT}
    >
      {({onRowsRendered, registerChild}) => (
        <AutoSizer disableHeight>
          {({width, height}) => {
            return (
              <List
                {...deals}
                ref={registerChild}
                height={viewHeight}
                onRowsRendered={onRowsRendered}
                rowCount={rowCount}
                rowHeight={rowHeight}
                rowRenderer={this._rowRenderer}
                width={width}
              />

            )
          }}
        </AutoSizer>
      )}
    </InfiniteLoader>
  }

  calculateNumberOfRows (totalCount) {
    const numberOfDealsInRow = calculateCountOfItemsInRow()
    const roundNumRows = Math.ceil(totalCount / numberOfDealsInRow)
    const rowCount = roundNumRows !== 1 ? roundNumRows + 1 : roundNumRows
    if (this._isMounted)
      this.setState({rowCount, totalCount, noResultsMessageVisible: totalCount === 0, numberOfDealsInRow})
  }

  renderNoResultsMessage () {
    const {noResultsMessageVisible} = this.state
    if (noResultsMessageVisible) {
      this.props.setLoaderVisibility(loaderVisbility.HIDE)
      return <label
        style={styles.noResultsContainer}>{localize('activeDealScreen.noActiveDeals')}</label>
    }
  }

  renderCardDeals () {
    const {children} = this.props
    const {totalCount} = this.state
    return totalCount !== 0 ? this.renderDealsList() : <div>{children} {this.renderNoResultsMessage()}</div>
  }

  render () {
    return (
      <Fragment>
        {this.renderRenewDealDialog()}
        {this.renderCardDeals()}
      </Fragment>
    )
  }
}

const mapStateToProps = state => {
  return {
    auth: state.auth
  }
}
export default connect(mapStateToProps, {setFormDialog, setLoaderVisibility})((ListCardDeals))