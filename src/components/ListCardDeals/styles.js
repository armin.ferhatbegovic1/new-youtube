import RootComponentStyles from '../Styles/RootComponentStyles'
import Dimensions from '../../utils/Dimensions'

const styles = {
  ...RootComponentStyles,
  grid: {
    padding: '20px',
    display: 'grid',
    gridTemplateRows: '85px 1fr 1fr 1fr',
    height: 'inherit'
  },
  buttons: {
    display: 'flex',
    justifyContent: 'end',
    alignItems: 'center',
    marginTop: '20px'
  },
  addNewButton: {
    marginTop: '20px',
    marginRight: '50px'
  },
  row: {
    padding: 24
  },
  circular: {
    height: Dimensions.fullHeight,
    minHeight: Dimensions.fullHeight,
    display: 'flex',
    flexDirection: 'column'
  },
  filterContainer: {
    flex:1,
    margin:Dimensions.doubleBaseMargin
  },
  filterChip:{
    margin:Dimensions.baseMargin
  }
}

export default styles