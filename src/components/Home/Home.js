import { Route } from 'react-router-dom'
import HomeLayout from '../../layouts/HomeLayout/HomeLayout'
import React from 'react'

const home = ({component: Component, ...rest}) => {
  return (
    <Route
      {...rest}
      render={matchProps => (
        <HomeLayout>
          <Component {...matchProps} />
        </HomeLayout>
      )}
    />
  )
}

export default home;