import RootComponentStyles from '../Styles/RootComponentStyles'

const styles = {
  ...RootComponentStyles,
  grid: {
    padding: '20px',
    display: 'grid',
    gridTemplateRows: '85px 1fr 1fr 1fr',
    height: 'inherit'
  },
  buttons: {
    display: 'flex',
    justifyContent: 'end',
    alignItems: 'center',
    marginTop: '20px'
  },
  addNewButton: {
    marginTop: '20px',
    marginRight: '50px'
  },
  row:{
    padding:24
  },
  listContainer:{
  //  marginTop:15
  }
}

export default styles