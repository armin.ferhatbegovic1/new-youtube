import React, { Component } from 'react'
import { awaitFunction, calculateCountOfItemsInRow, getRequest, isEmpty, localize } from '../../services'
import { changeDateFormat, placeholderBid } from './helpers'
import Grid from '@material-ui/core/Grid/Grid'
import {
  BATCH_SIZE,
  cardType,
  dateFormat,
  loaderVisbility,
  request,
  requestHeader,
  role,
  THRESHOLD_COUNT
} from '../../config/Constants'
import apiEndpoint from '../../config/config'
import styles from './styles'
import { AutoSizer, InfiniteLoader, List } from 'react-virtualized'
import { connect } from 'react-redux'
import CardBid from '../UI/CardBid/CardBid'
import { createLinkForFetchingListData } from '../../services/GlobalServices'
import { fetchingDealStatus } from '../ListCardDeals/helpers'
import { setLoaderVisibility } from '../../store/actions/loaderAction'

const ROW_HEIGHT = 370 //deal card
const ROW_HEIGHT_WITH_ACTIONS = 810//deal card with submit actions
const VIEW_HEIGHT = 847
const VIEW_HEIGHT_INF = 500


/*
const ROW_HEIGHT = 400 //deal card
const ROW_HEIGHT_WITH_ACTIONS = 1000//deal card with submit actions
const VIEW_HEIGHT = 300
*/

class ListCardBids extends Component {
  constructor (props) {
    super(props)
    this.state = {
      loadedRowCount: 0,
      loadedRowsMap: {},
      listOfPreparedItems: [],
      totalCount: 0,
      list: [],
      noResultsMessageVisible: false,
      rowCount: 0,
      viewHeight: this.props.auth['user'].role === role.INFLUENCER ? VIEW_HEIGHT : VIEW_HEIGHT_INF,
      rowHeight: this.props.auth['user'].role === role.INFLUENCER ? ROW_HEIGHT_WITH_ACTIONS : ROW_HEIGHT,
      selectDurationDialogOpen: false,
      numberOfDataInRow: calculateCountOfItemsInRow()
    }
    this._isMounted = false;
    this.InfiniteLoader = React.createRef()
  }

  componentDidUpdate (prevProps) {
    const viewHeight = window.innerHeight - this.getHeaderHeight()
    // const rowHeight = this.getRowHeight()
    if (this.state.viewHeight !== viewHeight && this.props.auth['user'].role === role.INFLUENCER) {
      this.setState({viewHeight})
    }
  }

  getRowHeight () {
    const firstRow = document.getElementById('row-0-0')
    if (!isEmpty(firstRow)) {
      return firstRow.clientHeight + 20
    } else {
      return ROW_HEIGHT
    }
  }
  recalculatePageHeight () {
    const {rowCount} = this.state
    if (rowCount > 1) {
      document.getElementsByClassName('ReactVirtualized__Grid__innerScrollContainer')[0].style.maxHeight = document.getElementsByClassName('ReactVirtualized__Grid__innerScrollContainer')[0].clientHeight -100- this.state.rowHeight + 'px'

    }
  }


  getHeaderHeight () {
    const firstRow = document.getElementsByTagName('header')
    if (!isEmpty(firstRow)) {
      return document.getElementsByTagName('header')[0].clientHeight
    } else {
      return ROW_HEIGHT
    }
  }

  componentWillUnmount () {
    this._isMounted = false
    window.removeEventListener('resize', this.resize)
  }

  async componentDidMount () {
    this._isMounted = true;
    const data = await this.getBids()
    if (!isEmpty(data.totalDocs)) {
      await awaitFunction(this.calculateNumberOfRows(data.totalDocs))
      this.props.setLoaderVisibility(loaderVisbility.HIDE)
    }
    window.addEventListener('resize', this.resize)
    this.recalculatePageHeight()
  }

  async getBids () {
    const {endpoint, filter, setLoaderVisibility} = this.props
    setLoaderVisibility(loaderVisbility.SHOW)
    if(this._isMounted )
    return await getRequest(createLinkForFetchingListData(endpoint, 1, 1, 'name', filter))
  }

  async componentWillReceiveProps (nextProps) {
    if (nextProps.filter !== this.props.filter) {
      await awaitFunction(this.setState({filter: nextProps.filter}))
      const data = await this.getBids()
      if (!isEmpty(data.totalDocs)) {
        this.calculateNumberOfRows(data.totalDocs)
        this.rerenderInfiniteLoader()
      }
      this.recalculatePageHeight()
    }
  }

  resize = () => {
    this.forceUpdate()
  }

  renderBidCardRow (index) {
    return <Grid container spacing={24}>
      {this.renderBidCard(index)}
    </Grid>
  }

  renderBidCard (index) {
    const {list} = this.state
    return list[index].map((bid, i) => {
      return (
        <Grid key={i} item xs sm md={4} lg={3}>
          <CardBid data={bid} key={bid._id} history={this.props.history}
            type={cardType.PLACEHOLDER_IMAGE} rerenderInfiniteLoader={this.rerenderInfiniteLoader} index={index} />
        </Grid>
      )
    })
  }

  rerenderInfiniteLoader = () => {
    const {totalCount} = this.state
    if (totalCount !== 0) {
      this.InfiniteLoader.current.resetLoadMoreRowsCache()
      this._loadMoreRows({
        startIndex: 0,
        stopIndex: 11
      })
    } else {
      this.props.setLoaderVisibility(loaderVisbility.HIDE)
    }
  }

  renderBidCardRowPlaceholder (index) {
    return <Grid container spacing={24}>
      {this.renderPlaceholderBidCard(index)}
    </Grid>

  }

  renderPlaceholderBidCard (index) {
    const tmpArray = [1, 2, 3, 4]
    return tmpArray.map((i) => {
      return <Grid key={i} item xs sm md={4} lg={3}>
        <CardBid data={placeholderBid} key={index} history={this.props.history}
          openModalToConfirmDelete={this.openModalToConfirmDelete}
          type={cardType.PLACEHOLDER} />
      </Grid>
    })
  }

  createListOfBids (startIndex, stopIndex, data) {
    const {loadedRowsMap, list, numberOfDataInRow} = this.state
    changeDateFormat(data['docs'], 'startDate', dateFormat.MMMMDDYYYY)
    changeDateFormat(data['docs'], 'endDate', dateFormat.MMMMDDYYYY)
    changeDateFormat(data['docs'], 'createdAt', dateFormat.MMMMDDYYYY)
    let c = 0
    const t = stopIndex !== startIndex ? stopIndex : stopIndex + 1
    for (let i = startIndex; i < t; i++) {
      loadedRowsMap[i] = fetchingDealStatus.STATUS_LOADED
      list[i] = data.docs.slice(numberOfDataInRow * c, numberOfDataInRow * c + numberOfDataInRow)
      c++
    }
    this.setState({list})
  }

  _isRowLoaded = ({index}) => {
    const {loadedRowsMap} = this.state
    return !!loadedRowsMap[index]
  }

  fetchBidsList (startIndex, stopIndex, link, resolve) {
    fetch(link, {
      method: request.GET,
      headers: requestHeader()
    })
    .then(function (response) {
      return response.json()
    })
    .then((data) => {
      this.createListOfBids(startIndex, stopIndex, data)
      this.forceUpdate()
      this.calculateNumberOfRows(data.totalDocs)
      this.props.setLoaderVisibility(loaderVisbility.HIDE)
      resolve()
    })
  }

  _loadMoreRows = ({startIndex, stopIndex}) => {
    const {loadedRowsMap, totalCount, numberOfDataInRow} = this.state
    const {filter, endpoint} = this.props
    if (totalCount !== 0) {

      for (let i = startIndex; i < stopIndex; i++) {
        loadedRowsMap[i] = fetchingDealStatus.STATUS_LOADING
      }

      return new Promise((resolve) => {
        let size = (stopIndex === startIndex) ? numberOfDataInRow : (stopIndex - startIndex) * numberOfDataInRow
        let skip = 0

        if (startIndex !== 0) {
          skip = (startIndex) * numberOfDataInRow
        }
        const link = createLinkForFetchingListData(apiEndpoint.apiEndpoint + endpoint, skip, size, 'name', filter)
        this._isMounted &&this.fetchBidsList(startIndex, stopIndex, link, resolve)
      })
    }
  }

  recalculateRowHeight (index, style) {
    const newStyle = {...style}
    if (index !== 0) {
      newStyle['top'] = newStyle['top'] - 100 * (index - 1)
    }

    return newStyle
  }

  _rowRenderer = ({index, key, style}) => {
    const {loadedRowsMap} = this.state
    const {children} = this.props
    return (
      <div key={key} style={this.recalculateRowHeight(index, style)}>
        {index === 0 ?
          children
          : null}
        <div key={key} style={styles.row} id={'row-' + key}>
          {loadedRowsMap[index] === fetchingDealStatus.STATUS_LOADED ? this.renderBidCardRow(index) : this.renderBidCardRowPlaceholder(index)}
        </div>
      </div>
    )
  }

  renderBidsList () {
    const {viewHeight, list, rowHeight, rowCount} = this.state

    return <InfiniteLoader
      isRowLoaded={this._isRowLoaded}
      loadMoreRows={this._loadMoreRows}
      rowCount={rowCount}
      ref={this.InfiniteLoader}
      minimumBatchSize={BATCH_SIZE}
      threshold={THRESHOLD_COUNT}
    >
      {({onRowsRendered, registerChild}) => (
        <AutoSizer disableHeight>
          {({width, height}) => {
            return (
              <List
                {...list}
                ref={registerChild}
                height={viewHeight}
                onRowsRendered={onRowsRendered}
                rowCount={rowCount}
                rowHeight={rowHeight}
                rowRenderer={this._rowRenderer}
                width={width}
              />

            )
          }}
        </AutoSizer>
      )}
    </InfiniteLoader>
  }

  calculateNumberOfRows (totalCount) {
    const {numberOfDataInRow} = this.state
    // this.setState({rowCount: rowCount % 2 === 0 ? rowCount : rowCount + 1,totalCount})
    const roundNumRows = Math.ceil(totalCount / numberOfDataInRow)
    const rowsNum = roundNumRows !== 1 ? roundNumRows + 1 : roundNumRows
    this._isMounted && this.setState({rowCount: rowsNum, totalCount, noResultsMessageVisible: totalCount === 0})

  }

  renderNoResultsMessage () {
    const {noResultsMessageVisible} = this.state
    const {noResultsMessageShow} = this.props
    if (noResultsMessageVisible && noResultsMessageShow) return <label
      style={styles.noResultsContainer}>{localize('activeDealScreen.noActiveDeals')}</label>
  }

  render () {
    const {totalCount} = this.state
    const {children} = this.props
    return (
      <div>
        {
          totalCount !== 0 ?
            this.renderBidsList()
            : <div>{children} {this.renderNoResultsMessage()}</div>
        } </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    auth: state.auth
  }
}
export default connect(mapStateToProps, {setLoaderVisibility})((ListCardBids))