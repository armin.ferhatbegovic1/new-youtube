import moment from 'moment'

export function changeDateFormat (data, fieldForEdit, dateFormat) {

  data.map((item) => {
    const date = fieldForEdit==='createdAt'? parseInt(item[fieldForEdit]):item[fieldForEdit]
    return item[fieldForEdit] = moment(date).format(dateFormat)
  })
  return data
}

export const fetchingDataStatus = {
  STATUS_LOADING: 1,
  STATUS_LOADED: 2
}

export const placeholderDeal = {
  description: '*************************',
  endDate: 'March 29, 2019',
  name: '****************',
  numberOfFollowers: 10000000000,
  startDate: 'February 27, 2019',
  status: 'Active',
  _id: '5c76a15b0ef9d017e0547996',
  budget:'****'
}

export const placeholderBid={
  dealId:placeholderDeal,
  additionalInfo:'**************************',
  createdAt:'**************',
  numberOfPosts:'***',
  numberOfStories:'***'
}