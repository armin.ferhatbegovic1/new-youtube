import React from 'react'
import styles from './styles'
import Images from '../../utils/Images'
import { awaitFunction, localize } from '../../services'
import ButtonIcon from '../UI/ButtonIcon/ButtonIcon'
import EditIcon from '@material-ui/icons/AddAPhoto'
import Dimensions from '../../utils/Dimensions'

class UserImage extends React.Component {

  constructor (props) {
    super(props)
    this.state = {hovered: false}
  }

  setImageHovered = async (state) => {
    await awaitFunction(this.setState({hovered: state}))
  }

  uploadImage = () => {
    this.upload.click()
  }

  renderOptionForUploadImage () {
    const {editImage} = this.props
    if (editImage) {
      return <div style={styles.userImageAddButtonContainer}>
        <div style={styles.imageUploadButton}>
          <ButtonIcon label={localize('submit')} buttonType='neutral' type='submit' handleAction={this.uploadImage}>
            <EditIcon />
          </ButtonIcon>
        </div>
      </div>
    }
  }


  render () {
    const {mini, header, src, imageUploadCallBack} = this.props
    const {hovered} = this.state
    return (<div
      style={header ? styles.userImageHeaderContainer : styles.userImageContainer}
    >
      <div style={mini ? styles.miniUserImage2 : Object.assign({}, styles['userImage' + Dimensions.getDevice()], styles.userImageRound)} onMouseEnter={() => this.setImageHovered(true)}
        onMouseLeave={() => this.setImageHovered(false)}>
        <img
          src={src !== null ? src : Images.userAvatar}
          style={mini ? styles.miniUserImage : Object.assign({}, styles['userImage' + Dimensions.getDevice()], styles.userImage)}
          alt='cover'
        />
        {hovered === true ?
          this.renderOptionForUploadImage()
          : null}

        <input
          ref={(ref) => this.upload = ref}
          accept='image/*'
          style={{display: 'none'}}
          id='contained-button-file-2'
          type='file'
          onChange={(event) => imageUploadCallBack(event.target.files)}
        />
      </div>
    </div>)
  }
}

export default UserImage
