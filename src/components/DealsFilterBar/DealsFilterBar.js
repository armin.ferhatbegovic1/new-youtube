import styles from '../../containers/UnpublishedDealsScreen/styles'
import React, { Fragment } from 'react'
import Chip from '@material-ui/core/Chip/Chip'
import DoneIcon from '@material-ui/icons/Done'
import Colors from '../../utils/Colors'
import Dimensions from '../../utils/Dimensions'
import { deviceType } from '../../config/Constants'

export const DealsFilterBar = (props) => {
  const {filterItems,changeFilterState} = props
  return <Fragment><div style={Dimensions.getDevice()!== deviceType.PHONE?styles.filterContainer:styles.mobileFilterContainer}>
    {filterItems.map((filter,i) => {
        return <Chip
          key={filter.label}
          label={filter.label}
          style={Object.assign({}, styles.filterChip,filter.value?styles.chipEnable :styles.chipDisable)}
          onClick={() => {changeFilterState(i)}}
          onDelete={() => {}}
          deleteIcon={<DoneIcon style={{color: filter.value ? 'white' : Colors.primaryDark}} />}
        />
      }
    )}
  </div>
    {Dimensions.getDevice()=== deviceType.PHONE?<div>usoo</div>:null    }
  </Fragment>

}