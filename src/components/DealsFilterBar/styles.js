import RootComponentStyles from '../Styles/RootComponentStyles'
import Dimensions from '../../utils/Dimensions'

const styles = {
  ...RootComponentStyles,
  filterContainer: {
    flex:1,
    margin:Dimensions.doubleBaseMargin
  },
  filterChip:{
    margin:Dimensions.baseMargin
  }
}

export default styles