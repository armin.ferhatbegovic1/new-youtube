import { menuItemType, role } from '../../config/Constants'
import { localize } from '../../services'

export const menuItems = [
  {
    type: menuItemType.TITLE,
    name: localize('deals'),
    role: [role.ADMIN, role.BRAND,role.INFLUENCER]
  },
  {
    type: menuItemType.LINK,
    name: 'New Video',
    path:'/new-video',
    role: [role.ADMIN, role.BRAND]
  },
  {
    type: menuItemType.LINK,
    name: localize('videoList'),
    path:'/video-list',
    role: [role.ADMIN, role.BRAND]
  }
]
