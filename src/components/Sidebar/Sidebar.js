import React, { Component } from 'react'
import classNames from 'classnames'
import Drawer from '@material-ui/core/Drawer'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import Chevronright from '@material-ui/icons/ChevronRight'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { withStyles } from '@material-ui/core/styles'
import { menuItems } from './helpers'
import styles from './styles'
import { deviceType, menuItemType, role } from '../../config/Constants'
import Typography from '@material-ui/core/Typography/Typography'
import Divider from '@material-ui/core/Divider'
import Dimensions from '../../utils/Dimensions'
import Logo from '../UI/Logo/Logo'
import { awaitFunction, isEmpty } from '../../services'

class Sidebar extends Component {
  constructor (props) {
    super(props)
    this.state = {
      menuItemsDefault: null
    }

  }

  goToScreen (path) {
    this.setState({path})
    this.props.history.push(path)
  }

  isActive = (value) => {
    const {classes} = this.props
    const urlPath = window.location.href
    let newPath
    if (!isEmpty(value) && value.hasOwnProperty('pathname')) {
      newPath = value.pathname + value.search
    } else {
      newPath = value
    }
    return urlPath.includes(newPath) ? classes.activeItem : classes.item
  }

  getMenuItem (item, key) {
    const {classes} = this.props
    switch (item.type) {
      case menuItemType.LINK:
        return <div className={classes.menuItem} key={key}>
          <ListItem button onClick={() => {
            this.goToScreen(item.path)
          }} key={key} className={this.isActive(item.path)}>
            <ListItemText
              primary={<Typography variant='h6'
                className={classes.itemText}>{item.name}</Typography>}
            />
            <ListItemIcon><Chevronright /></ListItemIcon>
          </ListItem><Divider /></div>
      case menuItemType.TITLE:
        return <div className={classes.itemTitle} key={key}><span
          className={classes.menuTitle} key={key}>{item.name}</span><Divider /></div>
      default:
        return null
    }
  }

  async componentWillMount () {
    await this.createMenuItemsForInfluencer()
  }

  async componentDidUpdate (prevProps) {

    if (this.props.auth !== prevProps.auth)
    {
      await this.createMenuItemsForInfluencer()
    }
  }

  createMenuItemsForInfluencer = async () => {
    const {auth} = this.props
    const menuItemsDefault = menuItems.slice()
    if (!isEmpty(auth['user'].categories)) {
      const categories = auth['user'].categories
      const menuDealsCategory = []
      categories.forEach((category) => {
        menuDealsCategory.push({
          type: menuItemType.LINK,
          name: category.name,
          path: {
            pathname: '/deals',
            search: '?category=' + category._id
            // state: { detail: response.data }
          },
          role: [role.INFLUENCER]
        })
      })

      for (let i = 0; i < categories.length; i++) {
        menuItemsDefault.splice(2 + i, 0, menuDealsCategory[i])
      }
    }
    await awaitFunction(this.setState({menuItemsDefault}))
  }

  render () {
    const {open, classes, auth} = this.props
    const {menuItemsDefault} = this.state
    return (
      <Drawer
        variant='permanent'
        classes={{
          paper: classNames(
            classes.drawerPaper,
            !open && classes.drawerPaperClose
          )
        }}
        open={open}
      >
        {Dimensions.getDevice() === deviceType.PHONE ?
          <Logo mini={true} header={true} /> : null}
        <List>
          {menuItemsDefault !== null ?
            Object.keys(menuItemsDefault)
            .map((key) => {
              return (
                menuItemsDefault[key].role.indexOf(auth.user.role) !== -1 ?
                  this.getMenuItem(menuItemsDefault[key], key)
                  : null
              )
            })
            : null}
        </List>
      </Drawer>
    )
  }
}

const mapStateToProps = state => {
  return {
    auth: state.auth,
    errors: state.errors
  }
}
export default connect(mapStateToProps)(withRouter(withStyles(styles)(Sidebar)))