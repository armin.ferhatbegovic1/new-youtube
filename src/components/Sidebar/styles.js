import RootComponentStyles from '../Styles/RootComponentStyles'
import Colors from '../../utils/Colors'
import Dimensions from '../../utils/Dimensions'

const drawerWidth = 240

const styles = theme => ({
  ...RootComponentStyles,
  drawerPaper: {
    position: 'fixed',
    top: 70,
    zIndex: 1,
    whiteSpace: 'nowrap',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    })
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    }),
    width: '0!important',
    [theme.breakpoints.up('sm')]: {
      width: 0
    }
  },
  listItem: {
    color: Colors.primary,
    fontWeight: 'bold',
    fontSize: 4,
    textDecoration: 'none!important'
  },
  menuTitle: {
    color: Colors.color5,
    textTransform: 'uppercase',

    fontSize: Dimensions.getWidth() < Dimensions.phone ? Dimensions.textXSmall : Dimensions.textSmall,
    marginTop: Dimensions.tripeBaseMargin,
    marginBottom: Dimensions.doubleBaseMargin,
    marginLeft: Dimensions.baseMargin,
    display: 'flex'
  },
  itemText: {
    textDecoration: 'unset',
    color: '#2db1ff',
    fontSize: Dimensions.getWidth() < Dimensions.phone ? Dimensions.textXSmall : Dimensions.textSmall,
    paddingLeft: 30
  }, item: {
    paddingRight: Dimensions.baseMargin
  },
  activeItem: {
    backgroundColor: Colors.secondaryLight3,
    paddingRight: 0
  },
  menuItem: {
    paddingLeft: 0
  },
  itemTitle: {
    paddingLeft: Dimensions.baseMargin
  }
})

export default styles