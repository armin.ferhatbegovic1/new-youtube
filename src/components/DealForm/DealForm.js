import React, { Component, Fragment } from 'react'
import { awaitFunction, getRequest, isEmpty, localize, postRequest, prepareDealDataForSave } from '../../services'
import { dealForm, submitDealForm } from '../../forms/'
import {
  alertDialogType,
  dealSaveType,
  formDialog,
  formMode,
  layoutType,
  positionButton,
  role,
  toastNotification
} from '../../config/Constants'
import InputForm from '../../components/UI/InputForm/InputForm'
import { getDeal } from '../../services/'
import RenewDealDialog from '../UI/RenewDealDialog/RenewDealDialog'
import { prepareDealDataAndRedirectToPaymentScreen } from '../../services/DealServices'
import styles from '../../containers/EditDealScreen/styles'
import SubMainButton from '../UI/SubMainButton/SubMainButton'
import { dealsEndpoints } from '../../config/Endpoints'
import connect from 'react-redux/es/connect/connect'
import { setToastNotification } from '../../store/actions/toastAction'
import { setAlertDialog } from '../../store/actions/alertDialogAction'
import { setFormDialog } from '../../store/actions/formDialogAction'

const SAVE = 'SAVE'
const SAVE_PAY = 'SAVE_PAY'

class DealForm extends Component {
  constructor (props) {
    super(props)
    this.state = {
      dealForm: JSON.parse(JSON.stringify(dealForm)),
      dealID: !isEmpty(this.props.match) ? this.props.match.params.dealID : null,
      formIsValid: false,
      loading: false,
      mode: !isEmpty(props.mode) ? props.mode : formMode.VIEW,
      selectDurationDialogOpen: false
    }
  }

  changeFormMode = () => {
    this.setState({mode: formMode.EDIT})
  }

  async componentDidMount () {
    this._isMounted = true
    const {dealID} = this.state
    if (!isEmpty(dealID) && this._isMounted) {
      const dealData = await getDeal(dealID)
      this.setState({dealData})
    }
  }

  selectDurationDialogOpen () {
    this.setState({selectDurationDialogOpen: true})
  }

  selectDurationDialogClose = async () => {
    await awaitFunction(this.setState({selectDurationDialogOpen: false}))
  }

  isFormEditable () {
    const {auth} = this.props
    return auth['user'].role !== role.INFLUENCER
  }

  getModeOfSaveDeal (saveMode) {
    const {dealID} = this.state
    if (!isEmpty(dealID))
      return saveMode === SAVE ? dealSaveType.DATA_SAVE : dealSaveType.DATA_SAVE_AND_PAY
    else
      return saveMode === SAVE ? dealSaveType.NEW_DATA_SAVE : dealSaveType.NEW_DATA_SAVE_AND_PAY
  }

  renderSubmitDealButton () {
    const {auth} = this.props
    if (auth['user'].role === role.INFLUENCER) {
      return <div style={styles.cardDealActions}>
        <SubMainButton label={localize('bidToDeal')} buttonType='tertiary' type='button'
          onClick={() => this.checkIsDealSubmited()} />
      </div>
    }
  }

  confirmBidToDeal = async (bidData) => {
    const {auth} = this.props
    const currentUser = auth.user
    const {dealID} = this.state
    bidData['dealId'] = dealID
    bidData['influencerId'] = currentUser['_id']
    return await postRequest(dealsEndpoints.DEAL_BID, bidData, toastNotification.HIDE)
  }

  openModalToSubmitDeal = () => {
    this.props.setFormDialog(formDialog.SHOW, localize('cardDeal.beforeYousubmitaDeal'), localize('cardDeal.submitDealDetails'), alertDialogType.CONFIRM, submitDealForm, this.confirmBidToDeal, null)
  }

  checkIsDealSubmited = async () => {
    const {setAlertDialog} = this.props
    const {dealID} = this.state
    if (this._isMounted) {
      const response = await getRequest(dealsEndpoints.DEAL_BID_CHECK + '/' + dealID)
      if (response['status'] === true) {
        setAlertDialog(true, localize('cardDeal.youHaveAlreadyAppliedToThisDeal'), localize('newDealScreen.dealWarning'), alertDialogType.WARNING)
      } else {
        this.openModalToSubmitDeal()
      }
    }
  }

  getDealForm () {
    const {mode, dealData, dealForm, selectDurationDialogOpen, dealID} = this.state
    const {history} = this.props
    if ((
      (mode === formMode.EDIT) && !isEmpty(dealData)) || mode === formMode.NEW
      || (mode === formMode.VIEW && !isEmpty(dealData))

    ) {
      return <Fragment>
        {this.isFormEditable() ? <RenewDealDialog open={selectDurationDialogOpen} close={this.selectDurationDialogClose}
            submitAction={(paymentInfo) => {prepareDealDataAndRedirectToPaymentScreen(paymentInfo, dealID, history)}} />
          : null}
        <InputForm formElements={dealForm}
          prepareDataAndSubmit={() => prepareDealDataForSave(this.getModeOfSaveDeal(SAVE), this)}
          mode={mode} data={dealData} layout={layoutType.COLUMNS_1} lastElementInLayout={layoutType.COLUMNS_1}
          editFormButton={this.changeFormMode} saveButtonPosition={positionButton.RIGHT} rowCount={4}
          prepareDataAndSubmitAdvanced={() => prepareDealDataForSave(this.getModeOfSaveDeal(SAVE_PAY), this)}
          advanceButtonLabel={localize('newDealScreen.saveAndPublishDeal')} editable={this.isFormEditable()}>
          {this.renderSubmitDealButton()}
        </InputForm>
      </Fragment>
    }
    /*if (mode === formMode.VIEW && !isEmpty(dealData)) {
      return <InputForm formElements={dealForm}
        prepareDataAndSubmit={() => prepareDealDataForSave(this.getModeOfSaveDeal(SAVE), this)}
        mode={mode} data={dealData} layout={layoutType.COLUMNS_1} lastElementInLayout={layoutType.COLUMNS_1}
        editFormButton={this.changeFormMode} saveButtonPosition={positionButton.RIGHT} rowCount={4}
        editable={this.isFormEditable()} />
    }*/
    else return null
  }

  render () {
    return this.getDealForm()
  }
}

const mapStateToProps = state => {
  return {
    auth: state.auth
  }
}

export default connect(mapStateToProps, {setToastNotification, setAlertDialog, setFormDialog})(DealForm)