import RootComponentStyles from '../Styles/RootComponentStyles'
import Colors from '../../utils/Colors'
import Dimensions from '../../utils/Dimensions'

const styles = {
  ...RootComponentStyles,
  card: {
    maxWidth: 400
  },
  media: {
    height: 0,
    paddingTop: '56.25%' // 16:9
  },
  placeholderMedia: {
    height: 0,
    paddingTop: '56.25%', // 16:9
    opacity: '0.4'
  },
  dealActive: {
    backgroundColor: Colors.active
  },
  dealDraft: {
    backgroundColor: Colors.draft
  },
  requirementLabel: {
    fontWeight: 'bold'
  },
  cardButton: {
    display: 'block',
    textAlign: 'initial'
  },
  previewDealLink: {
    textDecoration: 'none',
    color: Colors.black

  },
  placeholder: {
    opacity: '0.1'
  },
  cardDealLargeDesktop: {
    fontSize: Dimensions.textMedium
  },
  cardDealTitleDesktop: {
    fontSize: Dimensions.textSmall
  },

  cardDealTitleTablet: {
    fontSize: Dimensions.textSmall
  },
  cardDealTitlePhone: {
    fontSize: Dimensions.textXSmall
  }

}
export default styles










