import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Card, Grid } from '@material-ui/core'
import { Progress } from 'react-sweet-progress'
import 'react-sweet-progress/lib/style.css'
import styles from './styles'
import { circularLoader } from '../../config/Constants'
import { isEmpty } from '../../services'
import { getHeaderHeight } from '../../services/GlobalServices'

class FinalResponseMessage extends Component {

  constructor (props) {
    super(props)
    this.state = {
      percent: 0,
      seconds: 0,
      redirect: false,
      heightContainer:'100vh'
    }
  }

  tick () {
    const {percent} = this.state
    this.setState(prevState => ({
      seconds: prevState.seconds + 1,
      percent: percent === 100 ? 100 : prevState.percent + 1,
      redirect: percent === 100 ? true : false
    }))

  }





  componentDidMount () {
    const heightHeader = getHeaderHeight()
    if(!isEmpty(heightHeader) && heightHeader!==0){
    this.setState({heightContainer:'89.5vh'})
    }
    this.interval = setInterval(() => this.tick(), 20)
  }

  componentWillUnmount () {
    clearInterval(this.interval)
  }

  getProgressThemeByType () {
    const {type} = this.props
    switch (type) {
      case circularLoader.SUCCESS:
        return {}
      case circularLoader.PROGRESS:
        return {
          success: {
            symbol: '👇',
            color: 'orange'
          }
        }
      case circularLoader.FAILURE:
        return {
          success: {
            symbol: '👇',
            color: 'red'
          }
        }
      default: return {}
    }

  }

  render () {
    const {percent, redirect,heightContainer} = this.state
    const {children, url} = this.props

    if (!isEmpty(url) && redirect) {
      window.location.href = url
    }

    return (
      <Grid
        item
        xs={12}
        style={styles.paddingFullStandard}>
        <Card>
            <div style={Object.assign({}, styles.flexContainer,{  height:heightContainer})}>
            <Progress type='circle' percent={percent} status='success' theme={this.getProgressThemeByType()} />
            {children}
          </div>
        </Card>

      </Grid>
    )
  }
}

const mapStateToProps = state => {
  return {
    auth: state.auth
  }
}

export default connect(mapStateToProps)(FinalResponseMessage)

