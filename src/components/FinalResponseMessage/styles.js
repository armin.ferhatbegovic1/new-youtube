import RootContainerStyles from '../../containers/Styles/RootContainerStyles'
import Dimensions from '../../utils/Dimensions'
import Colors from '../../utils/Colors'

const styles  = ({
  ...RootContainerStyles,
  flexContainer: {
    height: '100vh',
    padding: 0,
    margin: 0,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column'
  },
  subTitle:{
    fontWeight: 'bold',
    fontSize:Dimensions.textLarge,
    color:Colors.color5
  },

})

export default styles