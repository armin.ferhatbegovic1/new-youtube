import { getDataFromURL, isEmpty, localize, postRequest } from '../services'
import {
  alertDialogType,
  autoCloseDialog,
  formDialog,
  inputTypeFormElement,
  toastNotification
} from '../config/Constants'
import store from '../store/index'
import { brandChangePasswordForm } from './changePassword'
import { setFormDialog } from '../store/actions/formDialogAction'
import { userEndpoints } from '../config/Endpoints'


