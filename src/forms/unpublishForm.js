import { localize } from '../services'
import { inputTypeFormElement } from '../config/Constants'

export const unpublishForm= {
  label: localize('cardDeal.reason'),
    value: '',
    errorMessages: localize('thisFieldIsRequired'),
    validation: {
    required: true
  },
  valid: true,
    inputType: inputTypeFormElement.TEXT_AREA
}