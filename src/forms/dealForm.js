import { localize } from '../services'
import { inputTypeFormElement } from '../config/Constants'
export const numberOfFollowersList = ['10', '10000', '50000', '100000', '1000000']

export const dealForm = {
  name: {
    label: localize('newDealScreen.dealName'),
    value: '',
    errorMessages: localize('thisFieldIsRequired'),
    validation: {
      required: true
    },
    valid: true
  },
  category: {
    label: localize('newDealScreen.dealCategory'),
    placeholder: localize('newDealScreen.selectCategories'),
    value: [],
    errorMessages: localize('thisFieldIsRequired'),
    validation: {
      multiple: true,
      required: true,
      minLength: 1,
      maxLength: 3
    },
    inputType: inputTypeFormElement.SELECT_LIST_CATEGORIES,
    valid: true
  },
  budget: {
    label: localize('newDealScreen.dealBudget'),
    value: '',
    errorMessages: localize('thisFieldIsRequired'),
    validation: {
      required: true,
      isNumeric: true
    },
    valid: true
  },
  description: {
    label: localize('newDealScreen.dealDescription'),
    value: '',
    errorMessages: localize('thisFieldIsRequired'),
    validation: {
      required: true
    },
    multiline:true,
    valid: true
  },
  numberOfFollowers: {
    label: localize('newDealScreen.selectNumberOfFollowers'),
    value: '',
    errorMessages: localize('thisFieldIsRequired'),
    validation: {
      required: true
    },
    inputType: inputTypeFormElement.SELECT_LIST,
    itemsList:numberOfFollowersList,
    valid: true
  },
  worldwideDeal: {
    label: localize('newDealScreen.worldwideDeal'),
    value: false,
    errorMessages: '',
    inputType: inputTypeFormElement.SWITCH,
    valid: true,
    validation: {}
  },
  country: {
    label: localize('newDealScreen.influencerCountry'),
    placeholder: localize('newDealScreen.selectCountry'),
    value: null,
    errorMessages: localize('thisFieldIsRequired'),
    validation: {
      required: 'dependence',
      dependenceField: {
        fieldName: 'worldwideDeal',
        value: false
      }
    },
    visibility: {
      dependenceField: {
        fieldName: 'worldwideDeal',
        value: true
      }
    },
    inputType: inputTypeFormElement.COUNTRY_AUTO_SUGGEST,
    valid: true,
    id: null
  },
  city: {
    label: localize('newDealScreen.influencerCity'),
    placeholder: localize('newDealScreen.selectCity'),
    value: '',
    errorMessages: localize('thisFieldIsRequired'),
    validation: {
      required: false
    },
    inputType: inputTypeFormElement.CITY_AUTO_SUGGEST,
    valid: true,
    id: null,
    visibility: {
      dependenceField: {
        fieldName: 'worldwideDeal',
        value: true
      }
    }
  },
  image: {
    label:localize('newDealScreen.uploadImage'),
    value: '',
    errorMessages: localize('thisFieldIsRequired'),
    validation: {
      required: true
    },
    inputType: inputTypeFormElement.UPLOAD_EDIT_IMAGE,
    valid: true
  }
}

export const submitDealForm = {
  numberOfPosts: {
    label: localize('cardDeal.numberOfFeedPost'),
    value: '',
    errorMessages: localize('thisFieldIsRequired'),
    validation: {
      required: true,
      isNumeric: true
    },
    valid: true
  },
  numberOfStories: {
    label: localize('cardDeal.numberOfStories'),
    value: '',
    errorMessages: localize('thisFieldIsRequired'),
    validation: {
      required: true,
      isNumeric: true
    },
    valid: true
  },
  additionalInfo: {
    label: localize('cardDeal.additionInformations'),
    value: '',
    errorMessages: localize('thisFieldIsRequired'),
    validation: {
      required: false
    },
    inputType: inputTypeFormElement.TEXT_AREA,
    valid: true
  }
}