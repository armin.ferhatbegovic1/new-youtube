import { localize } from '../services'
import { inputTypeFormElement } from '../config/Constants'

export const cancelReasonForm= {
  label: localize('cardBid.reason'),
    value: '',
    errorMessages: localize('thisFieldIsRequired'),
    validation: {
    required: true
  },
  valid: true,
    inputType: inputTypeFormElement.TEXT_AREA
}