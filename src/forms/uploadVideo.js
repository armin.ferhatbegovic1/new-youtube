import {localize} from '../services'
import {inputTypeFormElement} from "../config/Constants";
const classificationList=['by sentence','by word']
export const uploadVideoForm = {
    title: {
        label: localize('title'),
        value: '',
        errorMessages: localize('thisFieldIsRequired'),
        validation: {
            required: true
        },
        valid: true
    },

    difficulty: {
        label: 'Difficulty',
        value: '',
        errorMessages: localize('thisFieldIsRequiredAndNumeric'),
        validation: {
            required: true
        },
        valid: true
    },
    video_duration: {
        label: 'Video Duration',
        value: '',
        errorMessages: localize('thisFieldIsRequiredAndNumeric'),
        validation: {
            required: false
        },
        valid: true
    },
  classification_type: {
    label: 'Classification type',
    value: '',
    errorMessages: localize('thisFieldIsRequiredAndNumeric'),
    validation: {
      required: false
    },
    valid: true,
    inputType: inputTypeFormElement.SELECT_LIST,
    itemsList: classificationList,
  },description: {
        label: 'Description',
        value: '',
        errorMessages: localize('thisFieldIsRequired'),
        validation: {
            required: true
        },
        valid: true
    }
}

export const videoCoverForm = {
    videoCover: {
        label: localize('registrationScreen.email'),
        value: null,
        errorMessages: localize('thisFieldIsRequired'),
        validation: {
            required: false
        },
        inputType: inputTypeFormElement.IMAGE,
        valid: true
    }
}


export const audioFileForm = {
    videoCover: {
        label: localize('registrationScreen.email'),
        value: null,
        errorMessages: localize('thisFieldIsRequired'),
        validation: {
            required: false
        },
        inputType: inputTypeFormElement.MUSIC,
        valid: true
    },
    text: {
        label: 'Text',
        value: '',
        errorMessages: localize('thisFieldIsRequired'),
        validation: {
            required: false
        },
        valid: true,
    },
    start: {
        label: 'Start',
        value: '',
        errorMessages: localize('thisFieldIsRequired'),
        validation: {
            required: true
        },
        valid: true,
    },
    end: {
        label: 'End',
        value: '',
        errorMessages: localize('thisFieldIsRequired'),
        validation: {
            required: true
        },
        valid: true,
    },
    text_translated: {
        label: 'Text Translated',
        value: '',
        errorMessages: localize('thisFieldIsRequired'),
        validation: {
            required: true
        },
        valid: true,
        inputType: inputTypeFormElement.TEXT_AREA
    },

}


export const audioFileFormPreview = {
    audio_file_uuid: {
        label: localize('registrationScreen.email'),
        value: null,
        errorMessages: localize('thisFieldIsRequired'),
        validation: {
            required: false
        },
        inputType: inputTypeFormElement.MUSIC,
        valid: true
    },
    text: {
        label: 'Text',
        value: '',
        errorMessages: localize('thisFieldIsRequired'),
        validation: {
            required: false
        },
        valid: true,
    },
    start: {
        label: 'Start',
        value: '',
        errorMessages: localize('thisFieldIsRequired'),
        validation: {
            required: true
        },
        valid: true,
    },
    end: {
        label: 'End',
        value: '',
        errorMessages: localize('thisFieldIsRequired'),
        validation: {
            required: true
        },
        valid: true,
    },
    text_translated: {
        label: 'Text Translated',
        value: '',
        errorMessages: localize('thisFieldIsRequired'),
        validation: {
            required: true
        },
        valid: true,
        inputType: inputTypeFormElement.TEXT_AREA
    },
}
