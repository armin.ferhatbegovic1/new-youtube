import { localize } from '../services'
import { inputTypeFormElement, PASSWORD_MIN_CHAR } from '../config/Constants'
import { errorMessageForMinCharInputField } from '../services/GlobalServices'

export const brandLoginForm= {
  user_id: {
    label: localize('loginScreen.userID'),
    value: '',
    errorMessages: localize('thisFieldIsRequired'),
    validation: {
      required: true,
    },
    valid: true
  },
  password: {
    label: localize('loginScreen.password'),
    value: '',
    errorMessages: localize('thisFieldIsRequired'),
    validation: {
      required: true
    },
    type: 'password',
    valid: true
  }
}

export const brandRegistrationForm= {
  email: {
    label: localize('registrationScreen.email'),
    value: '',
    errorMessages: localize('thisFieldIsRequired'),
    validation: {
      required: true,
      isEmail: true
    },
    valid: true
  },
  fullName: {
    label: localize('registrationScreen.fullName'),
    value: '',
    errorMessages: localize('thisFieldIsRequired'),
    validation: {
      required: true
    },
    valid: true
  },
  password: {
    label: localize('registrationScreen.password'),
    value: '',
    errorMessages: errorMessageForMinCharInputField(PASSWORD_MIN_CHAR),
    type: 'password',
    validation: {
      required: true,
      minLength: PASSWORD_MIN_CHAR
    },
    valid: true
  },
  repeatPassword: {
    label: localize('registrationScreen.repeatPassword'),
    value: '',
    errorMessages: localize('passwordDoesNotMatch'),
    type: 'password',
    validation: {
      required: 'dependence',
      minLength: PASSWORD_MIN_CHAR,
      dependenceField: {
        fieldName: 'password',
        operation: 'equal'
      }
    },
    valid: true
  },
  policyAccepted: {
    label: localize('registrationScreen.agreeToTermsOfUser'),
    value: false,
    errorMessages: localize('thisFieldIsRequired'),
    validation: {
      required: true,
      minLength: PASSWORD_MIN_CHAR
    },
    inputType: inputTypeFormElement.CHECKBOX,
    valid: true
  }
}
