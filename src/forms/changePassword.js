import { localize } from '../services'
import { errorMessageForMinCharInputField } from '../services/GlobalServices'
import { PASSWORD_MIN_CHAR } from '../config/Constants'



export const brandChangePasswordForm = {
  oldPassword: {
    label: localize('profileScreen.oldPassword'),
    value: '',
    errorMessages: errorMessageForMinCharInputField(PASSWORD_MIN_CHAR),
    type: 'password',
    validation: {
      required: true,
      minLength: PASSWORD_MIN_CHAR
    },
    valid: true
  },
  newPassword: {
    label: localize('profileScreen.newPassword'),
    value: '',
    errorMessages: errorMessageForMinCharInputField(PASSWORD_MIN_CHAR),
    type: 'password',
    validation: {
      required: true,
      minLength: PASSWORD_MIN_CHAR
    },
    valid: true
  },
  repeatPassword: {
    label: localize('profileScreen.passwordConfirmation'),
    value: '',
    errorMessages: localize('passwordDoesNotMatch'),
    type: 'password',
    validation: {
      required: 'dependence',
      minLength: PASSWORD_MIN_CHAR,
      dependenceField: {
        fieldName: 'newPassword',
        operation: 'equal'
      }
    },
    valid: true
  }
}