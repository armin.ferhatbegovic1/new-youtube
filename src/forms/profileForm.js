import { getDataFromURL, isEmpty, localize, postRequest } from '../services'
import {
  alertDialogType,
  autoCloseDialog,
  formDialog,
  inputTypeFormElement,
  toastNotification
} from '../config/Constants'
import store from '../store/index'
import { brandChangePasswordForm } from './changePassword'
import { setFormDialog } from '../store/actions/formDialogAction'
import { userEndpoints } from '../config/Endpoints'

const languageList = ['English', 'Germany']

export const userProfileForm = {
  profileImage: {
    label: localize('registrationScreen.email'),
    value: null,
    errorMessages: localize('thisFieldIsRequired'),
    validation: {
      required: false
    },
    inputType: inputTypeFormElement.IMAGE,
    valid: true
  },
  aboutMe: {
    label: localize('profileScreen.aboutMe'),
    value: '',
    errorMessages: localize('thisFieldIsRequired'),
    validation: {
      required: false
    },
    valid: true,
    multiline: true,
    disabled: !isEmpty(getDataFromURL('fullName'))
    // inputType: inputTypeFormElement.TEXT_AREA
  },
  email: {
    label: localize('registrationScreen.email'),
    value: '',
    errorMessages: localize('thisFieldIsRequired'),
    validation: {
      required: true,
      isEmail: true
    },
    valid: true
  },
  language: {
    label: localize('profileScreen.selectLanguage'),
    value: [],
    errorMessages: localize('thisFieldIsRequired'),
    validation: {
      required: false
    },
    inputType: inputTypeFormElement.SELECT_LIST,
    itemsList: languageList,
    valid: true
  },
  fullName: {
    label: localize('registrationScreen.fullName'),
    value: getDataFromURL('fullName'),
    errorMessages: localize('thisFieldIsRequired'),
    validation: {
      required: true
    },
    valid: true,
    disabled: !isEmpty(getDataFromURL('fullName'))
  }
}

export const instagramProfileForm = {
  ...userProfileForm,
  instagramUsername: {
    label: localize('registrationScreen.instagramUserName'),
    value: '',
    errorMessages: localize('thisFieldIsRequired'),
    validation: {
      required: true
    },
    valid: true,
    disabled: true
  },
  categoriesOfInterest: {
    label: localize('newDealScreen.dealCategory'),
    placeholder: localize('newDealScreen.selectCategories'),
    value: [],
    errorMessages: localize('thisFieldIsRequired'),
    validation: {
      multiple: true,
      required: true,
      minLength: 1,
      maxLength: 3
    },
    inputType: inputTypeFormElement.SELECT_LIST_CATEGORIES,
    valid: true
  },
  worldwide: {
    label: localize('newDealScreen.worldwideDeal'),
    errorMessages: '',
    value: false,
    valid: true,
    inputType: inputTypeFormElement.SWITCH,
    validation: {}
  },
  followers: {
    label: localize('profileScreen.followersOnInstagram'),
    value: '',
    errorMessages: localize('thisFieldIsRequired'),
    validation: {
      required: false
    },
    valid: true,
    disabled: true
  },
  countryOfInterest: {
    label: localize('registrationScreen.country'),
    placeholder: localize('newDealScreen.selectCountry'),
    value: null,
    errorMessages: localize('thisFieldIsRequired'),
    validation: {
      required: 'dependence',
      dependenceField: {
        fieldName: 'worldwide',
        value: false
      }
    },
    visibility: {
      dependenceField: {
        fieldName: 'worldwide',
        value: true
      }
    },
    inputType: inputTypeFormElement.COUNTRY_AUTO_SUGGEST,
    valid: true,
    id: null
  },
  cityOfInterest: {
    label: localize('registrationScreen.city'),
    placeholder: localize('newDealScreen.selectCity'),
    value: '',
    errorMessages: localize('thisFieldIsRequired'),
    validation: {
      required: false
    },
    inputType: inputTypeFormElement.CITY_AUTO_SUGGEST,
    valid: true,
    id: null,
    visibility: {
      dependenceField: {
        fieldName: 'worldwide',
        value: true
      }
    }
  }
}

export const brandProfileForm =(user)=> ({
  ...userProfileForm,
  name: {
    label: localize('profileScreen.companyName'),
    value: '',
    errorMessages: localize('thisFieldIsRequired'),
    validation: {
      required: true
    },
    valid: true
  },
  vat: {
    label: localize('profileScreen.vat'),
    value: '',
    errorMessages: localize('thisFieldIsRequired'),
    valid: true,
    validation: {
      required: false
    }
  },
  country: {
    label: localize('registrationScreen.country'),
    placeholder: localize('newDealScreen.selectCountry'),
    value: null,
    errorMessages: localize('thisFieldIsRequired'),
    inputType: inputTypeFormElement.COUNTRY_AUTO_SUGGEST,
    valid: true,
    id: null,
    validation: {
      required: true
    }
  },
  address: {
    label: localize('profileScreen.companyAddress'),
    value: '',
    errorMessages: localize('thisFieldIsRequired'),
    validation: {
      required: false
    },
    valid: true
  },
  city: {
    label: localize('registrationScreen.city'),
    placeholder: localize('newDealScreen.selectCity'),
    value: '',
    errorMessages: localize('thisFieldIsRequired'),
    validation: {
      required: false
    },
    inputType: inputTypeFormElement.CITY_AUTO_SUGGEST,
    valid: true,
    id: null,
    disabled: false
  },
  positionInCompany: {
    label: localize('profileScreen.position'),
    value: '',
    errorMessages: localize('thisFieldIsRequired'),
    validation: {
      required: false
    },
    valid: true
  },
  changePassword: {
    label: localize('profileScreen.changePassword'),
    value: null,
    errorMessages: localize('thisFieldIsRequired'),
    inputType: inputTypeFormElement.MAIN_BUTTON,
    actionCallBack: ()=>openDialogForChangePassword(user),
    valid: true,
    validation: {
      required: false
    }
  }
})

function openDialogForChangePassword (user) {
  store.dispatch(setFormDialog(formDialog.SHOW, localize('profileScreen.changeYourPassword'), localize('profileScreen.password'), alertDialogType.CONFIRM, brandChangePasswordForm, (data) => {return changePassword(data, user)},null, autoCloseDialog.AUTO_CLOSE_DIALOG_FALSE))

}

async function changePassword (data, user) {
 return await postRequest(userEndpoints.BRAND_CHANGE_PASSWORD + '/' + user['_id'], data, toastNotification.HIDE, null, localize('profileScreen.passwordChangedSuccessfully'))
}



