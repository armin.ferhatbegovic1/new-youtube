import { getDataFromURL, isEmpty, localize } from '../services'
import { inputTypeFormElement } from '../config/Constants'

export const influencerRegistrationForm= {
  email: {
    label: localize('registrationScreen.email'),
    value: '',
    errorMessages: localize('thisFieldIsRequired'),
    validation: {
      required: true,
      isEmail: true
    },
    valid: true
  },
  fullName: {
    label: localize('registrationScreen.fullName'),
    value: getDataFromURL('fullName'),
    errorMessages: localize('thisFieldIsRequired'),
    validation: {
      required: true
    },
    valid: true,
    disabled: !isEmpty(getDataFromURL('fullName'))
  },
  instagramUserName: {
    label: localize('registrationScreen.instagramUserName'),
    value: getDataFromURL('userName'),
    errorMessages: localize('thisFieldIsRequired'),
    validation: {
      required: true
    },
    valid: true,
    disabled: !isEmpty(getDataFromURL('userName'))
  },
  category: {
    label: localize('newDealScreen.dealCategory'),
    placeholder: localize('newDealScreen.selectCategories'),
    value: [],
    errorMessages: localize('thisFieldIsRequired'),
    validation: {
      multiple: true,
      required: true,
      minLength: 1,
      maxLength: 3
    },
    inputType: inputTypeFormElement.SELECT_LIST_CATEGORIES,
    valid: true
  },
  worldwide: {
    label: localize('newDealScreen.worldwideDeal'),
    value: false,
    errorMessages: '',
    valid: true,
    inputType: inputTypeFormElement.SWITCH,
    validation: {}
  },
  country: {
    label: localize('registrationScreen.country'),
    placeholder: localize('newDealScreen.selectCountry'),
    value: null,
    errorMessages: localize('thisFieldIsRequired'),
    validation: {
      required: 'dependence',
      dependenceField: {
        fieldName: 'worldwide',
        value: false
      }
    },
    visibility: {
      dependenceField: {
        fieldName: 'worldwide',
        value: true
      }
    },
    inputType: inputTypeFormElement.COUNTRY_AUTO_SUGGEST,
    valid: true,
    id: null
  },
  city: {
    label: localize('registrationScreen.city'),
    placeholder: localize('newDealScreen.selectCity'),
    value: '',
    errorMessages: localize('thisFieldIsRequired'),
    validation: {
      required: false
    },
    inputType: inputTypeFormElement.CITY_AUTO_SUGGEST,
    valid: true,
    id: null,
    visibility: {
      dependenceField: {
        fieldName: 'worldwide',
        value: true
      }
    }
  },
  policyAccepted: {
    label: localize('registrationScreen.agreeToTermsOfUser'),
    value: false,
    errorMessages: localize('thisFieldIsRequired'),
    validation: {
      required: true
    },
    inputType: inputTypeFormElement.CHECKBOX,
    valid: true
  }
}
