import RootContainerStyles from '../Styles/RootContainerStyles'
import Colors from '../../utils/Colors'
import Dimensions from '../../utils/Dimensions'

const styles = {
    ...RootContainerStyles,
    chipEnable: {
        backgroundColor: Colors.secondary
    },
    chipDisable: {
        backgroundColor: Colors.secondaryLight
    },
    noResultsContainer: {
        color: Colors.secondary,
        fontSize: Dimensions.textX4Large,
        marginLeft: Dimensions.doubleBaseMargin
    },
    grow: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: 20,
    },
    title: {
        display: 'none',
    },
    search: {
        position: 'relative',
        borderRadius: 10,
        marginRight: 20,
        marginLeft: 60,

    },
    searchIcon: {
        width: 60,
        height: 60,
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center', marginLeft: '-60px'
    },
    inputRoot: {
        color: 'inherit',
    },
    inputInput: {
        padding: 10,
        width: '100%',
    },
    sectionDesktop: {
        display: 'none',
    },
    sectionMobile: {
        display: 'flex',
    },
    container: {
        marginLeft: 20,
        marginRight: 10,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        fontSize:15
    },
    inputForm: {
        marginRight:20
    }
}

export default styles