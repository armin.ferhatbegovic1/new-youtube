/** @flow */
import React, {Component, Fragment} from 'react'
import {awaitFunction, deleteRequest, isEmpty, localize} from '../../services'
import ContainerToolbar from '../../components/UI/ContainerToolbar/ContainerToolbar'
import TableSimple from '../../components/UI/TableSimple/TableSimple'
import MiniPanelList from '../../components/UI/MiniPanelList/MiniPanelList'
import {alertDialogType, dateFormat, table, toastNotification} from '../../config/Constants'
import {getRequestVideo} from '../../services/RequestManager/RequestManager'
import apiEndpoint from '../../config/config'
import Switch from '@material-ui/core/Switch/Switch'
import FormControlLabel from '@material-ui/core/FormControlLabel/FormControlLabel'
import FormGroup from '@material-ui/core/FormGroup/FormGroup'
import {changeDateFormat} from "../../components/ListCardDeals/helpers";
import TextField from "@material-ui/core/TextField";
import connect from "react-redux/es/connect/connect";
import {setAlertDialog} from "../../store/actions/alertDialogAction";
import SearchIcon from '@material-ui/icons/Search'
import RefreshIcon from '@material-ui/icons/Refresh'
import ButtonIcon from "../../components/UI/ButtonIcon/ButtonIcon";
import styles from './styles'

export const transactionTableHeaderDataConfig = [
    {label: localize('videoListScreen.title'), name: 'title'},
    {label: localize('videoListScreen.uploadTime'), name: 'created_on'},
    {label: localize('videoListScreen.difficulty'), name: 'difficulty'},
    {label: localize('videoListScreen.image'), name: 'cover_file_uuid', property: 'image'},
    {label: localize('actions'), name: 'actions'}
]

class InvoicesListScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
            data: null,
            totalPages: 0,
            totalCount: 0,
            currentPage: 0,
            activeDealsActions: null,
            selectDurationDialogOpen: false,
            noResultsMessageVisible: false,
            isRead: false,
            search: ''
        }
    }

    async getAndLoadTransactionData(page, title = null) {
        const data = await this.getTransactions(page, title)
        await this.loadTransactionsData(data['payload'], page)
    }

    async componentDidMount() {
        const {currentPage} = this.state
        await this.getAndLoadTransactionData(currentPage)
    }

    setTablePage = async (page) => {
        await this.getAndLoadTransactionData(page)
    }

    async getTransactions(page, title = null) {
        let param = '&per_page=10&order_by=title&order_dir=desc&order_by=created_on&is_composed=false&is_read=' + this.state.isRead.toString()
        if (!isEmpty(title)) {
            param += '&title=*' + title + '*'
        }
        return await getRequestVideo(apiEndpoint.apiEndpoint + '/video/list?page=' + page + param)
    }

    async componentDidUpdate(prevProps) {
        if (prevProps.sortData !== this.props.sortData) {
            await this.getAndLoadTransactionData(0)
        }
    }

    changeProperty(data) {
        return data.map((item, index) => {
            return data[index].url = apiEndpoint.apiEndpoint + '/file/' + item['video_file_uuid']
        })
    }

    async loadTransactionsData(data, currentPage) {
        if (!isEmpty(data) && data['total'] !== 0) {
            this.changeProperty(data['items'])
            data['items'] = changeDateFormat(data['items'], 'created_on', dateFormat.MMMMDDYYYY)
            await awaitFunction(this.setState({
                data: data['items'],
                currentPage: currentPage,
                totalCount: data['total'],
                totalPages: this.calculateTotalPages(data['total']),
                noResultsMessageVisible: false
            }))

        } else {
            this.setState({noResultsMessageVisible: true})
        }
    }

    calculateTotalPages(totalCount) {
        return Math.ceil(totalCount / table.TABLE_ROWS_PER_PAGE)
    }

    async openPDFinNewTab(transactionID) {

    }

    viewVideo = (data) => {
        const {history} = this.props
        history.push('/preview-video?id=' + data)
    }


    modifyVideo = (data) => {
        const {history} = this.props
        history.push('/edit-video?id=' + data)
    }

    renderToolbar() {
        return <ContainerToolbar title='Video List'>
            {this.renderSearchBar()}
        </ContainerToolbar>
    }

    confirmDeleteAction = async (videoID) => {
        await deleteRequest('/video/' + videoID, toastNotification.SHOW, 'Video Record Deleted')
        await this.getAndLoadTransactionData(0)
    }
    openModalToConfirmDelete = (data) => {
        const {setAlertDialog} = this.props
        setAlertDialog(true, localize('deleteDealMessage'), localize('confirmDelete'), alertDialogType.CONFIRM, () => this.confirmDeleteAction(data))
    }

    renderTransactionsTable() {
        const {data, totalPages, totalCount, currentPage} = this.state
        return !isEmpty(data)
            ? <TableSimple
                currentPage={currentPage}
                setTablePage={this.setTablePage}
                totalPages={totalPages}
                totalCount={totalCount}
                size={[12, 12, 12]}
                header={transactionTableHeaderDataConfig}
                data={data}
                fields={transactionTableHeaderDataConfig}
                title='Video List'
                flexWidth
                actions={[{
                    actionLabel: localize('videoListScreen.view'),
                    action: this.viewVideo
                },
                    {
                        actionLabel: localize('videoListScreen.delete'),
                        action: this.openModalToConfirmDelete
                    },
                    {
                        actionLabel: localize('videoListScreen.modify'),
                        action: this.modifyVideo
                    }]}
                actionsProperty={'video_uuid'}
            />
            : null
    }

    renderTransactionsTableTabletMobile() {
        const {data, totalPages, totalCount, currentPage} = this.state
        return !isEmpty(data) ? <MiniPanelList currentPage={currentPage}
                                               setTablePage={this.setTablePage}
                                               totalPages={totalPages}
                                               totalCount={totalCount}
                                               data={data} fields={transactionTableHeaderDataConfig}
        /> : null
    }

    renderTransactionsData() {
        const {noResultsMessageVisible} = this.state
        if (noResultsMessageVisible) {
            return <label>No Results</label>
        }
        /*  if (Dimensions.getWidth() < Dimensions.desktop) {
            return this.renderTransactionsTableTabletMobile()
          }*/
        return this.renderTransactionsTable()
    }

    handleChange = async () => {
        const {isRead} = this.state
        await awaitFunction(this.setState({isRead: !isRead}))
        await this.getAndLoadTransactionData(0)
    }

    renderIsRead() {
        const {isRead} = this.state
        return <div style={{marginLeft: 30}}><FormGroup row><FormControlLabel
            control={
                <Switch
                    checked={isRead}
                    onChange={() => this.handleChange()}
                    value='checkedB'
                    color='primary'
                />
            }
            label='Is Read'
        /></FormGroup></div>
    }

    searchByTitle = async () => {
        await this.getAndLoadTransactionData(0, this.state.search)
    }
    refresh = async () => {
        await this.getAndLoadTransactionData(0)
    }

    handleChange = event => {
        this.setState({search: event.target.value});
    };

    renderSearchBar() {
        return <form style={styles.container} noValidate autoComplete="off"><TextField
            id="outlined-name"
            label="Search Video By Title"
            value={this.state.search}
            onChange={this.handleChange}
            margin="normal"
            variant="outlined"
            style={styles.inputForm}
        />
            <ButtonIcon  buttonType='primary' type='submit' handleAction={this.searchByTitle}>
                <SearchIcon/>
            </ButtonIcon>
            <div style={{margin:10}}></div>
            <ButtonIcon  buttonType='secondary' type='submit' handleAction={this.refresh}>
                <RefreshIcon/>
            </ButtonIcon>
        </form>
    }

    render() {
        return (
            <Fragment>
                {this.renderToolbar()}
                {this.renderTransactionsData()}
            </Fragment>

        )
    }
}

const
    mapStateToProps = state => {
        return {
            sortData: state.sortData
        }
    }

export default connect(mapStateToProps, {setAlertDialog})(InvoicesListScreen)
