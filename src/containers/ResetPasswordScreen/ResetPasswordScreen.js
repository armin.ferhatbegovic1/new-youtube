import React, { Component } from 'react'
import { newPasswordBrand } from '../../store/actions/authentication'
import Card from '@material-ui/core/Card'
import { connect } from 'react-redux'
import Logo from '../../components/UI/Logo/Logo'
import { formMode, layoutType, loaderVisbility, toastType } from '../../config/Constants'
import styles from './styles'
import { getDataFromURL, isEmpty, localize, prepareDataFormForSubmitAction } from '../../services'
import InputForm from '../../components/UI/InputForm/InputForm'
import { redirectToScreen } from '../../services/GlobalServices'
import { setLoaderVisibility } from '../../store/actions/loaderAction'
import { brandNewPasswordForm } from '../../forms/resetPassword'
import { setToastNotification } from '../../store/actions/toastAction'

class ResetPasswordScreen extends Component {

  constructor (props) {
    super(props)
    this.state = {
      brandNewPasswordForm: JSON.parse(JSON.stringify(brandNewPasswordForm)),
      formIsValid: false,
      loading: false,
      token: getDataFromURL('token')
    }

  }

  async componentWillMount () {
    const {history, setToastNotification} = this.props
    console.log(this.props)
    const {token} = this.state
    if (isEmpty(token)) {
      setToastNotification(true, localize('resetPasswordScreen.noToken'), toastType.FAILURE)
      redirectToScreen(history, '/')
    }

  }

  submitResetPasword = async () => {
    const {newPasswordBrand, setLoaderVisibility, history} = this.props
    const {brandNewPasswordForm, token} = this.state
    const passwordObj = prepareDataFormForSubmitAction(brandNewPasswordForm)
    setLoaderVisibility(loaderVisbility.SHOW)
    await newPasswordBrand(token, {password: passwordObj['newPassword']}, history)
  }

  render () {
    const {brandNewPasswordForm} = this.state
    return (
      <div style={styles.root}>
        <Card
          style={styles.loginCardForm}>
          <Logo />
          <label style={styles.introText}>{localize('loginScreen.welcomeBrand')}</label>
          <InputForm formElements={brandNewPasswordForm} prepareDataAndSubmit={this.submitResetPasword}
            mode={formMode.NEW} layout={layoutType.COLUMNS_1} />
        </Card>
      </div>
    )

  }
}

const
  mapStateToProps = state => ({
    auth: state.auth
  })

const
  mapStateToDispatch = dispatch => ({
    newPasswordBrand: (token, password, history) => dispatch(newPasswordBrand(token, password, history)),
    setLoaderVisibility: (visibility) => dispatch(setLoaderVisibility(visibility)),
    setToastNotification: (open, message, type) => dispatch(setToastNotification(open, message, type))
  })

export default connect(mapStateToProps, mapStateToDispatch)(ResetPasswordScreen)