import React, { Component } from 'react'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import ListItemText from '@material-ui/core/ListItemText'
import Switch from '@material-ui/core/Switch'
import PaletteIcon from '@material-ui/icons/Palette'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { swapThemeColors, toggleThemeMode } from '../../store/reducers/settings'
import { localize } from '../../services'
import ContainerToolbar from '../../components/UI/ContainerToolbar/ContainerToolbar'

class SettingScreen extends Component {

  componentWillMount () {
    const {auth, history} = this.props
    const {isAuthenticated} = auth
    if (!isAuthenticated) {
      history.push('/signin')
    }
  }

  render () {
    return (<div>
      <ContainerToolbar title={localize('settings')} />
      <Card>
        <CardContent>
          <List>
            <ListItem>
              <ListItemIcon>
                <PaletteIcon />
              </ListItemIcon>
              <ListItemText primary='Dark Mode' />
              <ListItemSecondaryAction>
                <Switch
                  onChange={(e, checked) => this.props.toggleThemeMode(checked)}
                  checked={this.props.settings.darkMode}
                />
              </ListItemSecondaryAction>
            </ListItem>

          </List>
        </CardContent>
      </Card>
    </div>)
  };

}

const mapStateToProps = state => {
  return {
    settings: state.settings,
    auth: state.auth
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      toggleThemeMode: checked => toggleThemeMode(checked),
      swapThemeColors: checked => swapThemeColors(checked)
    },
    dispatch
  )
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SettingScreen)
