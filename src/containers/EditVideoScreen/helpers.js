import { getRequest } from '../../services'
import { videos } from '../../config/Endpoints'
import { patchRequest ,postRequest} from '../../services/RequestManager/RequestManager'
import { request, requestHeader, toastNotification } from '../../config/Constants'
import AxiosInstance from '../../services/Axios'
import apiEndpoint from '../../config/config'
const configHeader = {
  headers: {
    'X-Auth-Token': localStorage.getItem('jwtToken')
  }
}

export const getVideoFormData =async(videoID) =>{
  return await getRequest(videos.VIDEO_BY_ID + '/' + videoID)
}
export const optimizationAction =async(videoID) =>{
 return await patchRequest('/video/' + videoID + '/optimize', {}, toastNotification.SHOW)
}
export const modifyVideoRecordAction =async(videoID,data) =>{
  return await patchRequest('/video/' + videoID, data, toastNotification.SHOW)
}
export const newVideoRecordAction =async(data) =>{
  return await postRequest('/video', data, toastNotification.SHOW)
}
export const uploadFileAction =async(file,belnogs_to='video.video') =>{
  const formData = new FormData()
  formData.append('file', file)
  formData.append('belongs_to', belnogs_to)
  formData.append('public', true)
  return await AxiosInstance.post(apiEndpoint.apiEndpoint+ videos.FILE_UPLOAD, formData, configHeader)
}
export const getVideoBinary =async(url) =>{
  const file= await  fetch(url, {
    method: request.GET,
    headers: requestHeader()
  }).then(r => r.blob())
  return file
 /* var reader = new FileReader();
  reader.readAsDataURL(response.data);
  reader.onloadend = function() {
    var base64data = reader.result;
    self.props.onMainImageDrop(base64data)
  }*/
}
