/** @flow */
import React, { Component, Fragment } from 'react'
import { awaitFunction, getRequest, isEmpty, localize, prepareDataFormForSubmitAction } from '../../services'
import styles from './styles'
import MainButton from '../../components/UI/MainButton/MainButton'
import ReactPlayer from 'react-player'
import Images from '../../utils/Images'
import { uploadVideoForm, videoCoverForm } from '../../forms/uploadVideo'
import { alertDialogType, formMode, layoutType, toastNotification, toastType } from '../../config/Constants'
import InputForm from '../UI/InputForm/InputForm'
import Grid from '@material-ui/core/Grid/Grid'
import ButtonWithIcon from '../UI/ButtonIcon/ButtonIcon'
import DeleteIcon from '@material-ui/icons/Delete'
import AxiosInstance from '../../services/Axios'
import Button from '@material-ui/core/Button/Button'
import { setToastNotification } from '../../store/actions/toastAction'
import connect from 'react-redux/es/connect/connect'
import Dimensions from '../../utils/Dimensions'
import UploadAudioFiles from './UploadAudioFiles'
import SubMainButton from '../UI/SubMainButton/SubMainButton'
import { setAlertDialog } from '../../store/actions/alertDialogAction'
import { videos } from '../../config/Endpoints'
import { patchRequest } from '../../services/RequestManager/RequestManager'
import apiEndpoint from '../../config/config'
import UploadAudioFiles2 from '../PreviewUploadVideo/UploadAudioFiles'

const config = {
  headers: {
    'X-Auth-Token': localStorage.getItem('jwtToken')
  }
}

class EditUploadVideo extends Component {
  constructor (props) {
    super(props)
    this.state = {
      title: '',
      file: null,
      video: null,
      video2: null,
      url: null,
      pip: false,
      playing: false,
      playing2: false,
      controls: false,
      light: false,
      volume: 0.8,
      muted: false,
      played: 0,
      loaded: 0,
      duration: 0,
      playbackRate: 1.0,
      loop: false,
      message: null,
      uploadVideoForm: JSON.parse(JSON.stringify(uploadVideoForm)),
      videoCoverForm: JSON.parse(JSON.stringify(videoCoverForm)),
      imageUploadResponse: null,
      videoUploadResponse: null,
      audioSetienceNumber: 0,
      audioSetienceData: [],
      videoID: new URL(window.location.href).searchParams.get('id'),
      videoData: null,
      coverData: null, video_file_uuid: null,
      savedVideo: true,
      newVideoUuid: null
    }
  }

  async componentWillMount () {

    const data = await this.getVideoFormData()
    if (!isEmpty(data)) {
      await awaitFunction(this.setState({
        videoData: data['payload'],
        video: videos.FILE_VIEW + data['payload'].video_file_uuid,
        coverData: videos.FILE_UPLOAD + '/' + data['payload']['cover'].uuid,
        audioFiles: data['payload'].sentences,
        imageUploadResponse: {data: {object_uuid: data['payload'].cover_file_uuid}},
        video_file_uuid: {data: {object_uuid: data['payload'].video_file_uuid}},
        payload: data['payload']
      }))
    } else {
      this.setState({noResultsMessageVisible: true})
    }
  }

  async getVideoFormData () {
    return await getRequest(videos.VIDEO_BY_ID + '/' + this.state.videoID)
  }

  onFormSubmit = async (e) => {
    const {file, imageUploadResponse, video_file_uuid} = this.state
    const {setAlertDialog} = this.props
    e.preventDefault() // Stop form submit
    if (isEmpty(imageUploadResponse)) {
      setAlertDialog(true, localize('videoImageCoverIsNotUploaded'), localize('videoUploadWarning'), alertDialogType.WARNING)
    } else if (file === null && video_file_uuid == null) {
      setAlertDialog(true, localize('videoIsNotUploaded'), localize('videoUploadWarning'), alertDialogType.WARNING)
    } else {
      if (video_file_uuid !== null) {
        await this.videoRecordUpload(video_file_uuid)
      } else {
        this.fileUpload(file).then(async (response) => {
          await this.videoRecordUpload(response)
        })
      }
    }
  }
  addAudioSetienceNumber = () => {
    const {audioSetienceNumber} = this.state
    let newValue = audioSetienceNumber + 1
    this.setState({audioSetienceNumber: newValue})
  }
  onChange = (e) => {
    awaitFunction(this.setState({file: e.target.files[0], video: URL.createObjectURL(e.target.files[0])}))
  }

  async saveOriginalVideo () {
    await this.videoRecordUpload()
  }

  fileUpload = async (file) => {
    const formData = new FormData()
    formData.append('file', file)
    formData.append('belongs_to', 'video.video')
    formData.append('public', true)
    return await AxiosInstance.post(videos.FILE_UPLOAD, formData, config)
  }

  imageUpload = async (file) => {
    const formData = new FormData()
    const {videoCoverForm} = this.state
    const {setToastNotification} = this.props
    const uploadVideo = prepareDataFormForSubmitAction(videoCoverForm)
    if (isEmpty(uploadVideo['videoCover'])) {
      setToastNotification(true, 'You didnt add any cover image')
      return
    } else {
      formData.append('belongs_to', 'video.cover')
      formData.append('public', true)
      formData.append('file', uploadVideo['videoCover'].value)
      await AxiosInstance.post(videos.FILE_UPLOAD, formData, config).then(reponse => {
        let reponseMessage = ''
        let type = toastType.SUCCESS
        if (reponse.data.success === true && reponse.status === 200) {
          reponseMessage = 'Cover Image is successful uploaded'
          this.setState({imageUploadResponse: reponse})
        }
        if (reponse.data.success !== true || reponse.status !== 200) {
          reponseMessage = 'Cover Image uploaded is failed' + reponse.data
          type = toastType.FAILURE
        }

        setToastNotification(true, reponseMessage, type)
      })
    }
  }

  videoRecordUpload = async (response) => {
    const {uploadVideoForm, imageUploadResponse, audioSetienceData, videoID, newFileUuid,audioFiles} = this.state
    const uploadVideo = prepareDataFormForSubmitAction(uploadVideoForm)
    console.log("response =============",response)
    let video_file_uuid = !isEmpty(response) ? response.data['object_uuid'] : newFileUuid
    let dataVideoRecord = {
      ...uploadVideo,
      'cover_file_uuid': imageUploadResponse['data'].object_uuid,
      'video_file_uuid': video_file_uuid
    }
    /*  if (audioSetienceData.length !== 0) {
        dataVideoRecord['sentences'] = audioSetienceData.concat(audioFiles)
      }*/
    await patchRequest('/video/' + videoID, dataVideoRecord, toastNotification.SHOW)
    this.setState({video_file_uuid})
  }

  submitUploadVideoForm = () => {
    this.clickUploadBtn()
  }

  deleteVideo = () => {
    this.setState({video: null, file: null, video_file_uuid: null})
  }

  clickUploadBtn () {
    document.querySelector('#uploadBtn button').click()
  }

  playPause = () => {
    this.setState({playing: !this.state.playing})
  }
  stop = () => {
    this.setState({url: null, playing: false})
  }
  stop2 = () => {
    this.setState({playing2: false})
  }
  playPause2 = () => {
    this.setState({playing2: !this.state.playing2})
  }
  optimization = async () => {
    const {video_file_uuid, payload} = this.state
    let videoFileUuid = payload['video_uuid']
    if (!isEmpty(video_file_uuid)) {
      const data = await patchRequest('/video/' + videoFileUuid + '/optimize', {}, toastNotification.SHOW)
      if (data['success']) {
        let newFileUuid = data['payload'].video_file_uuid
        this.setState({video2: apiEndpoint.apiEndpoint + '/file/' + newFileUuid, newFileUuid})
      }
    } else {
      this.props.setAlertDialog(true, localize('youHaveToSaveVideoFirst'), localize('videoUploadWarning'), alertDialogType.WARNING)
    }
  }
  addAudioDataInArray = async (data, index) => {
    await awaitFunction(this.setState({
      audioSetienceData: this.state.audioSetienceData.concat(data)
    }))
    console.log("================== ",this.state.audioSetienceData)
  }

  renderAudioFields = () => {
    const {audioFiles, videoID} = this.state
    if (!isEmpty(audioFiles) && audioFiles.length !== 0) {
      return Array.apply(0, Array(audioFiles.length)).map((x, i) => {
        return <UploadAudioFiles videoID={videoID} key={i} index={i} addAudioData={this.addAudioDataInArray}
          data={audioFiles[i]} />
      })
    }
  }
  renderAudioFieldsNEw = () => {
    const {audioSetienceNumber} = this.state
    if (audioSetienceNumber !== 0) {
      return Array.apply(0, Array(audioSetienceNumber)).map((x, i) => {
        return <UploadAudioFiles2 key={i} index={i} addAudioData={this.addAudioDataInArray} />
      })
    }
  }

  renderVideoControls () {
    const {video, playing} = this.state
    return video !== null ?
      <div style={styles.playerControls}>
        <Button variant='contained' size='small' color='primary' style={styles.btnContainer}
          disabled={!video}
          onClick={this.optimization}>
          {localize('optimization')}
        </Button>
        <Button variant='contained' size='small' color='primary' style={styles.btnContainer}
          disabled={!video}
          onClick={this.stop}>
          {localize('stop')}
        </Button>
        <Button variant='contained' size='small' color='primary' style={styles.btnContainer}
          disabled={!video}
          onClick={this.playPause}>
          {playing ? 'Pause' : 'Play'}
        </Button>
        <ButtonWithIcon handleAction={this.deleteVideo} buttonType='primary'>
          <DeleteIcon />
        </ButtonWithIcon>
      </div> : null
  }

  renderVideoControls2 () {
    const {video2, playing2} = this.state
    return video2 !== null ?
      <div style={styles.playerControls}>
        <Button variant='contained' size='small' color='primary' style={styles.btnContainer}
          disabled={!video2}
          onClick={this.stop2}>
          {localize('stop')}
        </Button>
        <Button variant='contained' size='small' color='primary' style={styles.btnContainer}
          disabled={!video2}
          onClick={this.playPause2}>
          {playing2 ? 'Pause' : 'Play'}
        </Button>
      </div> : null
  }

  renderVideoUploadContainer () {
    const {video, playing} = this.state
    return <div style={styles.videoContainerUpload}>
      <div style={{flexDirection: 'column', display: 'flex', marginLeft: 40}}>
        <div style={styles.videoContainer} id='video-container'> {video !== null ?
          <div><ReactPlayer url={video}
            width={'100%'} height={'auto'}
            playing={playing} />
          </div>
          : <img
            src={Images.videoPlaceholder}
            alt='placeholder'
            onClick={() => {
              this.upload.click()
            }}
            style={styles.videoPlaceholder}
          />}</div>
        {this.renderVideoControls()}
        {this.renderVideoUploadContainerOptimize()}
      </div>

      <div style={{flexDirection: 'column', display: 'flex', justifyContent: 'center'}}>
        {this.renderImageCoverUpload()}
        {this.renderVideoDetails()}

      </div>
    </div>
  }

  renderVideoUploadContainerOptimize () {
    const {video2, playing2} = this.state
    return !isEmpty(video2) ? <div style={styles.videoContainerUpload}>
      <div style={{flexDirection: 'column', display: 'flex', marginLeft: 40}}>
        <div style={styles.videoContainer} id='video-container'> {video2 !== null ?
          <div><ReactPlayer url={video2}
            width={'100%'} height={'auto'}
            playing={playing2} />
          </div>
          : <img
            src={Images.videoPlaceholder}
            alt='placeholder'
            onClick={() => {
              this.upload.click()
            }}
            style={styles.videoPlaceholder}
          />}</div>
        {this.renderVideoControls2()}
      </div>
    </div> : null
  }

  renderVideoDetails () {
    const {uploadVideoForm, message, videoData, savedVideo, newFileUuid} = this.state
    return <div>
      <form onSubmit={this.onFormSubmit}>
        <input type='file' onChange={this.onChange}
          style={{display: 'none'}}
          ref={(ref) => this.upload = ref}
          id='video-file-2' />
        <div id='uploadBtn' style={{display: 'none'}}>
          <MainButton label={localize('uploadVideo')} buttonType='primary'
            type='submit' />
        </div>
      </form>
      {!isEmpty(videoData) ?
        <Fragment>
          <div style={styles.saveVideoButtonContainer}>
            {!isEmpty(newFileUuid) ? <div><SubMainButton label='Save Original Video' buttonType='secondary'
              type='button' onClick={() => {
              this.saveOriginalVideo()
            }} />
              <SubMainButton label='Save Optimized Video' buttonType='secondary'
                type='button' onClick={() => {
                this.saveOptimizationVideo()
              }} /></div> : null}
          </div>
          <InputForm data={videoData} formElements={uploadVideoForm}
            prepareDataAndSubmit={this.submitUploadVideoForm}
            mode={formMode.EDIT} layout={layoutType.COLUMNS_3} buttonLabel='Modify Video'>
            {!isEmpty(message) ? <label
              style={{fontSize: Dimensions.textMedium, alignSelf: 'center'}}>{message}</label> : null}
          </InputForm></Fragment> : null}
    </div>

  }

  saveOptimizationVideo = () => {
    const {video2} = this.state
    const {setToastNotification} = this.props
    if (isEmpty(video2)) {
      setToastNotification(true, 'You didnt click button optimization')
    } else {
      setToastNotification(true, 'You save optimized video successfully', toastType.SUCCESS)
    }
  }

  loadCoverDataInForm () {
    const {videoCoverForm, coverData} = this.state
    videoCoverForm['videoCover'].value = coverData
    setTimeout(() => this.setState({coverData: 1}), 1000)
  }

  renderImageCoverUpload () {
    const {videoCoverForm, coverData} = this.state
    if (!isEmpty(coverData) && coverData !== 1) {
      this.loadCoverDataInForm()
    }

    return !isEmpty(coverData) ? <div style={{justifyContent: 'flex-start', display: 'flex'}}>
      <InputForm formElements={videoCoverForm}
        prepareDataAndSubmit={this.imageUpload} data={coverData}
        mode={formMode.EDIT} layout={layoutType.COLUMNS_1} lastElementInLayout={layoutType.COLUMNS_2}
        buttonLabel='Modify Cover' />
      <div style={{height: 50, alignSelf: 'center'}}>
        <SubMainButton label='Add New Audio' buttonType='primary'
          type='button' onClick={() => this.addAudioSetienceNumber()} />
      </div>
    </div> : null
  }

  renderVideoUploader () {
    return (
      <Fragment>
        <Grid container spacing={24}>
          <Grid item xs={12}>
            {this.renderVideoUploadContainer()}
          </Grid>
          <Grid item xs={12}>
            <div style={{display: 'flex', flexWrap: 'wrap'}}> {this.renderAudioFields()}
              {this.renderAudioFieldsNEw()}</div>
          </Grid>
        </Grid>
      </Fragment>
    )
  }

  render () {
    return (
      this.renderVideoUploader()
    )
  }
}

export default connect(null, {setToastNotification, setAlertDialog})((EditUploadVideo))
