/** @flow */
import React, { Component } from 'react'
import { localize } from '../../services'
import ContainerToolbar from '../../components/UI/ContainerToolbar/ContainerToolbar'
import Paper from '@material-ui/core/Paper/Paper'
import styles from './styles'
import EditUploadVideo from '../../components/EditVideo/EditUploadVideo'

class EditVideoScreen extends Component {

  renderVideoUploader () {
    return (
      <div id='new-deal-screen'>
        <ContainerToolbar title={localize('modifyVideo')} />
        <Paper style={styles.paper}>
          <EditUploadVideo />
        </Paper>
      </div>
    )
  }

  render () {
    return (
      this.renderVideoUploader()
    )
  }
}

export default EditVideoScreen
