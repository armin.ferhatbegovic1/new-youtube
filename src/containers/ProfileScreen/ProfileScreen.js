import React, { Component } from 'react'
import {
  awaitFunction,
  getRequest,
  isEmpty,
  localize,
  prepareDataFormForSubmitAction,
  putRequest
} from '../../services'
import styles from './styles'
import { getFormProperties, getUserData, prepareCompanyData } from './helpers'
import { formMode, layoutType, positionButton, role, toastNotification } from '../../config/Constants'
import { connect } from 'react-redux'
import InputForm from '../../components/UI/InputForm/InputForm'
import Paper from '@material-ui/core/Paper/Paper'
import ContainerToolbar from '../../components/UI/ContainerToolbar/ContainerToolbar'
import { userEndpoints } from '../../config/Endpoints'
import { setCurrentUserProfileCategories, setCurrentUserProfileImage } from '../../store/actions/userActions'

class ProfileScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
      profileForm: {
        ...getFormProperties(props['auth'].user)
      },
      formIsValid: false,
      mode: formMode.VIEW
    }
    this._isMounted = false
  }

  changeFormMode = () => {
    this.setState({mode: formMode.EDIT})
  }
  componentWillUnmount() {
    this._isMounted = false;
  }
  async componentDidMount () {
    this._isMounted = true
    if (this._isMounted) {
      const profile = await getRequest(userEndpoints.PROFILE_VIEW)
      const userData = getUserData(profile)
      awaitFunction(this.setState({userData, userRole: profile['role'], userId: profile['_id']}))
    }
  }

  saveUserAccount = async (userData) => {
    const {setCurrentUserProfileImage} = this.props
    const userID = userData['_id']
    await putRequest(userEndpoints.PROFILE_EDIT + '/' + userID, userData, toastNotification.SHOW,localize('successfullySavedChanges'))
    setCurrentUserProfileImage(userData['profileImage'])
  }

  prepareDataForSave = async () => {
    const {userId, profileForm, userRole} = this.state
    const{setCurrentUserProfileCategories}=this.props
    this.setState({mode: formMode.VIEW})
    if (userRole === role.INFLUENCER) {
      const dataForSubmit = prepareDataFormForSubmitAction(profileForm)
      dataForSubmit['_id'] = userId
      setCurrentUserProfileCategories(profileForm['categoriesOfInterest'].value)
      await this.saveUserAccount(dataForSubmit)
    }
    if (userRole === role.BRAND) {
      const dataForSubmit = prepareDataFormForSubmitAction(profileForm)
      const brandData = prepareCompanyData(dataForSubmit, userId)
      await this.saveUserAccount(brandData)
    }
  }

  render () {
    const {profileForm, userData, mode} = this.state
    return (
      <div>
        <ContainerToolbar title={localize('profileScreen.myAccount')} />
        <Paper style={styles.paper}>
          {!isEmpty(userData) ?
            <InputForm formElements={profileForm} prepareDataAndSubmit={this.prepareDataForSave}
              mode={mode} data={userData} layout={layoutType.COLUMNS_2} lastElementInLayout={layoutType.COLUMNS_2}
              editFormButton={this.changeFormMode} saveButtonPosition={positionButton.RIGHT} editable={true} /> : null}
        </Paper>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    auth: state.auth
  }
}

export default connect(mapStateToProps, {
  setCurrentUserProfileImage,setCurrentUserProfileCategories
})((ProfileScreen))