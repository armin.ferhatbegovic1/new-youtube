import { isEmpty } from '../../services'
import { role } from '../../config/Constants'
import { brandProfileForm, instagramProfileForm } from '../../forms'

export function getUserData (user) {
  if (user.role === role.BRAND) {
    return getBrandData(user)
  }
  if (user.role === role.INFLUENCER) {
    return getInfluencerData(user)
  }
}

function getInfluencerData (user) {
  return {
    ...getBasicUserData(user),
    instagramUsername: user['instagramUsername'],
    categoriesOfInterest: user['categoriesOfInterest'],
    followers: user['followers'],
    worldwide: user['worldwide'],
    countryOfInterest: !isEmpty(user['countryOfInterest']) ? user['countryOfInterest'] : null,
    cityOfInterest: !isEmpty(user['cityOfInterest']) ? user['cityOfInterest'] : null
  }
}

function getBrandData (user) {
  return {
    ...getBasicUserData(user),
    name: !isEmpty(user['company']) ? user['company'].name : '',
    vat: !isEmpty(user['company']) ? user['company'].vat : '',
    country: !isEmpty(user['company']) ? user['company'].country : '',
    city: !isEmpty(user['company']) ? user['company'].city : '',
    address: !isEmpty(user['company']) ? user['company'].address : '',
    positionInCompany: !isEmpty(user['positionInCompany']) ? user['positionInCompany'] : null
  }
}

function getBasicUserData(user) {
  return {
    fullName: user['fullName'],
    email: user['email'],
    profileImage: !isEmpty(user['profileImage']) ? user['profileImage'] : null,
    aboutMe: user['aboutMe']
  }
}

export function getFormProperties (user) {
  if (user['role'] === role.BRAND) {
    return brandProfileForm(user)
  }
  if (user['role'] === role.INFLUENCER) {
    return instagramProfileForm
  }
}

export function prepareCompanyData (dataForm, userID) {
  const company = {
    'name': dataForm['name'],
    'vat': dataForm['vat'],
    'address': dataForm['address'],
    'city': dataForm['city'],
    'country': dataForm['country']
  }
  return {
    aboutMe: dataForm['aboutMe'],
    _id: userID,
    fullName: dataForm['fullName'],
    profileImage: dataForm['profileImage'],
    email: dataForm['email'],
    positionInCompany: dataForm['positionInCompany'],
    company: company
  }
}