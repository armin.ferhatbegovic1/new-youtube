/** @flow */
import React, {Component} from 'react'
import {localize} from '../../services'
import ContainerToolbar from '../../components/UI/ContainerToolbar/ContainerToolbar'
import styles from './styles'
import Paper from '@material-ui/core/Paper/Paper'
import PreviewVideo from '../../components/PreviewVideo/PreviewVideo'

class PreviewVideoScreen extends Component {

    renderToolbar() {
        return <ContainerToolbar title={localize('previewVideoScreen.previewVideo')}>
        </ContainerToolbar>
    }

    render() {
        return <div>
            {this.renderToolbar()}
            <Paper style={styles.paper}>
                <PreviewVideo/>
            </Paper>
        </div>
    }
}

export default PreviewVideoScreen