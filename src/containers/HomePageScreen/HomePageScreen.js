import React, { Component } from 'react'
import { connect } from 'react-redux'
import Images from '../../utils/Images'
import styles from './styles'
import { localize } from '../../services'
import SubMainButton from '../../components/UI/SubMainButton/SubMainButton'
import { role } from '../../config/Constants'

class HomePageScreen extends Component {

  goToPreStepScreen (userType) {
    const {history} = this.props
    history.push({
      pathname: '/login',
      state: {
        userType
      }
    })

  }

  render () {
    return (
      <div style={styles.mainContainer}>

        <div style={styles.leftContainer}>
          <div style={styles.intro}>
            {localize('homePageScreen.intro')}
          </div>
          <div style={styles.introButtons}>
            <SubMainButton label={localize('homePageScreen.iAmBrand')} buttonType='primary' type='button'
              onClick={() => this.goToPreStepScreen(role.BRAND)} />
            <SubMainButton label={localize('homePageScreen.iAmInfluencer')} buttonType='secondary'
              type='submit' onClick={() => this.goToPreStepScreen(role.INFLUENCER)} />
          </div>
          <div style={styles.introButtons}>
            <SubMainButton label={localize('homePageScreen.topDeals')} buttonType='tertiary'
              type='button' onClick={() => this.goToPreStepScreen(role.BRAND)} />
          </div>
        </div>


        <div style={styles.container}>
          <img
            src={Images.logo}
            style={styles.logo}
            alt={localize('logo')}
          />
        </div>


      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    auth: state.auth
  }
}
export default connect(mapStateToProps)((HomePageScreen))

