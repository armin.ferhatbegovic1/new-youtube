import RootContainerStyles from '../Styles/RootContainerStyles'
import Colors from '../../utils/Colors'
import Dimensions from '../../utils/Dimensions'

const styles = {
    ...RootContainerStyles,
    logo: {
        width: '100%',
        height: 'auto'
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center'
    },
    rightContainer: {
        display: 'flex',
        flexFlow: 'column nowrap',
        height: 'calc(100vh - 100px)'
    },
    leftContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        display: 'flex',
        flexDirection: 'column'
    },
    mainContainer: {
        display: 'flex',
        flexFlow: 'row wrap',
        justifyContent: 'center',
        width: '100%'
    },
    intro: {
        textAlign: 'center',
        color: Colors.secondary,
        fontSize: Dimensions.textLarge,
      maxWidth:800
    },
    introButtons: {
        display: 'flex',
        flexFlow: 'row wrap',
        justifyContent: 'space-evenly',
        width: '100%',
      margin:35
    }
}

export default styles