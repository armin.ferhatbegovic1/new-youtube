import RootContainerStyles from '../Styles/RootContainerStyles'
import Dimensions from '../../utils/Dimensions'

const styles = {
  ...RootContainerStyles,
  grid: {
    padding: '20px',
    display: 'grid',
    gridTemplateRows: '85px 1fr 1fr 1fr',
    height: 'inherit'
  },
  button: {
    marginTop: '25px',
    marginBottom: '20px'
  },
  formContainer: {
    flexDirection: 'column',
    paddingLeft: Dimensions.tripeBaseMargin,
    paddingRight: Dimensions.tripeBaseMargin
  },
  fieldContainer: {
    display: 'flex',
    marginBottom: 15
  },
  submitButton: {
    display: 'flex',
    justifyContent: 'center'
  },
  checkBoxContainer: {
    marginLeft: Dimensions.baseMargin,
    marginBottom: Dimensions.baseMargin
  },
  forgotPassword: {
    display: 'flex',
    justifyContent: 'center',
    marginTop: Dimensions.baseMargin * 1.5
  },
  containerFull: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    marginBottom: Dimensions.baseMargin
  },
  loginRegisterButton: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between'
  },
  loginButtonMarginTop: {
    marginTop: Dimensions.doubleBaseMargin * 5
  },
  loginBrandButtonMarginTop:{
    marginTop: Dimensions.doubleBaseMargin * 3
  },
  instagramButton: {
    display: 'none'
  },
  subIntroText:{
    textAlign:'center',
  }
}

export default styles