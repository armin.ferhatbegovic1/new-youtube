import React, {Component} from 'react'
import {forgotPasswordBrand, loginBrand, loginInfluencer, registerBrandUser} from '../../store/actions/authentication'
import Card from '@material-ui/core/Card'
import {connect} from 'react-redux'
import Logo from '../../components/UI/Logo/Logo'
import {formMode, layoutType, loaderVisbility, preStepScreenMode, role} from '../../config/Constants'
import styles from './styles'
import {getDataFromURL, isEmpty, localize, prepareDataFormForSubmitAction} from '../../services'
import SubMainButton from '../../components/UI/SubMainButton/SubMainButton'
import {brandLoginForm, brandRegistrationForm} from '../../forms'
import InputForm from '../../components/UI/InputForm/InputForm'
import {goToHomeScreen} from '../../services/GlobalServices'
import {setLoaderVisibility} from '../../store/actions/loaderAction'
import {brandResetPasswordForm} from '../../forms/resetPassword'
import Button from '@material-ui/core/Button/Button'

class LoginScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
            userType: this.setUsrTypeByURLParam(),
            brandLoginForm: JSON.parse(JSON.stringify(brandLoginForm)),
            brandRegistrationForm: JSON.parse(JSON.stringify(brandRegistrationForm)),
            brandForgotPasswordForm: JSON.parse(JSON.stringify(brandResetPasswordForm)),
            mode: this.setModeByURLParam(),
            formIsValid: false,
            loading: false
        }
    }


    async componentWillMount() {
        const {loginInfluencer, history} = this.props
        const token = getDataFromURL('token')
        if (!isEmpty(token)) {
            setLoaderVisibility(loaderVisbility.SHOW)
            loginInfluencer(token, history)
        }

    }

    submitBrandLoginForm = async () => {
        const {loginBrand, setLoaderVisibility, history} = this.props
        const {brandLoginForm} = this.state
        const brandUser = prepareDataFormForSubmitAction(brandLoginForm)
        setLoaderVisibility(loaderVisbility.SHOW)
        await loginBrand(brandUser, history)
    }

    submitBrandForgotPassword = async () => {
        const {forgotPasswordBrand, history} = this.props
        const {brandForgotPasswordForm} = this.state
        const brandUser = prepareDataFormForSubmitAction(brandForgotPasswordForm)
        await forgotPasswordBrand(brandUser, history)
    }

    submitBrandRegistrationForm = () => {
        const {brandRegistrationForm} = this.state
        const {history, registerBrandUser} = this.props
        const newBrandUser = prepareDataFormForSubmitAction(brandRegistrationForm)
        registerBrandUser(newBrandUser, history)
    }

    componentDidMount() {
        const {auth, history} = this.props
        if (isEmpty(getDataFromURL('token')) && auth.isAuthenticated && auth['user'].role === role.INFLUENCER)
            goToHomeScreen(history)
    }

    setUsrTypeByURLParam() {
        const {history} = this.props
        const params = history.location.state

        if (!isEmpty(params)) {

            if (!isEmpty(params['role']))
                return params['role']

            if (!isEmpty(params['userType']))
                return params['userType']
        }
        return null
    }

    setModeByURLParam() {
        return preStepScreenMode.BRAND_LOGIN
    }

    getUserIntro() {
        const {userType} = this.state
        switch (userType) {
            case role.BRAND:
                return localize('loginScreen.welcomeBrand')
            case role.INFLUENCER:
                return localize('loginScreen.welcomeInfluencer')
            default:
                return null
        }
    }

    getUserForm() {
        const {mode, brandLoginForm, brandRegistrationForm, brandForgotPasswordForm} = this.state
        switch (mode) {
            case preStepScreenMode.BRAND_LOGIN:
                return <InputForm formElements={brandLoginForm} prepareDataAndSubmit={this.submitBrandLoginForm}
                                  mode={formMode.NEW} layout={layoutType.COLUMNS_1}/>
            case preStepScreenMode.BRAND_REGISTRATION:
                return <InputForm formElements={brandRegistrationForm}
                                  prepareDataAndSubmit={this.submitBrandRegistrationForm}
                                  layout={layoutType.COLUMNS_2} mode={formMode.NEW}/>
            case preStepScreenMode.INFLUENCER_LOGIN:
                return this.getInfluencerLoginButton()
            case preStepScreenMode.BRAND_FORGOT_PASSWORD:
                return <InputForm formElements={brandForgotPasswordForm}
                                  prepareDataAndSubmit={this.submitBrandForgotPassword}
                                  buttonLabel={localize('continue')}
                                  mode={formMode.NEW} layout={layoutType.COLUMNS_1}/>
            default:
                return null
        }

    }

    loginUser() {
        const {userType} = this.state
        switch (userType) {
            case role.BRAND:
                this.setState({mode: preStepScreenMode.BRAND_LOGIN})
                break
            case role.INFLUENCER:
                this.setState({mode: preStepScreenMode.INFLUENCER_LOGIN})
                this.loginBrandByInstagramHandler()
                break
            default:
                return null
        }
    }

    registerUser() {
        const {userType} = this.state
        switch (userType) {
            case role.BRAND:
                this.setState({mode: preStepScreenMode.BRAND_REGISTRATION})
                break
            case role.INFLUENCER:
                this.setState({mode: preStepScreenMode.INFLUENCER_REGISTRATION})
                break
            default:
                return null
        }
    }

    loginBrandByInstagramHandler() {
        document.querySelector('#instBtn button').click()
        this.props.setLoaderVisibility(loaderVisbility.SHOW)
    }

    getBrandLoginRegistrationButton() {
        return <div style={styles.loginRegisterButton}>
            <div style={Object.assign({}, styles.containerFull, styles.loginBrandButtonMarginTop)}>
                <SubMainButton
                    label={localize('loginScreen.login')}
                    buttonType='primary' type='button'
                    onClick={() => this.loginUser()}/></div>
            <label style={styles.subIntroText}>{localize('loginScreen.or')}</label>
            <div style={styles.containerFull}>
                <SubMainButton label={localize('loginScreen.register')} buttonType='secondary'
                               type='submit' onClick={() => this.registerUser()}/>
                <Button component='span' onClick={() => this.setState({mode: preStepScreenMode.BRAND_FORGOT_PASSWORD})}>
                    {localize('loginScreen.forgotPassword')}
                </Button>
            </div>
        </div>
    }

    getInfluencerLoginButton() {
        return <div style={styles.loginRegisterButton}>
            <div style={Object.assign({}, styles.containerFull, styles.loginButtonMarginTop)}>
                <SubMainButton
                    label={localize('loginScreen.signInWithInstagram')}
                    buttonType='primary' type='button'
                    onClick={() => this.loginUser()}/></div>
        </div>
    }

    getUserButtons() {
        const {userType} = this.state
        if (userType === role.INFLUENCER) {
            return this.getInfluencerLoginButton()
        }
        if (userType === role.BRAND) {
            return this.getBrandLoginRegistrationButton()
        }
    }

    getFormLayout() {
        const {mode} = this.state
        if (mode !== preStepScreenMode.BRAND_REGISTRATION)
            return styles.loginCardForm

        if (mode === preStepScreenMode.BRAND_REGISTRATION)
            return styles.registerCardForm

    }

    render() {
        const {mode} = this.state
        return (
            <div style={styles.root}>
                <Card
                    style={this.getFormLayout()}>
                    <Logo/>
                    <div id='instBtn' style={styles.instagramButton}>
                    </div>
                    <label style={styles.introText}>{this.getUserIntro()}</label>
                    {mode === 'default' ? this.getUserButtons() :
                        this.getUserForm()
                    }
                </Card>
            </div>
        )

    }
}

const
    mapStateToProps = state => ({
        auth: state.auth,
        errors: state.errors
    })

const
    mapStateToDispatch = dispatch => ({
        loginInfluencer: (token, history) => dispatch(loginInfluencer(token, history)),
        loginBrand: (user, history) => dispatch(loginBrand(user, history)),
        forgotPasswordBrand: (user, history) => dispatch(forgotPasswordBrand(user, history)),
        registerBrandUser: (user, history) => dispatch(registerBrandUser(user, history)),
        setLoaderVisibility: (visibility) => dispatch(setLoaderVisibility(visibility))
    })

export default connect(mapStateToProps, mapStateToDispatch)(LoginScreen)
