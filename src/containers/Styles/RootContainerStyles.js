import Dimensions from '../../utils/Dimensions'
import Colors from '../../utils/Colors'
import { deviceType } from '../../config/Constants'

const RootContainerStyles = {
  root: {
    height: 'inherit',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  card: {
    width: '350px',
    height: '300px'
  },
  containerFull: {
    width: '100% !important',
    height: '100% !important',
    overflowY: 'auto !important',
    overflowX: 'hidden !important'
  },
  paddingFullStandard: {
    padding: '16px !important'
  },
  marginTop50: {
    marginTop: '50px'
  },
  marginTop25: {
    marginTop: '25px'
  },
  width75: {
    width: '70%'
  },
  subContainer: {
    paddingTop: Dimensions.getDevice() === deviceType.PHONE ? Dimensions.doubleBaseMargin : Dimensions.tripeBaseMargin,
    paddingBottom: Dimensions.getDevice() === deviceType.PHONE ? Dimensions.doubleBaseMargin : Dimensions.tripeBaseMargin,
    paddingLeft: Dimensions.getDevice() === deviceType.PHONE ? Dimensions.baseMargin : 2 * Dimensions.tripeBaseMargin,
    paddingRight: Dimensions.getDevice() === deviceType.PHONE ? Dimensions.baseMargin : 2 * Dimensions.tripeBaseMargin
  },
  flexEnd: {
    justifyContent: 'flex-end'
  },
  centered: {
    alignItems: 'center'
  },
  flex: {
    display: 'flex'
  },
  flexFiller: {
    flex: 1
  },
  fullWidth: {
    width: '100%'
  },
  fullHeight: {
    height: '100%'
  },
  horizontalCenter: {
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'row'
  },
  horizontalStart: {
    display: 'flex',
    justifyContent: 'flex-start',
    flexDirection: 'row'
  },
  horizontalEnd: {
    display: 'flex',
    justifyContent: 'flex-end',
    flexDirection: 'row'
  },
  verticalCenter: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column'
  },
  fullCenter: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  flexDirectionRow: {
    display: 'flex',
    flexDirection: 'row'
  },
  flexDirectionColumn: {
    display: 'flex',
    flexDirection: 'column'
  },
  textCenter: {
    textAlign: 'center'
  },
  textLeft: {
    textAlign: 'left'
  }, checkBoxSecondary: {
    color: Colors.secondary
  },
  loginCardForm: {
    width: '380px',
    height: '500px'
  },
  registerCardForm: {
    width: '700px',
    height: '600px'
  },
  screenTitle: {
    fontSize: Dimensions.getDevice() === deviceType.PHONE ? Dimensions.textLarge : ''
  },
  introText: {
    marginTop: Dimensions.baseMargin,
    marginBottom: Dimensions.baseMargin,
    color: Colors.color5,
    textAlign: 'center',
    display: 'flex',
    justifyContent: 'center',
    fontSize: Dimensions.textMedium
  },
  paper: {
    margin: 24
  },
  responseTitle: {
    fontWeight: 'bold',
    fontSize: Dimensions.textX4Large,
    marginTop: Dimensions.doubleBaseMargin,
    marginBottom: Dimensions.doubleBaseMargin,
    color: Colors.secondary
  },
  responseSubTitle: {
    fontWeight: 'bold',
    fontSize: Dimensions.textLarge,
    color: Colors.color5
  },
  responseGetStartedButton: {
    display: 'flex',
    flexFlow: 'row wrap',
    justifyContent: 'space-evenly',
    width: '100%',
    margin: Dimensions.doubleBaseMargin * 2
  },
  responseTextLabel: {
    margin: Dimensions.baseMargin,
    fontWeight: 'bold',
    color: Colors.color5
  },
  filterContainer: {
    flex: 1
  },
  mobileFilterContainer:{
    boxShadow: '0px 0px 0px 0px rgba(0,0,0,0),0px 2px 4px -1px rgba(0,0,0,0.2), 0px 4px 5px 0px rgba(0,0,0,0.14), 0px 1px 10px 0px rgba(0,0,0,0.12)',
    display:'flex',
    flexDirection:'column',
    marginTop:230,
    backgroundColor:Colors.yellow
  },
  filterChip: {
    boxShadow: '0px 2px 4px -1px rgba(0,0,0,0.2), 0px 4px 5px 0px rgba(0,0,0,0.14), 0px 1px 10px 0px rgba(0,0,0,0.12)',
    margin: Dimensions.baseMargin,
    color:Colors.white
  }
}

export default RootContainerStyles