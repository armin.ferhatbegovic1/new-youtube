import React, { Component } from 'react'
import FinalResponseMessage from '../../components/FinalResponseMessage/FinalResponseMessage'
import styles from './styles'
import { localize } from '../../services'
import SubMainButton from '../../components/UI/SubMainButton/SubMainButton'
import { redirectToScreen } from '../../services/GlobalServices'
import { role } from '../../config/Constants'

class AfterRegistrationScreen extends Component {
  goToLoginScreen = () => {
    const {history, location} = this.props
    redirectToScreen(history, '/login', location['state'].role)
  }

  getBrandMessage () {
    const {location} = this.props
    const createdUserRole = location['state'].role
    return createdUserRole === role.BRAND ? <label
      style={styles.responseTextLabel}>{localize('afterRegistrationScreen.checkEmailToActivateAccount')}</label> : null
  }

  render () {
    return (
      <div id='new-deal-screen'>
        <FinalResponseMessage>
          <label style={styles.responseTitle}>{localize('afterRegistrationScreen.congratulations')}</label>
          <label
            style={styles.responseSubTitle}>{localize('afterRegistrationScreen.yourAccountHasBeenSuccessfullyCreated')}</label>
          {this.getBrandMessage()}
          <div style={styles.responseGetStartedButton}>
            <SubMainButton label={localize('afterRegistrationScreen.getStarted')} buttonType='tertiary'
              type='button' onClick={() => this.goToLoginScreen()} />
          </div>
        </FinalResponseMessage>
      </div>
    )
  }
}

export default AfterRegistrationScreen