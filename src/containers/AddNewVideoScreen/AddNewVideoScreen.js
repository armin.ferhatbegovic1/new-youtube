/** @flow */
import React, { Component } from 'react'
import { localize } from '../../services'
import ContainerToolbar from '../../components/UI/ContainerToolbar/ContainerToolbar'
import Paper from '@material-ui/core/Paper/Paper'
import styles from './styles'
import NewVideo from '../../components/NewVideo/NewUploadVideo'

class AddNewVideoScreen extends Component {

  renderVideoUploader () {
    return (
      <div id='new-deal-screen'>
        <ContainerToolbar title={localize('addNewVideo')} />
        <Paper style={styles.paper}>
          <NewVideo />
        </Paper>
      </div>
    )
  }

  render () {
    return (
      this.renderVideoUploader()
    )
  }
}

export default AddNewVideoScreen
