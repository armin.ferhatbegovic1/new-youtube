import React, {Component} from 'react'
import {AxiosInstance} from "../../services";

const styles = {
    container: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        height: 200
    },
    href: {
        color: '-webkit-link',
        cursor: 'pointer',
        textDecoration: 'underline'
    }
}

class Redirection extends Component {
    componentDidMount() {
        const head = document.getElementsByTagName("head")[0];
        head.innerHTML = '<head><meta charSet="UTF-8"><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta http-equiv="X-UA-Compatible" content="ie=edge"><title>爱说才会赢</title></head>'
        this.onClick()
    }

    onClick() {
        console.log("onClick")
        console.log("redirectToApp")
        var scheme = "voiceoverapp";
        var openURL = "video" + window.location.search + window.location.hash;
        // var openURL = "video" + "/?id=77db563e-551d-4d1e-8a92-5f16de34d697" + window.location.hash;
        var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent);
        var Android = /Android/.test(navigator.userAgent);
        var newLocation;
        if (iOS) {
            newLocation = scheme + ":" + openURL;
        }
        /*else if (Android) {
            newLocation = "intent://" + openURL + "#Intent;scheme=" + scheme + ";package=com.digitalgalaxy.voiceoverapp;end";
        }*/
        else {
            newLocation = scheme + "://" + openURL;
        }

        console.log(newLocation)
        window.location.replace(newLocation);
    }

    render() {
        return (
            <div>
                <div style={styles.container}>
                    <p>请稍候，我们将您重定向到爱说才会赢应用...</p>
                    <div onClick={this.onClick} style={styles.href}>爱说才会赢</div>
                </div>
            </div>
        )
    }

}

export default Redirection