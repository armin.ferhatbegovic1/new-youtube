import React, { Component } from 'react'
import Card from '@material-ui/core/Card'
import { connect } from 'react-redux'
import styles from './styles'
import { loginInfluencer } from '../../store/actions/authentication'
import Logo from '../../components/UI/Logo/Logo'
import { getDataFromURL, localize, postRequest, prepareDataFormForSubmitAction } from '../../services'
import { formMode, layoutType, role, toastNotification } from '../../config/Constants'
import { authEndpoints } from '../../config/Endpoints'
import InputForm from '../../components/UI/InputForm/InputForm'
import { influencerRegistrationForm } from '../../forms'
import { goToHomeScreen, redirectToScreen } from '../../services/GlobalServices'

class RegistrationScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
      registrationForm: JSON.parse(JSON.stringify(influencerRegistrationForm)),
      formIsValid: false,
      loading: false,
      passwordSame: true
    }
  }

  componentWillMount () {
    const {auth, history} = this.props
    if (auth.isAuthenticated) {
      goToHomeScreen(history)
    }
  }

  prepareDataForRegistration = async () => {
    const {registrationForm} = this.state
    const dataForSubmit = prepareDataFormForSubmitAction(registrationForm)
    dataForSubmit['providerId'] = getDataFromURL('id')
    dataForSubmit['userId'] = getDataFromURL('userId')
    await this.registerInfluencer(dataForSubmit)
  }

  registerInfluencer = async (influencerData) => {
    const {history} = this.props
    await postRequest(authEndpoints.INFLUENCER_REGISTER, influencerData, toastNotification.SHOW, () => { redirectToScreen(history, '/success-registration', role.INFLUENCER)})
  }

  render () {
    const {registrationForm} = this.state
    return (
      <div style={styles.root}>
        <Card style={styles.influencerRegistrationCardForm}>
          <Logo />
          <label style={styles.introText}>{localize('loginScreen.welcomeInfluencer')}</label>
          <InputForm formElements={registrationForm} prepareDataAndSubmit={this.prepareDataForRegistration}
            mode={formMode.NEW} lastElementInLayout={layoutType.COLUMNS_1} />
        </Card>
      </div>
    )
  }

}

const mapStateToProps = state => ({
  auth: state.auth
})

const mapStateToDispatch = dispatch => ({
  loginInfluencer: (token, history) => dispatch(loginInfluencer(token, history))
})

export default connect(
  mapStateToProps,
  mapStateToDispatch
)(RegistrationScreen)

