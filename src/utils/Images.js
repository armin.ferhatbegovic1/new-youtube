const images = {
  logo: require('../assets/logo.png'),
  userAvatar: require('../assets/user-avatar.png'),
  videoPlaceholder: require('../assets/video-placeholder.jpg'),
  audioUpload: require('../assets/audio-upload.png'),
  audio: require('../assets/audio.png'),
  play: require('../assets/play.png'),
  stopIcon: require('../assets/stopIcon.png'),
}

export default images
