export const dealsEndpoints = {
  BRAND_PUBLISHED_DEALS: '/deal-list',
  BRAND_UNPUBLISHED_DEALS: '/completed-deals',
  INFLUENCER_ACTIVE_DEALS: '/influencer/deals',
  IMAGE: '/image',
  CATEGORIES: '/deal-categories',
  DEAL: '/deal',
  DEAL_BID: '/bid',
  DEAL_BID_CHECK: '/bid/check',
  DEAL_FEE: '/deal-fee',
  DEAL_PAYMENT: '/deal/payment',
  DEAL_CLOSE: '/close-deal',
  DEAL_STATUS_CHANGE: '/deal/deal-status',
  DEAL_PUBLISH_STATUS_CHANGE: '/deal/publish-status'
}
export const bidsEndpoints = {
  INFLUENCER_BIDS: '/bid/list',
  BRAND_BIDS: '/bid/brand/list',
  CANCEL_BID: '/bid/cancel', //influencer cancel
  BRAND_ACCEPT_BID: '/bid/accept',
  BRAND_REJECT_BID: '/bid/reject',
  BRAND_FINISH_BID: '/bid/finish',
  BRAND_CANCEL_BID: '/bid/brand/cancel'
}

export const authEndpoints = {
  INSTAGRAM_AUTH: '/auth/instagram/callback',
  INFLUENCER_REGISTER: '/register-influencer'
}

export const userEndpoints = {
  PROFILE_EDIT: '/profile',
  PROFILE_VIEW: '/profile',
  BRAND_USER_REGISTER: '/register',
  BRAND_CHANGE_PASSWORD: '/profile/update-password',
  BRAND_FORGOT_PASSWORD: '/forgotPassword',
  BRAND_NEW_PASSWORD:'/resetPassword',
}

export const videos={
  VIDEO_LIST:'https://voiceoverapi2.digitalgalaxy.cn:8443/api_vid/v1/video/list?order_by=title&order_dir=asc&is_read=true',
  FILE_UPLOAD:'/file',
  VIDEO_BY_ID:'/video',
  IMAGE:'/image',
  FILE:'/file',
    SENTENCE:'/sentence',
  FILE_VIEW:'/file',
}
