export const LayoutType = {
  empty: 'Empty',
  dashboard: 'Dashboard'
}

export const role = {
  ADMIN: 'admin',
  BRAND: 'admin',
  INFLUENCER: 'Influencer'
}

export const uploadDealImage = {
  MAX_UPLOAD_DEAL_IMAGE_WIDTH: 2000,
  MAX_UPLOAD_DEAL_IMAGE_HEIGHT: 2000,
  MIN_UPLOAD_DEAL_IMAGE_WIDTH: 400,
  MIN_UPLOAD_DEAL_IMAGE_HEIGHT: 400
}

export const alertDialogType = {
  WARNING: 'Warning',
  ERROR: 'Error',
  INFO: 'Info',
  CONFIRM: 'Confirm'
}

export const table = {
  TABLE_ROWS_PER_PAGE: 10
}

export const response = {
  OK: 200,
  BAD: 400
}
export const request = {
  GET: 'GET',
  POST: 'POST',
  DELETE: 'DELETE',
  PUT: 'PUT',
  PATCH:'PATCH'
}
export const toastNotification = {
  SHOW: true,
  HIDE: false
}
export const toastType = {
  SUCCESS: 'success',
  FAILURE: 'failure'
}

export const dateFormat = {
  YYYYMMDD: 'YYYY-MM-DD',
  MMDDYYYY: 'MM-DD-YYYY',
  MMMMDDYYYY: 'MMMM DD, YYYY'
}

export const dealFormType = {
  EDIT_DEAL: 'EDIT_DEAL_FORM',
  NEW_DEAL: 'NEW_DEAL_FORM'
}
export const formMode = {
  EDIT: 'EDIT',
  NEW: 'NEW',
  VIEW: 'VIEW'
}

export const dealFormCode = {
  VALID_FORM: 'VALID_FORM',
  INVALID_FORM: 'INVALID_FORM',
  FORM_SAVED: 'FROM_SAVED'
}
export const menuItemType = {
  DIVIDER: 'DIVIDER',
  TITLE: 'TITLE',
  LINK: 'LINK'
}

export const preStepScreenMode = {
  INFLUENCER_LOGIN: 'INFLUENCER_LOGIN',
  BRAND_LOGIN: 'BRAND_LOGIN',
  INFLUENCER_REGISTRATION: 'INFLUENCER_REGISTRATION',
  BRAND_REGISTRATION: 'BRAND_REGISTRATION',
  BRAND_FORGOT_PASSWORD:'BRAND_FORGOT_PASSWORD'
}
export const cardType = {
  PLACEHOLDER: 'PLACEHOLDER',
  ORIGINAL: 'ORIGINAL',
  PLACEHOLDER_IMAGE: 'PLACEHOLDER_IMAGE'
}

export const numberOfTextLines = {
  DEAL_DESCRIPTION: 2
}

export const deviceType = {
  PHONE: 'Phone',
  TABLET: 'Tablet',
  DESKTOP: 'Desktop',
  LARGE_DESKTOP: 'LargeDesktop'
}

export const countOfDeal = {
  ONE: 1,
  TWO: 2,
  THREE: 3,
  FOUR: 4
}

export const menuDealActions = {
  EDIT_DEAL: 'EDIT_DEAL',
  PREVIEW_DEAL: 'PREVIEW_DEAL',
  CLOSE_DEAL: 'CLOSE_DEAL',
  CANCEL_DEAL:'CANCEL_DEAL',
  DELETE_DEAL: 'DELETE_DEAL',
  RENEW_DEAL: 'RENEW_DEAL',
  CHANGE_PUBLISH_STATUS:'CHANGE_PUBLISH_STATUS'
}

export const inputTypeFormElement = {
  COUNTRY_AUTO_SUGGEST: 'COUNTRY_AUTO_SUGGEST',
  CITY_AUTO_SUGGEST: 'CITY_AUTO_SUGGEST',
  SELECT_LIST_CATEGORIES: 'SELECT_LIST_CATEGORIES',
  TEXT_FIELD: 'TEXT_FIELD',
  CHECKBOX: 'CHECKBOX',
  SWITCH: 'SWITCH',
  IMAGE: 'IMAGE',
  TEXT_AREA: 'TEXT_AREA',
  SELECT_LIST: 'SELECT_LIST',
  UPLOAD_EDIT_IMAGE: 'UPLOAD_EDIT_IMAGE',
  MAIN_BUTTON:'MAIN_BUTTON',
  MUSIC:'MUSIC'
}

export const layoutType = {
  COLUMNS_1: 12,
  COLUMNS_2: 6,
  COLUMNS_3: 4,
  COLUMNS_4: 3
}

export const positionButton = {
  CENTER: 'center',
  RIGHT: 'flex-end',
  LEFT: 'flex-start'
}
export const dealSaveType =
  {
    NEW_DATA_SAVE: 'NEW_DATA_SAVE',
    DATA_SAVE: 'DATA_SAVE',
    DATA_SAVE_AND_PAY: 'DATA_SAVE_AND_PAY',
    NEW_DATA_SAVE_AND_PAY: 'NEW_DATA_SAVE_AND_PAY'
  }

export const bidType = {
  SUBMITTED: 0,
  ACCEPTED: 1,
  REJECTED: 2,
  CANCELLED: 3,
  FINISHED: 4,
  CANCELLED_BY_BRAND: 5
}

export const formDialog = {
  SHOW: true, HIDE: false
}
export const REDIRECT_PAGE_TIME=1000
export const TOAST_HIDE_TIME=30000
export const requestHeader = (token=localStorage.getItem('jwtToken'))=> ({
  'content-type': 'application/json',
  'X-Auth-Token': token
})


export const circularLoader={
  SUCCESS:'SUCCESS',
  PROGRESS:'PROGRESS',
  FAILURE:'FAILURE'
}

export const loaderVisbility={
  SHOW:true,
  HIDE:false
}

export const THRESHOLD_COUNT=1
export const BATCH_SIZE=5

export const publishStatus={
  PUBLISHED:1,
  UNPUBLISHED:0
}
export const autoCloseDialog= {
  AUTO_CLOSE_DIALOG_TRUE:true,
  AUTO_CLOSE_DIALOG_FALSE:false
}
export const PASSWORD_MIN_CHAR=6

export const videoStatus={
  IN_PROGRESS:0,
  UPLOADED: 1
}
