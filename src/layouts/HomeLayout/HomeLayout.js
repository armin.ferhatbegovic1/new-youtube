import React, {Component, Fragment} from 'react'
import {withStyles} from '@material-ui/core/styles'
import {connect} from 'react-redux'
import Header from '../../components/Header/Header'
import styles from './styles'


class HomeLayout extends Component {
    state = {
        open: true
    }

    handleToggleDrawer = () => {
        this.setState(prevState => {
            return {open: !prevState.open}
        })
    }

    render() {
        const {classes, children} = this.props
        return (
            <Fragment>
                <div className={classes.root}>
                    <Header
                        logout={this.props.logout}
                        handleToggleDrawer={this.handleToggleDrawer}
                    />
                    <main
                        className={classes.content}
                    >
                        {children}
                    </main>

                </div>
            </Fragment>
        )
    }
}


export default connect(
    null
)(withStyles(styles)(HomeLayout))
