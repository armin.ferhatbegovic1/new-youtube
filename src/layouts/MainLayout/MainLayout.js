import React, { Fragment, Component } from 'react'
import { withStyles } from '@material-ui/core/styles'
import classNames from 'classnames'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import Header from '../../components/Header/Header'
import Sidebar from '../../components/Sidebar/Sidebar'
import { logoutUser } from '../../store/actions/authentication'
import styles from './styles'
import Dimensions from '../../utils/Dimensions'
import { deviceType } from '../../config/Constants'

const drawerWidth = 240


const calculateHeaderTop =()=> ({
  headerMargin:{
    marginTop: Dimensions.getDevice() === deviceType.PHONE ?68:90,
  }
})


class MainLayout extends Component {
  state = {
    open: Dimensions.getDevice() === deviceType.PHONE ? false : true
  }

  handleToggleDrawer = () => {
    this.setState(prevState => {
      return {open: !prevState.open}
    })
  }

  render () {

    const {classes, children,logout} = this.props
    const {open}=this.state
    return (
      <Fragment>
        <div className={classes.root}>
          <Header
            logout={logout}
            handleToggleDrawer={this.handleToggleDrawer}
            open={open}
          />
          <main
            style={calculateHeaderTop().headerMargin}
            className={classNames(classes.content, {
              [classes.contentShift]: this.state.open
            },
              )}
          >
            {children}
          </main>
        </div>
        <Sidebar open={this.state.open} drawerWidth={drawerWidth} />
      </Fragment>
    )
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      logoutUser: () => logoutUser()
    },
    dispatch
  )
}

export default connect(
  null,
  mapDispatchToProps
)(withStyles(styles)(MainLayout))
