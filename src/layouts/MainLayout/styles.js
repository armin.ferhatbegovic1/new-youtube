import Dimensions from '../../utils/Dimensions'
import { deviceType } from '../../config/Constants'

const drawerWidth = 240;

const styles = theme => ({
  root: {
    display: "flex"
  },
  content: {

    flexGrow: 1,
    marginLeft: 0,
  //  paddingTop: theme.spacing.unit * 3,
    paddingLeft:0,
    paddingRight:0,

    overflowX: "hidden"
  },
  contentShift: {
    marginLeft:Dimensions.getDevice() === deviceType.PHONE?'0!important':drawerWidth,
   // width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    })
  }
})

export default styles