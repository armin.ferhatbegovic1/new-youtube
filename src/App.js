import React, {Component} from 'react'
import CssBaseline from '@material-ui/core/CssBaseline'
import {MuiThemeProvider} from '@material-ui/core/styles'
import {connect, Provider} from 'react-redux'
import AppToast from './components/UI/AppToast/AppToast'
import AlertDialog from './components/UI/AlertDialog/AlertDialog'
import store from './store'
import {BrowserRouter, withRouter} from 'react-router-dom'
import {SetAuthToken} from './services/'
import jwt_decode from 'jwt-decode'
import {logoutUser} from './store/actions/authentication'
import RoutesList from './components/Routes/Routes'
import FormDialog from './components/UI/FormDialog/FormDialog'
import {setCurrentUser} from './store/actions/userActions'
import Loader from './components/UI/Loader/Loader'

class App extends Component {

    componentDidMount() {
        window.addEventListener('resize', this.updateWidth)
    }

    async componentWillMount() {
        const {dispatch, history} = this.props
        if (localStorage.jwtToken) {
            SetAuthToken(localStorage.jwtToken)
            const decoded = jwt_decode(localStorage.jwtToken)
            decoded['role'] = 'admin'
            dispatch(setCurrentUser(decoded))
            // await setUserProfileInfo(dispatch)
            const currentTime = Date.now() / 1000
            if (decoded.exp < currentTime) {
                dispatch(logoutUser(history))
                //  window.location.href = '/'
            }
        }
        window.removeEventListener('resize', this.resize)
    }

    render() {
        const {settings, auth} = this.props
        return (
            <MuiThemeProvider theme={settings.theme}>
                <CssBaseline/>
                <div style={{height: '100vh'}}>
                    <AppToast/>
                    <AlertDialog/>
                    <Loader/>
                    <FormDialog/>
                    <RoutesList authenticate={auth}/>
                </div>
            </MuiThemeProvider>
        )
    }
}

const mapStateToProps = state => {
    return {
        settings: state.settings,
        auth: state.auth,
        screen: state.screen
    }
}
const ConnectedApp = withRouter(connect(mapStateToProps)(App))

const Root = () => {
    return <BrowserRouter><Provider store={store}><ConnectedApp/></Provider></BrowserRouter>
}
export default Root

