import {getRequest, postRequest, putRequest} from './RequestManager/RequestManager'
import {dealsEndpoints} from '../config/Endpoints'
import {alertDialogType, dealFormCode, dealSaveType, formMode, toastNotification} from '../config/Constants'
import {localize} from './Localization/LocalizationManager'
import {isEmpty, prepareDataFormForSubmitAction} from './GlobalServices'

export async function getDeal(dealID) {
    return await getRequest(dealsEndpoints.DEAL + '/' + dealID)
}

export async function getDealImage(deal) {
    return await getRequest(dealsEndpoints.IMAGE + '/' + deal['image'])
}

export function prepareDealDataAndRedirectToPaymentScreen(paymentInfo, dealID, history) {
    history.push({
        pathname: '/payment-progress',
        state: {
            data: {dealId: dealID, payment: {...paymentInfo}}
        }
    })
}

export async function prepareDealDataForSave(saveMode, that) {
    const {dealForm, dealID, mode} = that.state
    const {setAlertDialog} = that.props
    const dataForSubmit = prepareDataFormForSubmitAction(dealForm)
    let response

    if (saveMode === dealSaveType.NEW_DATA_SAVE || saveMode === dealSaveType.NEW_DATA_SAVE_AND_PAY) {

        response = await postRequest(dealsEndpoints.DEAL, dataForSubmit, toastNotification.SHOW, null, localize('newDealScreen.dealSaveSuccessfully'))
    }
    if (saveMode === dealSaveType.DATA_SAVE || saveMode === dealSaveType.DATA_SAVE_AND_PAY) {
        response = await putRequest(dealsEndpoints.DEAL + '/' + dealID, dataForSubmit, toastNotification.SHOW, localize('newDealScreen.dealSaveSuccessfully'))
    }
    if (!isEmpty(response) && !isEmpty(response['_id'])) {
        that.setState({dealID: response['_id']})
    }
    if ((saveMode === dealSaveType.NEW_DATA_SAVE_AND_PAY || saveMode === dealSaveType.DATA_SAVE_AND_PAY) && !isEmpty(response)) {
        that.selectDurationDialogOpen()
    }
    if (mode !== formMode.NEW) {
        that.setState({mode: formMode.VIEW})
    }
    if (isEmpty(response) && response === dealFormCode.INVALID_FORM) {
        setAlertDialog(true, localize('newDealScreen.dealIsNotSaved'), localize('newDealScreen.dealWarning'), alertDialogType.WARNING)
    }
}

export function previewDeal(history, dealID) {
    history.push(/preview-deal/ + dealID)
}

export function editDeal(history, dealID) {
    history.push(/edit-deal/ + dealID)
}
