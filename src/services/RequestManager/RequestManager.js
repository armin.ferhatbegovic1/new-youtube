import apiEndpoint from '../../config/config'
import store from '../../store/index'
import { setToastNotification } from '../../store/actions/toastAction'
import { handleErrorResponse, handleResponseCode, isEmpty, localize } from '../'
import { loaderVisbility, request, requestHeader, toastType } from '../../config/Constants'
import { setLoaderVisibility } from '../../store/actions/loaderAction'

export async function getRequest (apiLocation, toastShow = false) {
  store.dispatch(setLoaderVisibility(loaderVisbility.SHOW))
  return fetch(apiEndpoint.apiEndpoint + apiLocation, {
    method: request.GET,
    headers: requestHeader()
  })
  .then(function (response) {
    return response.clone().json()
  })
  .then(function (data) {
    if (toastShow === true) {
      store.dispatch(setToastNotification(toastShow, handleResponseCode(data), toastType.SUCCESS))
    }
    return data
  })
  .catch(function (error) {
    store.dispatch(setToastNotification(true, handleErrorResponse(error), toastType.FAILURE))
    return error
  }).finally(function () {
    store.dispatch(setLoaderVisibility(loaderVisbility.HIDE))
  })
}

export function postRequest (apiLocation, data, toastShow = true, redirectionCallBack = null, toastMessage = localize('successfullyDataPost')) {
  store.dispatch(setLoaderVisibility(loaderVisbility.SHOW))
  return fetch(apiEndpoint.apiEndpoint + apiLocation, {
    method: request.POST,
    headers: requestHeader(),
    body: JSON.stringify(data)
  })
  .then(function (response) {
    if (response.status === response.BAD) {
      redirectionCallBack = null
    }
    return response.clone().json()
  })
  .then(function (dataResponse) {
    if (!isEmpty(dataResponse['errors'])) {
      store.dispatch(setToastNotification(toastShow, handleErrorResponse(dataResponse), 'error'))
    } else if (toastShow === true) {
      store.dispatch(setToastNotification(toastShow, handleResponseCode(dataResponse, toastMessage), toastType.SUCCESS))
    }
    if (redirectionCallBack != null) {
      redirectionCallBack(dataResponse)
    }
    return dataResponse
  })
  .catch(function (error) {
    store.dispatch(setToastNotification(true, handleErrorResponse(error), toastType.FAILURE))
    return error
  }).finally(function () {
    store.dispatch(setLoaderVisibility(loaderVisbility.HIDE))
  })
}
export function patchRequest (apiLocation, data, toastShow = true, redirectionCallBack = null, toastMessage = localize('successfullyDataPost')) {
  store.dispatch(setLoaderVisibility(loaderVisbility.SHOW))
  return fetch(apiEndpoint.apiEndpoint + apiLocation, {
    method: request.PATCH,
    headers: requestHeader(),
    body: JSON.stringify(data)
  })
  .then(function (response) {
    if (response.status === response.BAD) {
      redirectionCallBack = null
    }
    return response.clone().json()
  })
  .then(function (dataResponse) {

    if (!dataResponse.success) {
      store.dispatch(setToastNotification(toastShow, handleErrorResponse(dataResponse), 'error'))
    } else if (toastShow === true) {
      store.dispatch(setToastNotification(toastShow, handleResponseCode(dataResponse, toastMessage), toastType.SUCCESS))
    }
    if (redirectionCallBack != null) {
      redirectionCallBack(dataResponse)
    }
    return dataResponse
  })
  .catch(function (error) {
    store.dispatch(setToastNotification(true, handleErrorResponse(error), toastType.FAILURE))
    return error
  }).finally(function () {
    store.dispatch(setLoaderVisibility(loaderVisbility.HIDE))
  })
}
export function deleteRequest (apiLocation, toastShow = true, toastMessage = localize('successfullyDataDelete')) {
  return fetch(apiEndpoint.apiEndpoint + apiLocation, {
    method: request.DELETE,
    headers: requestHeader()
  })
  .then(function (response) {
    return response.clone().json()
  })
  .then(function (dataResponse) {
    if (toastShow === true) {
      store.dispatch(setToastNotification(toastShow, handleResponseCode(dataResponse, toastMessage), toastType.SUCCESS))
    }
    return dataResponse
  })
  .catch(function (error) {
    store.dispatch(setToastNotification(true, handleErrorResponse(error), toastType.FAILURE))
  })

}

export function putRequest (apiLocation, data, toastShow = true, toastMessage = localize('successfullyDataPut')) {
  store.dispatch(setLoaderVisibility(loaderVisbility.SHOW))
  return fetch(apiEndpoint.apiEndpoint + apiLocation, {
    method: request.PUT,
    headers: requestHeader(),
    body: JSON.stringify(data)
  })
  .then(function (response) {
    return response.clone().json()
  })
  .then(function (dataResponse) {
    if (toastShow === true) {
      store.dispatch(setToastNotification(true, typeof dataResponse.message === 'undefined' ? toastMessage : dataResponse.message, 'success'))
    }
    return dataResponse
  })
  .catch(function (error) {
    store.dispatch(setToastNotification(true, handleErrorResponse(error), toastType.FAILURE))
  }).finally(function () {
    store.dispatch(setLoaderVisibility(loaderVisbility.HIDE))
  })
}

export async function getRequestVideo (apiLocation, toastShow = false) {
  store.dispatch(setLoaderVisibility(loaderVisbility.SHOW))
  return fetch( apiLocation, {
    method: request.GET,
    headers: requestHeader()
  })
  .then(function (response) {
    return response.clone().json()
  })
  .then(function (data) {
    if (toastShow === true) {
      store.dispatch(setToastNotification(toastShow, handleResponseCode(data), toastType.SUCCESS))
    }
    return data
  })
  .catch(function (error) {
    store.dispatch(setToastNotification(true, handleErrorResponse(error), toastType.FAILURE))
    return error
  }).finally(function () {
    store.dispatch(setLoaderVisibility(loaderVisbility.HIDE))
  })
}
