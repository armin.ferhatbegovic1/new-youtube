import { localize } from './Localization/LocalizationManager'
import moment from 'moment'
import Dimensions from '../utils/Dimensions'
import { REDIRECT_PAGE_TIME } from '../config/Constants'

export const awaitFunction = asyncFunction => {
  return new Promise(async resolve => {
    await asyncFunction
    resolve()
  })
}

export const isEmpty = (value) => {
  return (
    value === undefined ||
    value === null ||
    (typeof value === 'object' && Object.keys(value).length === 0) ||
    (typeof value === 'string' && value.trim().length === 0)
  )
}

export function checkIsBoolean (value) {
  return typeof value === 'boolean'
}

export const handleResponseCode = (responseData, toastMessage) => {
  if (isEmpty(responseData) || typeof responseData.message === 'undefined') {
    return !isEmpty(toastMessage) ? toastMessage : localize('successfullyDataReceived')
  }

  if (!isEmpty(responseData) && !isEmpty(responseData.message)) {
    return localize('responseCode.' + responseData['message'])
  }
}

export const handleErrorResponse = (error) => {
  console.log('Error => ', error)
  /*if (!isEmpty(error['error'])) {
     return localize('responseCode.' + error['error'])
   }
   if (!isEmpty(error['response'] && !isEmpty(error['response'].data))) {
     const errorData = JSON.parse(JSON.stringify(error['response'].data))
     const errorCode = errorData.error
     const errorMessage = errorData.message
     if (!isEmpty(errorMessage)) {
       return errorMessage.toString()
     }
     if (!isEmpty(errorCode)) {
       return localize('responseCode.' + errorCode)
     } else return localize('errorCode.999')
   } else {
     return localize('responseCode.999')
   }*/
  let message = ''
if(!isEmpty(error['errors'])){
  for (let i = 0; i < error['errors'].length; i++) {
    for (const [key, value] of Object.entries(error['errors'][i])) {
      message += ' ' + key + ' ' + value[0]
    }
  }}
  return 'Failed ' + message
}

export function changeDateFormat (date, dateFormat) {
  return moment(parseInt(date)).format(dateFormat)
}

export function calculateCountOfItemsInRow () {
  const deviceWidth = Dimensions.getWidth()
  /*if (deviceWidth > Dimensions.desktop+100) {
    return countOfDeal.FOUR
  }*/
  if (1500 <= deviceWidth)
    return 4

  if (1350 <= deviceWidth && deviceWidth < 1500)
    return 3
  if (768 <= deviceWidth && deviceWidth < 1350)
    return 2
  if (100 <= deviceWidth && deviceWidth < 768) {
    return 1
  }
}

export function calculateGrid () {
  const deviceWidth = Dimensions.getWidth()
  /*if (deviceWidth > Dimensions.desktop+100) {
    return countOfDeal.FOUR
  }*/
  if (1500 <= deviceWidth) {
    return 3
  }

  if (1350 <= deviceWidth && deviceWidth < 1500) {
    return 4
  }
  if (768 <= deviceWidth && deviceWidth < 1350) {
    return 6
  }
  if (100 <= deviceWidth && deviceWidth < 768) {
    return 12
  }

}

export function getDataFromURL (parameterName) {
  const url = new URL(window.location.href)
  return url.searchParams.get(parameterName)
}

export function prepareDataFormForSubmitAction (newDealForm) {
  const newDealData = {}
  for (let formElement in newDealForm) {
    newDealData[formElement] = newDealForm[formElement].value
    if (formElement === 'category' || formElement === 'categories' || formElement === 'categoriesOfInterest') {
      newDealData[formElement] = getIDsOfCategories(newDealForm[formElement].value)
    }
    if (formElement === 'country' || formElement === 'countryOfInterest') {
      if (newDealData['worldwideDeal'] === true || newDealData['worldwide'] === true) {
        newDealData[formElement] = null
      } else
        newDealData[formElement] = newDealForm[formElement].id
      //newDealData[formElement] =null
    }
    if (formElement === 'city' || formElement === 'cityOfInterest') {
      if (newDealData['worldwideDeal'] === true || newDealData['worldwide'] === true) {
        newDealData[formElement] = null
      } else
        newDealData[formElement] = newDealForm[formElement].id
      //newDealData[formElement] =null
    }
  }
  return newDealData
}

export function getIDsOfCategories (categories) {
  const categoriesID = []
  for (let i = 0; i < categories.length; i++) {
    categoriesID.push(categories[i]['_id'])
  }
  return categoriesID
}

export function createLinkForFetchingListData (endpoint, skip, size, sort, filter) {
  const filterModify = !isEmpty(filter) ? filter : ''

  const link = endpoint + '?skip=' + skip.toString() + '&size=' + size.toString() + '&sort=' + sort.toString() + filterModify
  return link
}

export function abbreviateNumber (value) {
  let newValue = value
  if (value >= 1000) {
    const suffixes = ['', 'k', 'm', 'b', 't']
    let suffixNum = Math.floor(('' + value).length / 3)
    let shortValue = ''
    for (let precision = 2; precision >= 1; precision--) {
      shortValue = parseFloat((suffixNum !== 0 ? (value / Math.pow(1000, suffixNum)) : value).toPrecision(precision))
      let dotLessShortValue = (shortValue + '').replace(/[^a-zA-Z 0-9]+/g, '')
      if (dotLessShortValue.length <= 2) { break }
    }

    newValue = shortValue + suffixes[suffixNum]
  }
  return newValue
}

export function extractItemsFromListBySpecificProperty (list, property) {
  return list.map((item) => {
    return item[property]
  })
}

export function getCurrentHostName () {
  const location = window.location
  return process.env.NODE_ENV !== 'development' ? location.protocol + '//' + location.hostname + (location.port ? ':' + location.port : '') : ''
}

export function goToHomeScreen (history) {
  history.push('/')
}

export function redirectToScreen (history, endpoint, param = {}, time = REDIRECT_PAGE_TIME) {
  setTimeout(
    () => {
      history.push({
        pathname: endpoint, state: {
          role: param
        }
      })
    },
    time
  )
}

export function getHeaderHeight () {
  const firstRow = document.getElementsByTagName('header')
  if (!isEmpty(firstRow)) {
    return document.getElementsByTagName('header')[0].clientHeight
  }
}

export function errorMessageForMinCharInputField (minChar) {
  return localize('enterAtLeast') + minChar + ' ' + localize('characters')
}
