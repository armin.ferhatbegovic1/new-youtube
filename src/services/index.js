import AxiosInstance from './Axios'
import { changeLanguage, localize, getCurrentLanguage } from './Localization/LocalizationManager'
import SetAuthToken from './SetAuthToken'
import {postRequest,getRequest,putRequest,deleteRequest} from './RequestManager/RequestManager'
import {awaitFunction,isEmpty,changeDateFormat,calculateCountOfItemsInRow,getDataFromURL,checkIsBoolean,prepareDataFormForSubmitAction,handleResponseCode,
  handleErrorResponse,getCurrentHostName} from './GlobalServices'
import {getDeal,getDealImage,prepareDealDataForSave}from './DealServices'
export {
  AxiosInstance, isEmpty, changeLanguage, localize, getCurrentLanguage, SetAuthToken,postRequest,getRequest,awaitFunction,putRequest,changeDateFormat,calculateCountOfItemsInRow,
  deleteRequest,getDataFromURL,checkIsBoolean,prepareDataFormForSubmitAction,getDeal,getDealImage,handleResponseCode,handleErrorResponse,getCurrentHostName,prepareDealDataForSave
}