import axios from 'axios'
import store from '../store/index'
import { setLoaderVisibility } from '../store/actions/loaderAction'
import { loaderVisbility } from '../config/Constants'


const instance = axios.create()
instance.interceptors.request.use(request => {
  store.dispatch(setLoaderVisibility(loaderVisbility.SHOW))
  return request
}, error => {
  store.dispatch(setLoaderVisibility(loaderVisbility.HIDE))
  console.log(error)
  return Promise.reject(error)
})

instance.interceptors.response.use(request => {
  console.log(request)
  store.dispatch(setLoaderVisibility(loaderVisbility.HIDE))


  return request
}, error => {
  console.log(error)
  return Promise.reject(error)
})

export default instance
