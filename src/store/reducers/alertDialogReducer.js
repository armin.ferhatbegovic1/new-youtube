import { DIALOG_TYPE, DIALOG_OPEN, DIALOG_MESSAGE,DIALOG_TITLE,DIALOG_ACTION } from '../actions/types'

export default function reducer(
  state = {
    open: false,
    message: '',
    type: '',
    title:'',
    action:null
  },
  action
) {
  switch (action.type) {
    case DIALOG_OPEN: {
      return { ...state, open: action.payload };
    }
    case DIALOG_MESSAGE: {
      return { ...state, message: action.payload };
    }
    case DIALOG_TYPE: {
      return { ...state, type: action.payload };
    }
    case DIALOG_TITLE: {
      return { ...state, title: action.payload };
    }
    case DIALOG_ACTION: {
      return { ...state, action: action.payload };
    }
    default:
      return state;
  }
}
