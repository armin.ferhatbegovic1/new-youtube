import { SET_CURRENT_USER, SET_CURRENT_USER_CATEGORIES, SET_CURRENT_USER_PROFILE_IMAGE } from '../actions/types'
import { isEmpty } from '../../services'

const initialState = {
  isAuthenticated: false,
  user: {
  }
}

export default function (state = initialState, action) {
  switch (action.type) {
    case SET_CURRENT_USER:
      return {
        ...state,
        isAuthenticated: !isEmpty(action.payload),
        user: action.payload
      }
    case SET_CURRENT_USER_PROFILE_IMAGE:
      return {
        ...state,
        user: {
          ...state['user'],
          profileImage: action.payload
        }
      }
    case SET_CURRENT_USER_CATEGORIES:
      return {
        ...state,
        user: {
          ...state['user'],
          categories: action.payload
        }
      }
    default:
      return state
  }
}
