import { CHANGE_SCREEN_WIDTH, CHANGE_SCREEN_HEIGHT } from '../actions/types'

export default function reducer (
  state = {
    width: {},
    height: {}
  },
  action
) {
  switch (action.type) {
    case CHANGE_SCREEN_WIDTH: {
      return {...state, width: action.payload}
    }
    case CHANGE_SCREEN_HEIGHT: {
      return {...state, height: action.payload}
    }
    default:
      return state
  }
}
