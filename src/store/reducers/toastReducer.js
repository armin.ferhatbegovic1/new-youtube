import { TOAST_TYPE, TOAST_OPEN, TOAST_MESSAGE } from '../actions/types'

export default function reducer(
    state = {
        open: false,
        message: '',
        type: ''
    },
    action
) {
    switch (action.type) {
        case TOAST_OPEN: {
            return { ...state, open: action.payload };
        }
        case TOAST_MESSAGE: {
            return { ...state, message: action.payload };
        }
        case TOAST_TYPE: {
            return { ...state, type: action.payload };
        }
        default:
            return state;
    }
}
