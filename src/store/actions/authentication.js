import { AxiosInstance, handleErrorResponse, handleResponseCode } from '../../services'
import { SetAuthToken } from '../../services/'
import jwt_decode from 'jwt-decode'
import { setToastNotification } from './toastAction'
import { loaderVisbility, role, toastType } from '../../config/Constants'
import { userEndpoints } from '../../config/Endpoints'
import { setCurrentUser } from './userActions'
import { redirectToScreen } from '../../services/GlobalServices'
import { setLoaderVisibility } from './loaderAction'
import apiEndpoint from "../../config/config";

export const registerBrandUser = (user, history) => dispatch => {
  dispatch(setLoaderVisibility(loaderVisbility.SHOW))
  AxiosInstance.post(userEndpoints.BRAND_USER_REGISTER, user)
  .then(res => {
      //const responseData = res['data']
      //dispatch(setToastNotification(toastNotification.SHOW, handleResponseCode(responseData), toastType.SUCCESS))
      //   dispatch(setToastNotification(true, localize('errorCode.' + responseData), toastType.SUCCESS))
      redirectToScreen(history, '/success-registration', role.BRAND, 0)
    }
  )
  .catch(error => {
    dispatch(setToastNotification(true, handleErrorResponse(error), toastType.FAILURE))
  })
  .finally(() => {
    dispatch(setLoaderVisibility(loaderVisbility.HIDE))
  })
}

export const loginBrand = (user, history) => dispatch => {
  dispatch(setLoaderVisibility(loaderVisbility.SHOW))
  /*const data = {
    user_id: '17209cf7-0274-4443-9db7-747db6d77e11',
    password: '17209cf7-0274-4443-9db7-747db6d77e11'
  }*/
  AxiosInstance.post(apiEndpoint.apiEndpoint+'/auth', user)
  .then(res => {
    const {token} = res.data.payload
    localStorage.setItem('jwtToken', token)
    //  SetAuthToken(token)
    const decoded = jwt_decode(token)
    decoded['role']=res['data'].payload['user_type']
    dispatch(setCurrentUser(decoded))
    redirectToScreen(history, '/video-list', {}, 0)
  })
  .catch(error => {
    let message = handleErrorResponse(error)
    dispatch(setToastNotification(true, 'Can\'t find user with credentials provided', toastType.FAILURE))
  }).finally(() => {
    dispatch(setLoaderVisibility(loaderVisbility.HIDE))
  })
}

export const forgotPasswordBrand = (user, history) => dispatch => {
  dispatch(setLoaderVisibility(loaderVisbility.SHOW))
  AxiosInstance.post(userEndpoints.BRAND_FORGOT_PASSWORD, user)
  .then(res => {
    dispatch(setToastNotification(true, handleResponseCode(res.data), toastType.SUCCESS))
    redirectToScreen(history, '/')
  })
  .catch(error => {
    dispatch(setToastNotification(true, handleErrorResponse(error), toastType.FAILURE))
  }).finally(() => {
    dispatch(setLoaderVisibility(loaderVisbility.HIDE))
  })
}
export const newPasswordBrand = (token, newPassword, history) => dispatch => {
  dispatch(setLoaderVisibility(loaderVisbility.SHOW))
  AxiosInstance.post(userEndpoints.BRAND_NEW_PASSWORD + '/' + token, newPassword)
  .then(res => {
    dispatch(setToastNotification(true, handleResponseCode(res.data), toastType.SUCCESS))
    redirectToScreen(history, '/login', role.BRAND)
  })
  .catch(error => {
    dispatch(setToastNotification(true, handleErrorResponse(error), toastType.FAILURE))
  }).finally(() => {
    dispatch(setLoaderVisibility(loaderVisbility.HIDE))
  })
}

export const loginInfluencer = (token, history) => async dispatch => {
  dispatch(setLoaderVisibility(loaderVisbility.SHOW))
  localStorage.setItem('jwtToken', token)
  const decoded = jwt_decode(token)
  dispatch(setCurrentUser(decoded))
  await setUserProfileInfo(dispatch)
  redirectToScreen(history, '/')
  //dispatch(setLoaderVisibility(loaderVisbility.HIDE))
}

export const logoutUser = (history) => dispatch => {
  localStorage.removeItem('jwtToken')
  SetAuthToken(false)
  dispatch(setCurrentUser({}))
  history.push('/')
}

export const setUserProfileInfo = async dispatch => {

  /*const profile = await getRequest(userEndpoints.PROFILE_EDIT)
  if (!isEmpty(profile['profileImage'])) {
    dispatch(setCurrentUserProfileImage(profile['profileImage']))
  }
  if (!isEmpty(profile['categoriesOfInterest'])) {
    dispatch(setCurrentUserProfileCategories(profile['categoriesOfInterest']))
  }*/
}

