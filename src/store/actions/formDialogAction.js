import {
  FORM_DIALOG_ACTION, FORM_DIALOG_ACTION_CALLBACK,
  FORM_DIALOG_FIELDS,
  FORM_DIALOG_MESSAGE,
  FORM_DIALOG_OPEN,
  FORM_DIALOG_TITLE,
  FORM_DIALOG_TYPE,
  FORM_DIALOG_AUTO_CLOSE
} from './types'

export const setFormDialog = (open, message, title, type, formFields, action = null, afterResponseCallback = null, autoClose = true) => dispatch => {
  dispatch({
    type: FORM_DIALOG_AUTO_CLOSE,
    payload: autoClose
  })
  dispatch({
    type: FORM_DIALOG_OPEN,
    payload: open
  })
  dispatch({
    type: FORM_DIALOG_MESSAGE,
    payload: message
  })
  dispatch({
    type: FORM_DIALOG_TITLE,
    payload: title
  })
  dispatch({
    type: FORM_DIALOG_TYPE,
    payload: type
  })
  dispatch({
    type: FORM_DIALOG_ACTION,
    payload: action
  })
  dispatch({
    type: FORM_DIALOG_FIELDS,
    payload: formFields
  })
  dispatch({
    type: FORM_DIALOG_ACTION_CALLBACK,
    payload: afterResponseCallback
  })
}

