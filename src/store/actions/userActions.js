import { SET_CURRENT_USER, SET_CURRENT_USER_CATEGORIES, SET_CURRENT_USER_PROFILE_IMAGE } from './types'

export const setCurrentUser = decoded => {
  return {
    type: SET_CURRENT_USER,
    payload: decoded
  }
}

export const setCurrentUserProfileImage = profileImage => dispatch => {
  dispatch({
    type: SET_CURRENT_USER_PROFILE_IMAGE,
    payload: profileImage
  })
}
export const setCurrentUserProfileCategories = categories => dispatch => {
  dispatch({
    type: SET_CURRENT_USER_CATEGORIES,
    payload: categories
  })
}
