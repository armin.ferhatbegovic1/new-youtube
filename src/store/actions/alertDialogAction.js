import { DIALOG_TYPE, DIALOG_OPEN, DIALOG_MESSAGE, DIALOG_TITLE, DIALOG_ACTION } from './types'

export const setAlertDialog = (open, message, title, type, action = null) => dispatch => {
  dispatch({
    type: DIALOG_OPEN,
    payload: open
  })
  dispatch({
    type: DIALOG_MESSAGE,
    payload: message
  })
  dispatch({
    type: DIALOG_TITLE,
    payload: title
  })
  dispatch({
    type: DIALOG_TYPE,
    payload: type
  })
  dispatch({
    type: DIALOG_ACTION,
    payload: action
  })
}
